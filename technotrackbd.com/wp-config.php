<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'technotrackdb');

/** MySQL database username */
define('DB_USER', 'technotr_user');

/** MySQL database password */
define('DB_PASSWORD', 'MF#Md7VsHbFV');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'Z-3lHwPQ`:D#F/7gi:iH(PRb~9V.Vds:eN1{$w{APECG#IHel lw?LMf3nF]${9)');
define('SECURE_AUTH_KEY',  ':N[HAulyWeQ[UBLH{hed.`ouh:>;T`OPyX`IKmYVI-OuTShn&oR>+D+]ze2+9wGc');
define('LOGGED_IN_KEY',    '$5b,<^O%#];D=_b,=,V4k4`XP9@hlqDB&.SZ5OFllMzFqy0TZ$VF7J[JpeSuiQ?g');
define('NONCE_KEY',        'VCS+m@,kKJN,CeO/t|,[1C0q*T`x,W)VoD):RWH<l3Bg:u}vf0i&q!3}w0;m{w7P');
define('AUTH_SALT',        'fA1*Oc@66X5Js.)99VPZEJE>)*%?d?Xt6h&i)$-U02qBkx)*lvX}xj9vIT noPBg');
define('SECURE_AUTH_SALT', 'Y#]`Rs>2Q&@fe0X:GK^+~]<}EL?K)J6d7F<?Gai#hcq*Nvu>F^pRZ?FJTYiYCF`o');
define('LOGGED_IN_SALT',   't.zjFS(0hnu=|pjL@y5?suA`[K_l<tE&o,SS5~(M)TH%h6OjWtark;Z#C^tDe*h0');
define('NONCE_SALT',       'sl<f^:*CdL{?&x$7W||D1{lKj`q}qs*D-AEM_`_!<wbe0g23K9<rQ`A*ICh:FH,|');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
