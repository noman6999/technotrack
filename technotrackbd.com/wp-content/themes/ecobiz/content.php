        <!-- List Latest News Start //-->
        <ul id="listlatestnews">
        <?php
            global $post, $ecobiz;
            
            $paged = (get_query_var('paged')) ?get_query_var('paged') : ((get_query_var('page')) ? get_query_var('page') : 1);
          
            $slideshow_order = $ecobiz['ecobiz-slideshow-order'] ? $ecobiz['ecobiz-slideshow-order'] : "date";
            $blog_category = $ecobiz['blog-category'];
            
            $blog_cats = array();
                                    
            foreach ($blog_category as $blog_cat => $val) {
                if ($val == 1 && $val !="" && $val !=0) {
                    array_push($blog_cats,$blog_cat);
                    $blog_include = implode(",",$blog_cats);    
                }
            }
        
            $blog_order = $ecobiz['blog-order'];
            $blog_number_perpage = $ecobiz['blog-number-perpage'];
            $blog_author = $ecobiz['blog-author'];
            $blog_comment = $ecobiz['blog-comment'];
            $blog_metapost = $ecobiz['blog-metapost'];
            
          $r = new WP_Query(array (
                    'cat'               => $blog_include,
                    'posts_per_page'    => $blog_number_perpage,
                    'orderby'           => $blog_order,
                    'paged'             => $paged
                )
            );
          //print_r($r);
          
          while ( $r->have_posts() ) : $r->the_post();
          $thumb   = get_post_thumbnail_id();
          $img_url = wp_get_attachment_url( $thumb,'full' ); //get full URL to image (use "large" or "medium" if the images too big)
          $image   = aq_resize( $img_url, 84, 84, true ); //resize & crop the image
          ?>
          <li>
            <div class="boximg-blog">
            <?php if (function_exists('has_post_thumbnail') && has_post_thumbnail()) {?>
              <div class="blogimage">
                <img src="<?php echo $image;?>" alt="" class="boximg-pad" />
              </div>
            <?php } ?>
            </div>
            <div <?php post_class('postbox'); ?>>
            <h3><a href="<?php the_permalink();?>"><?php the_title();?></a></h3>
            <p><?php the_excerpt();?></p>
           </div>
           <div class="clear"></div>
           <?php if ($blog_metapost == 1 ) { ?>
                <div class="metapost">
                  <span class="first"><?php echo __('Posted at ','ecobiz');?><?php the_time( get_option('date_format') ); ?> &raquo;</span>
                  <span><?php echo __('By ','ecobiz');?>: <?php the_author_posts_link();?>   &raquo;</span>                         
                  <span><?php echo __('Categories ','ecobiz');?>: <?php the_category(',');?>   &raquo;</span>
                  <?php if ($blog_comment == 1) { ?>
                  <span><?php comments_popup_link(__('0 Comment','ecobiz'),__('1 Comment','ecobiz'),__('% Comments','ecobiz'));?></span>
                  <?php } ?>
                </div>           
                <div class="clear"></div>
           <?php }?>
          </li>
          <?php endwhile;?> 
          </ul>
          <div class="clear"></div>
          <div class="pagination">
            <?php theme_blog_pagenavi('', '', $r, $paged);?>
          </div>  