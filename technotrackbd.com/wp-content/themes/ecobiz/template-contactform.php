<?php
/*
Template Name: Contact Form
*/
?>  
<?php get_header();?>

    <?php
        global $post, $ecobiz;
        
        $meta_prefix = "_imediapixel_";
        
        $page_slider_type = get_post_meta(get_the_ID(),$meta_prefix."page_slider_type",true);
        $page_slider_cat = get_the_terms(get_the_ID(),'slideshow_category');
        if (is_array($page_slider_cat) && !empty($page_slider_cat) && $page_slider_cat !="none") {
            foreach ($page_slider_cat as $slider_cat) {
                $slider_cat_name = $slider_cat->name;
            }     
        }
        $page_heading_image = get_post_meta(get_the_ID(),$meta_prefix."page_heading_image",true);
        $page_desc = get_post_meta(get_the_ID(),$meta_prefix."page_desc",true);
        $page_sidebar_position = get_post_meta(get_the_ID(),$meta_prefix."page_sidebar_position",true);
        $page_desc_position      = get_post_meta(get_the_ID(),$meta_prefix."page_desc_position",true);
        
        $slideshow_order    = $ecobiz['ecobiz-slideshow-order'];
        $breadcrumb         = $ecobiz['breadcrumb'];
        $info_address       = $ecobiz['office-address'];
        $info_phone         = $ecobiz['phone-number'];
        $info_fax           = $ecobiz['faximile-number'];
        $info_email         = $ecobiz['email-address'];
        $info_latitude      = $ecobiz['latitude-address'];
        $info_longitude     = $ecobiz['longitude-address'];
        $email_success_msg  = $ecobiz['email-success-msg'];
      ?>      
            
      <?php if ($page_slider_type =="" || !isset($page_slider_type) || $page_slider_type == "none" ) { ?>
        <!-- Page Heading --> 
        <div id="page-heading">
          <img src="<?php echo $page_heading_image ? $page_heading_image : get_template_directory_uri().'/images/page-heading.jpg';?>" alt="" />
          <div class="heading-text<?php if ($page_desc_position =="right") echo '-right';?>">
            <h3><?php the_title();?></h3>
            <p><?php echo stripslashes($page_desc);?></p>
          </div>
        </div>
        <!-- Page Heading End -->
      <?php } else { ?>
        <?php
        if ($page_slider_type !="none"  || $page_slider_type !="static-slider") {
          if ($page_slider_type == "nivo-slider") {
            imediapixel_get_nivoslider($slider_cat_name,$slideshow_order);
          } else if ($page_slider_type == "kwicks-slider") {
            imediapixel_get_kwicksslider($slider_cat_name,$slideshow_order);
          } else if ($page_slider_type == "static-slider") {
            echo imediapixel_get_staticslider();
          }
        }
        ?>
      <?php } ?>  
      <div class="clear"></div>
      <?php if (is_home() || is_front_page()) { ?>
        <div class="featuresbox">
          <?php 
          $count = count($sidebars_widgets['feature-boxes']);
          if ($count == 2) {
              echo '<ul class="features-2col">'; 
          } else if ($count == 3) {
              echo '<ul class="features-3col">';
          } else if ($count == 4) {
              echo '<ul>';
          } 
          ?>
          <?php
            if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('Site Features Box')) {
              echo __("Please place some \"Site Feature Box\" widget from Apppearances => Widgets menu",'ecobiz');
            } 
          ?>
          </ul>
        </div>
      <?php } ?>
      
      <div class="center">
      <?php if (!is_home() || !is_front_page()) { ?>      
        <?php if ($breadcrumb == 1) { ?>
          <div class="breadcrumb">
            <?php if ( function_exists( 'breadcrumbs_plus' ) ) breadcrumbs_plus(); ?>
          </div>
        <?php } ?>
      <?php } ?> 
      
        <!-- Main Content Wrapper -->
        <div class="maincontent-full">
          <!-- Contact Form -->
          <div id="conctactleft">
            <?php if (have_posts()) : ?>
            <?php while (have_posts()) : the_post();?>
            <?php the_content();?>
            <?php endwhile;?>
            <?php endif;?>
            
            <div class="success-message"><?php echo ($email_success_msg) ? stripslashes($email_success_msg) : __("Your message has been sent successfully. Thank you!",'ecobiz');?></div>
            
            <div id="maincontactform">
              <form action="#" id="contactform"> 
              <div>
                <label for="contactname"><?php echo __('Name ','ecobiz');?></label>
                <input type="text" name="contactname" class="textfield" id="contactname" value=""  /><span class="require"> *</span>
                <label for="contactsubject"><?php echo __('Subject ','ecobiz');?></label>
                <input type="text" name="contactsubject" class="textfield" id="contactsubject" value=""/><span class="require"> *</span>
                <label for="contactemail"><?php echo __('E-mail ','ecobiz');?></label> 
                <input type="text" name="contactemail" class="textfield" id="contactemail" value="" /><span class="require"> *</span>
                <label for="contactmessage"><?php echo __('Message ','ecobiz');?></label> 
                <textarea name="contactmessage" id="contactmessage" class="textarea" cols="8" rows="12"></textarea><span class="require"> *</span>
                <div class="clear"></div>
                <input type="hidden" name="siteurl" id="siteurl" value="<?php echo get_template_directory_uri();?>" />   
                <input type="hidden" name="sendto" id="sendto" value="<?php echo ($info_email) ? $info_email : get_option('admin_email');?>" />           
                <a href="#" class="button medium white" id="buttonsend"><?php echo __('SEND','ecobiz');?></a>
                <span class="loading" style="display: none;"><?php echo __('Please wait..','ecobiz');?></span>
              </div>
              </form>
            </div>
          </div>
          <!-- Contact Form End -->
          
          <!-- Contact Address -->
          <div id="contactright">
            
            <div id="map" class="imgbox">
              <?php echo do_shortcode('[gmap width="424" height="246" latitude="'.$info_latitude.'" longitude="'.$info_longitude.'"  controls="true" zoomcontrol="true" zoom="15" html="'.$info_address.'" popup="true"]');?>
            </div>
                
            <ul class="contactinfo">
              <?php if ($info_address) { ?><li><?php echo $info_address;?></li><?php } ?>
              <?php if ($info_phone) { ?><li><strong><?php echo __('Phone','ecobiz');?></strong> : <?php echo $info_phone;?></li><?php } ?>
              <?php if ($info_fax) { ?>
                <li><strong><?php echo __('Fax','ecobiz');?></strong> : <?php echo $info_fax;?></li><?php 
                } ?>
              <?php if ($info_email) { ?><li><strong><?php echo __('Email','ecobiz');?></strong> : <a href="mailto:<?php echo $info_email ? $info_email : "#";?>"><?php echo $info_email;?></a></li><?php } ?>
            </ul>      
            <div class="clear"></div>
          </div>
          <!-- Contact Address End -->          
        </div>
        <!-- Full Width Wrapper End -->
    
<?php get_footer();?>