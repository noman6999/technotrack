<?php get_header();?>
    <?php
    global $ecobiz;
    
    $meta_prefix = "_imediapixel_";
    $portfolio_page = $ecobiz['portfolio-page'];
    
    $page_heading_image = get_post_meta(get_the_ID(),$meta_prefix."page_heading_image",true);
    $bgtext_heading_position = get_post_meta(get_the_ID(),"_imediapixel_bgtext_heading_position",true);
    $page_desc = get_post_meta(get_the_ID(),$meta_prefix."page_desc",true);
    $page_sidebar_position = get_post_meta(get_the_ID(),$meta_prefix."page_sidebar_position",true);
    
    $breadcrumb         = $ecobiz['breadcrumb'];
    
    $nivo_transition = $ecobiz['nivo-transition'];
    $nivo_slices = $ecobiz['nivo-slice'];
    $nivo_animspeed = $ecobiz['nivo-speed'];
    $nivo_pausespeed = $ecobiz['nivo-pause'];
    $nivo_directionNav = $ecobiz['nivo-nav'];
    $nivo_pause_hover = $ecobiz['nivo-pause-hover'];
    $nivo_disable_permalink = $ecobiz['nivo-permalink'];
    $slideshow_order = $ecobiz['slideshow-order'];
    $enable_caption = $ecobiz['nivo-caption'];
    
    ?>
    <script type="text/javascript">
      jQuery(window).load(function($) {
        jQuery('#portfolio-slider').nivoSlider({
          effect:'<?php echo ($nivo_transition) ? $nivo_transition : "random";?>',
          slices:<?php echo ($nivo_slices) ? $nivo_slices : "15";?>,
          animSpeed:<?php echo ($nivo_animspeed) ? $nivo_animspeed : "500";?>, 
          pauseTime:<?php echo ($nivo_pausespeed) ? $nivo_pausespeed : "3000";?>,
          directionNav:<?php echo ($nivo_nav) ? $nivo_nav : "true";?>,
          pauseOnHover: <?php echo ($nivo_NavHide) ? $nivo_NavHide : "true";?>,
          controlNav: false,
          boxCols: 12,
          boxRows: 6,
        });
      });
      </script> 
            
      <!-- Page Heading --> 
      <div id="page-heading">
        <img src="<?php echo $page_heading_image ? $page_heading_image : get_template_directory_uri().'/images/page-heading.jpg';?>" alt="" />
        <div class="heading-text<?php if ($bgtext_heading_position =="right") echo '-right';?>">
          <?php
          $pf_cats = get_the_terms( get_the_ID(), 'portfolio_category');
          foreach ($pf_cats as $pf_cat) {
            $cat_title =  $pf_cat->name;
            $cat_description =  $pf_cat->description;
          }
          ?>
            <h3><?php echo $cat_title;?></h3>
            <p><?php echo stripslashes($cat_description);?></p>
        </div>
      </div>
      <!-- Page Heading End -->
      <div class="clear"></div>
      
      <div class="center">
      <?php if (!is_home() || !is_front_page()) { ?>      
        <?php if ($breadcrumb == 1) { ?>
          <div class="breadcrumb">
            <?php if ( function_exists( 'breadcrumbs_plus' ) ) breadcrumbs_plus(); ?>
          </div>
        <?php } ?>
      <?php } ?>    
      
        <!-- Main Content Wrapper -->
        <div class="maincontent-full">
          <?php if (have_posts()) : ?>
          <?php 
            while (have_posts()) : the_post();
            $thumb   = get_post_thumbnail_id();
            $img_url = wp_get_attachment_url( $thumb,'full' ); //get full URL to image (use "large" or "medium" if the images too big)
            $single_image   = aq_resize( $img_url, 872, 310, true ,true,true); //resize & crop the image
            
            $pf_link = get_post_meta(get_the_ID(), $meta_prefix.'portfolio_link', true );
            $pf_url = get_post_meta(get_the_ID(), $meta_prefix.'portfolio_url', true );
            $pf_gallery = get_post_meta(get_the_ID(), $meta_prefix.'portfolio_gallery', true );
            $pf_single_image   = aq_resize( $pf_link, 872, 310, true ,true,true); //resize & crop the image
            
          ?>

          <!-- Portfolio Detail Content -->
          <div class="col_full">
            <div class="portfolio-single-box">
                
                <?php if (is_array($pf_gallery) && !empty($pf_gallery) && ($pf_gallery !="")) { ?>            
                  <div id="portfolio-slider">
                   <?php
                  	foreach ($pf_gallery as $pf_gal) {
                  		$image = aq_resize( $pf_gal, 872, 310, true,true,true ); //resize & retain image proportions (soft crop)
                  		echo '<img src="' . $image . '"/>';
                  	}
                  ?>                                    
                  </div>
                  <?php } else if ($pf_link) { ?>
                    <?php 
                        if (is_youtube($pf_link)) {  
                            echo '<div class="video-container">'.wp_oembed_get($pf_link).'</div>';
                        } else if (is_vimeo($pf_link)) {
                            echo '<div class="video-container">'.wp_oembed_get($pf_link).'</div>';
                        } else { ?>
                            <div class="pf-single-image">
                            <?php if (function_exists('has_post_thumbnail') && has_post_thumbnail()) {?>
                              <img src="<?php echo $pf_single_image;?>" class="single-image" alt="" />
                            <?php } ?>
                            </div>
                        <?php } ?>
                  <?php } else { ?>
                    <div class="pf-single-image">
                    <?php if (function_exists('has_post_thumbnail') && has_post_thumbnail()) {?>
                      <img src="<?php echo $single_image;?>" class="single-image" alt="" />
                    <?php } ?>
                    </div>  
                  <?php } 
                ?>
            </div>
            
            <div class="single-pf-content">
              <h3><?php the_title();?></h3>
              <p><?php the_content();?></p>
            </div>
            
          </div>
          
          <!-- Portfolio Detail Content End -->
          <?php endwhile;?>
          <?php endif;?>
          
          <div class="clear"></div>
          <div class="random-portfolio">
          <?php imediapixel_get_related_portfolio($num=4,$title="Related Portfolio");?>
          </div>
          
        </div>
        <!-- Main Content Wrapper End -->
    
  <?php get_footer();?>