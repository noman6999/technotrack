        <?php
        global $post;
        $meta_prefix = "_imediapixel_";
        $page_sidebar = get_post_meta(get_the_ID(),$meta_prefix."page_sidebar",true);
        $page_sidebar_position = get_post_meta(get_the_ID(),$meta_prefix."page_sidebar_position",true);
        ?>                        
        
        <div id="sidebar-wrapper">
        <!-- Sidebar -->
            <?php if ($page_sidebar_position !="left") { ?>
                <div class="sidebar">
            <?php } else { ?>
                <div class="sidebar left">
            <?php } ?>
          <!-- Sidebar Box -->
          <?php          
            if(!empty($page_sidebar)) { 
                dynamic_sidebar( $page_sidebar );
            } else if (is_singular('post') || is_category() || is_archive() || is_search()) {
                if ( is_active_sidebar( 'blog-sidebar' ) ) { 
        			dynamic_sidebar( 'blog-sidebar' ); 
        		}		
            } else {
        		if ( is_active_sidebar( 'general-sidebar' )) { 
        			dynamic_sidebar( 'general-sidebar' ); 
        		}		
            }
            ?>                    
              <!-- Sidebar Box End -->
            </div>
            
            <?php if (is_singular('team')) { ?>
            <div class="sidebar">
                <div class="sidebar-box">
                    <div class="sidebartop"></div>
                    <div class="sidebarmain">
                      <div class="sidebarcontent">
                   	    <?php
                             $instance = array();
                             $instance['team_title'] = '<h4 class="sidebarheading">Meet the Team</h4>'; 
                             the_widget( 'Team_Widget',$instance); 
                        ?> 
                      </div>                  
                    </div>
                    <div class="sidebarbottom"></div>
                </div>
            </div>
            <?php
            }
              ?>
          </div>
        <!-- Sidebar End -->