        <?php global $ecobiz; ?>  
        <!-- Footer Box #1 -->
      <div class="footerbox footerbox-2col">
        <?php if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('bottom1')) { ?>
        <h4><?php echo __('Categories','ecobiz');?></h4>
        <ul>
          <?php wp_list_categories('title_li=&hide_empty=0');?> 
        </ul>
        <?php } ?>
      </div>
      
      
      <!-- Footer Box #3 -->
      <div class="footerbox  footerbox-2col box-last">
        <a href="<?php echo home_url();?>"><img src="<?php echo $footer_logo['url'];?>" alt="Footer Logo" class="alignleft"/></a>
        <div class="clear"></div>      
        <?php if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('bottom2')) { ?>
        <?php } ?>
        <ul class="social-links">
        <?php
            $twitter_id     = $ecobiz['twitter-id'];
            $linkedin_id    = $ecobiz['linkedin-id'];
            $facebook_id    = $ecobiz['facebook-id'];
            $flickr_id      = $ecobiz['flickr-id'];
            $skype_id       = $ecobiz['skype-id'];
            $youtube_id     = $ecobiz['youtube-id'];
        ?>
          <?php if ($twitter_id !="") { ?>
          <li>
            <a href="http://twitter.com/<?php echo $twitter_id;?>"><img src="<?php echo get_template_directory_uri();?>/images/twitter.png" alt="Twitter" /></a>
          </li>
          <?php } ?>
          <?php if ($facebook_id !="") { ?>
          <li>
            <a href="<?php echo $facebook_id;?>"><img src="<?php echo get_template_directory_uri();?>/images/facebook.png" alt="Facebook" /></a>
          </li>
          <?php } ?>
          <?php if ($linkedin_id !="") { ?>
          <li>
            <a href="<?php echo $linkedin_id;?>"><img src="<?php echo get_template_directory_uri();?>/images/linkedin.png" alt="Linkedin" /></a>
          </li>
          <?php } ?>
          <?php if ($skype_id !="") { ?>
          <li>
            <a href="skype:<?php echo $skype_id;?>?call"><img src="<?php echo get_template_directory_uri();?>/images/skype.png" alt="skype" /></a>
          </li>
          <?php } ?>
          <?php if ($flickr_id !="") { ?>
          <li>
            <a href="http://www.flickr.com/photos/<?php echo $flickr_id;?>"><img src="<?php echo get_template_directory_uri();?>/images/flickr.png" alt="Flickr" /></a>
          </li>
          <?php } ?>
          <?php if ($youtube_id !="") { ?>
          <li>
            <a href="http://www.youtube.com/user/<?php echo $youtube_id;?>"><img src="<?php echo get_template_directory_uri();?>/images/youtube.png" alt="youtube" /></a>
          </li>
          <?php } ?>
          <li><a href="<?php bloginfo('rss2_url');?>"><img src="<?php echo get_template_directory_uri();?>/images/rss.png" alt="RSS" /></a></li>
        </ul>
      </div>