<?php

/* Register Custom Post Type for Slideshow */
add_action('init', 'slideshow_post_type_init');

function slideshow_post_type_init() {
  $labels = array(
    'name' => __('Slideshow', 'post type general name','ecobiz'),
    'singular_name' => __('slideshow', 'post type singular name','ecobiz'),
    'add_new' => __('Add New', 'slideshow','ecobiz'),
    'add_new_item' => __('Add New slideshow','ecobiz'),
    'edit_item' => __('Edit slideshow','ecobiz'),
    'new_item' => __('New slideshow','ecobiz'),
    'view_item' => __('View slideshow','ecobiz'),
    'search_items' => __('Search slideshow','ecobiz'),
    'not_found' =>  __('No slideshow found','ecobiz'),
    'not_found_in_trash' => __('No slideshow found in Trash','ecobiz'), 
    'parent_item_colon' => ''
  );
  $args = array(
    'labels' => $labels,
    'public' => true,
    'publicly_queryable' => true,
    'show_ui' => true, 
    'rewrite' => true,
    'query_var' => true,
    'capability_type' => 'post',
    'hierarchical' => false,
    'show_in_nav_menus' => false,
    'menu_position' => 996,
    'supports' => array(
      'title',
      'editor',
      'thumbnail',
      'excerpt'  
    )
  );
  register_post_type('slideshow',$args);
}

register_taxonomy("slideshow_category", 
  	array("slideshow"), 
  	array( "hierarchical" => true, 
  			"label" => __("Slideshow Categories",'ecobiz'), 
  			"singular_label" => __("Slideshow Categories",'ecobiz'), 
  			"rewrite" => true,
  			"query_var" => true,
        "rewrite" => array(
          "slug" => "slideshow_category"
        )				    			
  		));

      
      
/* Register Custom Post Type for Portfolio */
add_action('init', 'portfolio_post_type_init');
function portfolio_post_type_init() {
  $labels = array(
    'name' => __('Portfolio', 'post type general name','ecobiz'),
    'singular_name' => __('portfolio', 'post type singular name','ecobiz'),
    'add_new' => __('Add New', 'portfolio','ecobiz'),
    'add_new_item' => __('Add New portfolio','ecobiz'),
    'edit_item' => __('Edit portfolio','ecobiz'),
    'new_item' => __('New portfolio','ecobiz'),
    'view_item' => __('View portfolio','ecobiz'),
    'search_items' => __('Search portfolio','ecobiz'),
    'not_found' =>  __('No portfolio found','ecobiz'),
    'not_found_in_trash' => __('No portfolio found in Trash','ecobiz'), 
    'parent_item_colon' => ''
  );
  $args = array(
    'labels' => $labels,
    'public' => true,
    'publicly_queryable' => true,
    'show_ui' => true, 
    'rewrite' => true,
    'query_var' => true,
    'capability_type' => 'post',
    'hierarchical' => false,
    'show_in_nav_menus' => false,
    'menu_position' => 997,
    'rewrite' => array(
      'slug' => 'portfolio_item',
      'with_front' => FALSE,
    ),    
    'supports' => array(
      'title',
      'editor',
      'thumbnail',
      'excerpt'
    )
  );

  register_post_type('portfolio',$args);
}

register_taxonomy("portfolio_category", 
			    	array("portfolio"), 
			    	array( "hierarchical" => true, 
			    			"label" => __("Portfolio Categories",'ecobiz'), 
			    			"singular_label" => __("Portfolio Categories",'ecobiz'), 
			    			"rewrite" => true,
			    			"query_var" => true,
                "rewrite" => array(
                  "slug" => "portfolio_category"
                )
			    		));  
			
      
/* Register Custom Post Type for Staff */
add_action('init', 'team_post_type_init');

function team_post_type_init() {
  $labels = array(
    'name' => __('Team', 'post type general name','ecobiz'),
    'singular_name' => __('Team', 'post type singular name','ecobiz'),
    'add_new' => __('Add New', 'ecobiz'),
    'add_new_item' => __('Add New Team','ecobiz'),
    'edit_item' => __('Edit Team','ecobiz'),
    'new_item' => __('New Team','ecobiz'),
    'view_item' => __('View Team','ecobiz'),
    'search_items' => __('Search Team','ecobiz'),
    'not_found' =>  __('No feam found','ecobiz'),
    'not_found_in_trash' => __('No team found in trash','ecobiz'), 
    'parent_item_colon' => ''
  );
  $args = array(
    'labels' => $labels,
    'public' => true,
    'publicly_queryable' => true,
    'show_ui' => true, 
    'rewrite' => true,
    'query_var' => true,
    'capability_type' => 'post',
    'hierarchical' => false,
    'show_in_nav_menus' => false,
    'menu_position' => 1000,
    'rewrite' => array(
      'slug' => 'team_detail',
      'with_front' => false,
    ),
    'supports' => array(
      'title',
      'editor',
      'thumbnail',
      'excerpt' 
    )
  );
  register_post_type('team',$args);
}

/* Register Custom Post Type for Testimonials */
add_action('init', 'testimonial_post_type_init');

function testimonial_post_type_init() {
  $labels = array(
    'name' => __('Testimonial', 'post type general name','ecobiz'),
    'singular_name' => __('Testimonial', 'post type singular name','ecobiz'),
    'add_new' => __('Add New', 'Testimonial','ecobiz'),
    'add_new_item' => __('Add New Testimonial','ecobiz'),
    'edit_item' => __('Edit Testimonial','ecobiz'),
    'new_item' => __('New Testimonial','ecobiz'),
    'view_item' => __('View Testimonial','ecobiz'),
    'search_items' => __('Search Testimonial','ecobiz'),
    'not_found' =>  __('No Testimonial found','ecobiz'),
    'not_found_in_trash' => __('No Testimonial found in Trash','ecobiz'), 
    'parent_item_colon' => ''
  );
  $args = array(
    'labels' => $labels,
    'public' => true,
    'publicly_queryable' => true,
    'show_ui' => true, 
    'rewrite' => true,
    'query_var' => true,
    'capability_type' => 'post',
    'hierarchical' => false,
    'show_in_nav_menus' => false,
    'menu_position' => 1000,
    'supports' => array(
      'title',
      'editor',
      'thumbnail',
      'excerpt'
    )
  );
  register_post_type('testimonial',$args);
}