// Creates a new plugin
(function() {
    tinymce.create('tinymce.plugins.imediapixelShortcodes', {

        init : function(ed, url) {
			// Add a button that opens a window
			ed.addButton('columns', {
				type: 'menubutton',
				text: 'Shortcodes',
				icon: false,
				onclick : function(e) {},
				menu: [
				{
                    text: 'Columns',
					
						//sub menu columns
						menu: [
							{
								text: 'One Half',
								onclick: function(){
									tinyMCE.activeEditor.selection.setContent('[col_12]content here[/col_12]'+'<br/>'+'[col_12_last]content here[/col_12_last]');
								}
							},
							{
								text: 'One Third',
								onclick: function(){
									tinyMCE.activeEditor.selection.setContent('[col_13]content here[/col_13]'+'<br/>'+'[col_13]content here[/col_13]'+'<br/>'+'[col_13_last]content here[/col_13_last]\r');
								}
							},
							{
								text: 'One Fourth',
								onclick: function(){
									tinyMCE.activeEditor.selection.setContent('[col_14]content here[/col_14]'+'<br/>'+'[col_14]content here[/col_14]'+'<br/>'+'[col_14]content here[/col_14]'+'<br/>'+'[col_14_last]content here[/col_14_last]');
								}
							},
							{
								text: 'Two Third',
								onclick: function(){
									tinyMCE.activeEditor.selection.setContent('[col_23]content here[/col_23]'+'<br/>'+'[col_13_last]content here[/col_13_last]');
								}
							},
							{
								text: 'Three Foruth',
								onclick: function(){
									tinyMCE.activeEditor.selection.setContent('[col_34]content here[/col_34]'+'<br/>'+'[col_14_last]content here[/col_14_last]');
								}
							},
	 
						]
						//end sub menu columns
                }, 
				//end columns
           
				
                {
                    text: 'Elements',
					
						//sub menu elements
						menu: [
              {
								text: 'Dropcap',
								onclick: function(){
									tinyMCE.activeEditor.selection.setContent('[dropcap]L[/dropcap]orem ipsum dolor sit amet');
								}
							},
							{
								text: 'Pullquote Left',
								onclick: function(){
									tinyMCE.activeEditor.selection.setContent('[pullquote_left]text here[/pullquote_left]');
								}
							},
							{
								text: 'Pullquote Right',
								onclick: function(){
									tinyMCE.activeEditor.selection.setContent('[pullquote_right]text here[/pullquote_right]');
								}
							},
							{
								text: 'Image',
								onclick: function(){
									tinyMCE.activeEditor.selection.setContent('[image source="" align=""]');
								}
							},
							{
								text: 'Button',
								onclick: function(){
									tinyMCE.activeEditor.selection.setContent('[button link="" size="" color=""]your text here[/button]');
								}
							},
                            {
								text: 'Info Box',
								onclick: function(){
									tinyMCE.activeEditor.selection.setContent('[info]text here[/info]');
								}
							},
                            {
								text: 'Success Box',
								onclick: function(){
									tinyMCE.activeEditor.selection.setContent('[success]text here[/success]');
								}
							},
                            {
								text: 'Warning Box',
								onclick: function(){
									tinyMCE.activeEditor.selection.setContent('[warning]text here[/warning]');
								}
							},
                            {
								text: 'Error Box',
								onclick: function(){
									tinyMCE.activeEditor.selection.setContent('[error]text here[/error]');
								}
							},
						]
						//end sub menu elements
                },
				//end elements
				
                {
                    text: 'List Style',
					
						//sub menu list style
						menu: [
							{
								text: 'Bullet',
								onclick: function(){
									tinyMCE.activeEditor.selection.setContent('[bulletlist]<ul>\r<li>Item #1</li>\r<li>Item #2</li>\r<li>Item #3</li>\r</ul>[/bulletlist]');
								}
							},
							{
								text: 'Star',
								onclick: function(){
									tinyMCE.activeEditor.selection.setContent('[starlist]<ul>\r<li>Item #1</li>\r<li>Item #2</li>\r<li>Item #3</li>\r</ul>[/starlist]');
								}
							},
							{
								text: 'Check',
								onclick: function(){
									tinyMCE.activeEditor.selection.setContent('[checklist]<ul>\r<li>Item #1</li>\r<li>Item #2</li>\r<li>Item #3</li>\r</ul>[/checklist]');
								}
							},
							{
								text: 'Arrow',
								onclick: function(){
									tinyMCE.activeEditor.selection.setContent('[arrowlist]<ul>\r<li>Item #1</li>\r<li>Item #2</li>\r<li>Item #3</li>\r</ul>[/arrowlist]');
								}
							}
						]
						//end sub menu list style
                },
				//end list style
				
                {
                    text: 'Content',
					
						//sub menu content
						menu: [
							{
								text: 'Page List',
								onclick: function(){
									tinyMCE.activeEditor.selection.setContent('[pagelist parent_page="" num=6 orderby="date" style="3col" readmore_text="Read more &raquo;"]');
								}
							},
                            {
								text: 'Blog List',
								onclick: function(){
									tinyMCE.activeEditor.selection.setContent('[bloglist cat="category_id" num=-1]');
								}
							},
                            {
								text: 'Team List',
								onclick: function(){
									tinyMCE.activeEditor.selection.setContent('[teamlist column=2|3|4 showpost=-1]');
								}
							},
                            {
								text: 'Testimonial List',
								onclick: function(){
									tinyMCE.activeEditor.selection.setContent('[testimonial_list column=2|3|4 showpost=-1]');
								}
							},
                            {
								text: 'Tabs',
								onclick: function(){
									tinyMCE.activeEditor.selection.setContent('[tabs]<br/>[tab title="your title here"]your content here[/tab]<br/>[tab title="your title here"]your content here[/tab]<br/>[tab title="your title here"]your content here[/tab]<br/>[/tabs]');
								}
							},
							{
								text: 'Toggle',
								onclick: function(){
									tinyMCE.activeEditor.selection.setContent('[toggle title="your title here"]your content here[/toggle]<br/>[toggle title="your title here"]your content here[/toggle]<br/>[toggle title="your title here"]your content here[/toggle]');
								}
							},
                            {
								text: 'Google Map',
								onclick: function(){
									tinyMCE.activeEditor.selection.setContent('[gmap width="" height="" latitude="" longitude="" zoom="" html="" popup="" marker="yes"]');
								}
							},
                            {
								text: 'Video',
								onclick: function(){
									tinyMCE.activeEditor.selection.setContent('[video url="" width="" height=""]');
								}
							},
                            {
								text: 'Icon',
								onclick: function(){
									tinyMCE.activeEditor.selection.setContent('[icon name="icon-name" size="standard|icon-2x|icon-3x|icon-4x" align="left|right" color="#ffffff"]');
								}
							},
						]
						//end sub menu content
                }
				//end content
            ]
			//end menu
				
			});
			
			
        }

    });

    tinymce.PluginManager.add('imediapixelShortcodes', tinymce.plugins.imediapixelShortcodes);

})();