<?php
global $ecobiz;

/*-----------------------------------------------------------------------------------*/
/*	Register Widget   
/*-----------------------------------------------------------------------------------*/
if ( function_exists('register_sidebar') )
  register_sidebar(array(
    'name'          =>'Homepage Sidebar',
    'id'            => 'homepage-sidebar',
    'description'   => __( 'Widget place for homepage.', 'ecobiz' ),
    'before_widget' => '<div class="sidebar-box"><div class="sidebartop"></div><div class="sidebarmain"><div id="%1$s" class="sidebarcontent %2$s">',
    'after_widget'  => '</div></div><div class="sidebarbottom"></div></div>',
    'before_title'  => '<h4 class="sidebarheading">',
    'after_title'   => '</h4>'
  ));
if ( function_exists('register_sidebar') )
  register_sidebar(array(
    'name'=>'Site Features Box',
    'id'            => 'feature-boxes',
    'description'   => __( 'Located below the slideshow section at the homepage.', 'ecobiz' ),
    'before_widget' => '<li>',
    'after_widget'  => '</li>',
    'before_title'  => '',
    'after_title'   => ''
));
if ( function_exists('register_sidebar') )
  register_sidebar(array(
    'name'=>'General Sidebar',
    'id'            => 'general-sidebar',
    'before_widget' => '<div class="sidebar-box"><div class="sidebartop"></div><div class="sidebarmain"><div id="%1$s" class="sidebarcontent %2$s">',
    'after_widget'  => '</div></div><div class="sidebarbottom"></div></div>',
    'before_title'  => '<h4 class="sidebarheading">',
    'after_title'   => '</h4>'
  ));
if ( function_exists('register_sidebar') )
  register_sidebar(array(
    'name'=>'Blog Sidebar',
    'id'            => 'blog-sidebar',
    'before_widget' => '<div class="sidebar-box"><div class="sidebartop"></div><div class="sidebarmain"><div id="%1$s" class="sidebarcontent %2$s">',
    'after_widget'  => '</div></div><div class="sidebarbottom"></div></div>',
    'before_title'  => '<h4 class="sidebarheading">',
    'after_title'   => '</h4>'
  ));
if ( function_exists('register_sidebar') )
  register_sidebar(array(
    'name'=>'bottom1',
    'id'            => 'bottom1',
    'before_widget' => '<div id="%1$s" class="%2$s">',
    'after_widget'  => '</div>',
    'before_title'  => '<h4>',
    'after_title'   => '</h4>'
  ));        
if ( function_exists('register_sidebar') )
  register_sidebar(array(
    'name'=>'bottom2',
    'id'            => 'bottom2',
    'before_widget' => '<div id="%1$s" class="%2$s">',
    'after_widget'  => '</div>',
    'before_title'  => '<h4>',
    'after_title'   => '</h4>'
  ));        
if ( function_exists('register_sidebar') )
  register_sidebar(array(
    'name'=>'bottom3',
    'id'            => 'bottom3',
    'before_widget' => '<div id="%1$s" class="%2$s">',
    'after_widget'  => '</div>',
    'before_title'  => '<h4>',
    'after_title'   => '</h4>'
  ));        
if ( function_exists('register_sidebar') )
  register_sidebar(array(
    'name'=>'bottom4',
    'id'            => 'bottom4',
    'before_widget' => '<div id="%1$s" class="%2$s">',
    'after_widget'  => '</div>',
    'before_title'  => '<h4>',
    'after_title'   => '</h4>'
  ));            

if(isset($ecobiz['sidebar-widget']) && $ecobiz['sidebar-widget']!=""){
   $sidebars = $ecobiz['sidebar-widget'];		
	foreach ( $sidebars as $sidebar) {
	  register_sidebar(array(
		'name'              => $sidebar,
		'id'          => $sidebar,
		'description'       => __('A Custom sidebar created from Appearance >> Theme Options >> Sidebars.', 'ecobiz-admin'),
		'before_widget' => '<div class="sidebar-box"><div class="sidebartop"></div><div class="sidebarmain"><div id="%1$s" class="sidebarcontent %2$s">',
        'after_widget'  => '</div></div><div class="sidebarbottom"></div></div>',
        'before_title'  => '<h4 class="sidebarheading">',
        'after_title'   => '</h4>'
	  ));
	}     
}

/*-----------------------------------------------------------------------------------*/
/*	Page to box 
/*-----------------------------------------------------------------------------------*/
class PageBox_Widget extends WP_Widget {
  function PageBox_Widget() {
    $widgets_opt = array('description'=>'Display pages as small box in sidebar');
    parent::WP_Widget(false,$name= "ECOBIZ - Page to Box",$widgets_opt);
  }
  
  function form($instance) {
    global $post;
    
    $pageid = esc_attr($instance['pageid']);
    $opt_thumbnail = esc_attr($instance['opt_thumbnail']);
    $pageexcerpt = esc_attr($instance['pageexcerpt']);
    
		$pages = get_pages();
		$listpages = array();
		foreach ($pages as $pagelist ) {
		   $listpages[$pagelist->ID] = $pagelist->post_title;
		}
  ?>  
	 <p><label>Please select the page
		<select  name="<?php echo $this->get_field_name('pageid'); ?>"  id="<?php echo $this->get_field_id('pageid'); ?>" >
			<?php foreach ($listpages as $opt => $val) { ?>
		<option value="<?php echo $opt ;?>" <?php if ( $pageid  == $opt) { echo ' selected="selected" '; }?>><?php echo $val; ?></option>
		<?php } ?>
		</select>
		</label></p>
  <p>
		<input class="checkbox" type="checkbox" <?php if ($opt_thumbnail == "on") echo "checked";?> id="<?php echo $this->get_field_id('opt_thumbnail'); ?>" name="<?php echo $this->get_field_name('opt_thumbnail'); ?>" />
		<label for="<?php echo $this->get_field_id('opt_thumbnail'); ?>"><small>display thumbnail?</small></label><br />
    </p>
    <p><label for="pageexcerpt">Number of words for excerpt :
  		<input id="<?php echo $this->get_field_id('pageexcerpt'); ?>" name="<?php echo $this->get_field_name('pageexcerpt'); ?>" type="text" class="widefat" value="<?php echo $pageexcerpt;?>" /></label></p>  
    <?php    
  } 
  
  function update($new_instance, $old_instance) {
    return $new_instance;
  }
  
  function widget( $args, $instance ) {
    global $post;
    
    extract($args);
    
    $pageid = apply_filters('pageid',$instance['pageid']);
    $opt_thumbnail = apply_filters('opt_thumbnail',$instance['opt_thumbnail']);
    $pageexcerpt = apply_filters('pageexcerpt',$instance['pageexcerpt']);
    if ($pageexcerpt =="") $pageexcerpt = 20;
  
    $pagelist = new WP_Query('post_type=page&page_id='.$pageid);
    
    echo $before_widget;
    
    while ($pagelist->have_posts()) : $pagelist->the_post();
    
    $thumb   = get_post_thumbnail_id();
    $img_url = wp_get_attachment_url( $thumb,'full' ); //get full URL to image (use "large" or "medium" if the images too big)
    $image   = aq_resize( $img_url, 182, 78, true ); //resize & crop the image
    
    $title = $before_title.get_the_title().$after_title;
    
    echo $title;    
    ?>
     <?php if ($opt_thumbnail == "on") { ?>
        <div class="boximg2">
        <?php if (function_exists('has_post_thumbnail') && has_post_thumbnail()) {?>
        <img src="<?php echo $image;?>" class="boximg-pad aligncenter" alt="" />
        <?php } ?>
        </div>
        <?php 
        }  
      ?>
    <p><?php echo the_excerpt();?></p>
    <p><a href="<?php the_permalink();?>"  class="button white small"> Read more &raquo;</a></p>
    <?php      
    endwhile;
    echo $after_widget;
  } 
}

add_action('widgets_init', create_function('', 'return register_widget("PageBox_Widget");'));


/*-----------------------------------------------------------------------------------*/
/*	Latest News Widget  
/*-----------------------------------------------------------------------------------*/
class LatestNews_Widget extends WP_Widget {
  
  function LatestNews_Widget() {
    $widgets_opt = array('description'=>'ECOBIZ Latest News Theme Widget');
    parent::WP_Widget(false,$name= "ECOBIZ - Latest News",$widgets_opt);
  }
  
  function form($instance) {
    global $post;
    
    $blogpage = esc_attr($instance['blogpage']);
    $newstitle = esc_attr($instance['newstitle']);
    $numnews = esc_attr($instance['numnews']);
    
  ?>
    <p><label for="newstitle">Title:
  		<input id="<?php echo $this->get_field_id('newstitle'); ?>" name="<?php echo $this->get_field_name('newstitle'); ?>" type="text" class="widefat" value="<?php echo $newstitle;?>" /></label></p>  
	 <p><label for="numnews">Number to display:
  		<input id="<?php echo $this->get_field_id('numnews'); ?>" name="<?php echo $this->get_field_name('numnews'); ?>" type="text" class="widefat" value="<?php echo $numnews;?>" /></label></p>
     <p><label for="blogpage">Your Blog Page:
  		<input id="<?php echo $this->get_field_id('blogpage'); ?>" name="<?php echo $this->get_field_name('blogpage'); ?>" type="text" class="widefat" value="<?php echo $blogpage;?>" /></label></p>        
    <?php    
  } 
  
  function update($new_instance, $old_instance) {
    return $new_instance;
  }
  
  function widget( $args, $instance ) {
    global $post;
    
    extract($args);
    
    $blogpage = apply_filters('blogpage',$instance['blogpage']);
    $newstitle = apply_filters('newstitle',$instance['newstitle']);
    $numnews = apply_filters('numnews',$instance['numnews']);    
    
    if ($numnews == "") $numnews = 4;
    if ($newstitle == "") $newstitle = "Latest News";
    
    echo $before_widget;
    $title = $before_title.$newstitle.$after_title;
    imediapixel_latestnews($numnews,$title);
    ?>
   <div class="clear"></div>
   <a href="<?php echo $blogpage;?>" class="button-more"><?php echo __('View All News','ecobiz');?></a>
   <?php
   wp_reset_query();    
   echo $after_widget;
  } 
}

add_action('widgets_init', create_function('', 'return register_widget("LatestNews_Widget");'));


/*-----------------------------------------------------------------------------------*/
/*	Latest Works
/*-----------------------------------------------------------------------------------*/
class LatestWorks_Widget extends WP_Widget {
  
  function LatestWorks_Widget() {
    $widgets_opt = array('description'=>'Display latest portfolio item randomly in sidebar');
    parent::WP_Widget(false,$name= "ECOBIZ - Latest Work",$widgets_opt);
  }
  
  function form($instance) {
    global $post;
    
    $pftitle = esc_attr($instance['pftitle']);
    
    
  ?>
    <p><label for="pftitle">Title:
  		<input id="<?php echo $this->get_field_id('pftitle'); ?>" name="<?php echo $this->get_field_name('pftitle'); ?>" type="text" class="widefat" value="<?php echo $pftitle;?>" /></label></p>
    <?php    
  } 
  
  function update($new_instance, $old_instance) {
    return $new_instance;
  }
  
  function widget( $args, $instance ) {
    global $post;
    
    extract($args);
    
    $pftitle = apply_filters('pftitle',$instance['pftitle']);
    
    if ($pftitle == "") $pftitle = __("Latest Work",'ecobiz');
    
    echo $before_widget;
    $title = $before_title.$pftitle.$after_title;
    imediapixel_latestworks($num=1,$title)
    ?>
   <?php
   wp_reset_query();    
   echo $after_widget;
  } 
}

add_action('widgets_init', create_function('', 'return register_widget("LatestWorks_Widget");'));

/*-----------------------------------------------------------------------------------*/
/*	Testimonial Widget   
/*-----------------------------------------------------------------------------------*/
class Testimonial_Widget extends WP_Widget {
  function Testimonial_Widget() {
    $widgets_opt = array('description'=>'ECOBIZ Testimonial Widget');
    parent::WP_Widget(false,$name= "ECOBIZ - Testimonial",$widgets_opt);
  }
  
  function form($instance) {
    global $post;
    
    $testititle = esc_attr($instance['testititle']);
    $numtesti = esc_attr($instance['numtesti']);
    
  ?>
    <p><label for="testititle">Title:
  		<input id="<?php echo $this->get_field_id('testititle'); ?>" name="<?php echo $this->get_field_name('testititle'); ?>" type="text" class="widefat" value="<?php echo $testititle;?>" /></label></p>  
	<p><label for="numtesti">Number to display:
  		<input id="<?php echo $this->get_field_id('numtesti'); ?>" name="<?php echo $this->get_field_name('numtesti'); ?>" type="text" class="widefat" value="<?php echo $numtesti;?>" /></label></p>
    <?php    
  } 
  
  function update($new_instance, $old_instance) {
    return $new_instance;
  }
  
  function widget( $args, $instance ) {
    global $post;
    
    extract($args);
    
    $testititle = apply_filters('testititle',$instance['testititle']);
    $numtesti = apply_filters('numtesti',$instance['numtesti']);    
        
    if ($numtesti == "") $numtesti = 1;
    if ($testititle == "") $testititle = "Testimonials";
    
    echo $before_widget;
    $title = $before_title.$testititle.$after_title;
    echo imediapixel_testimonial_list("",$numtesti,$title);
    echo $after_widget;
  } 
}

add_action('widgets_init', create_function('', 'return register_widget("Testimonial_Widget");'));

/*-----------------------------------------------------------------------------------*/
/*	Team Widget   
/*-----------------------------------------------------------------------------------*/
class Team_Widget extends WP_Widget {
  function Team_Widget() {
    $widgets_opt = array('description'=>'ECOBIZ Team Widget');
    parent::WP_Widget(false,$name= "ECOBIZ - Team List",$widgets_opt);
  }
  
  function form($instance) {
    global $post;
    
    $team_title = esc_attr($instance['team_title']);
    $team_num = esc_attr($instance['team_num']);
    
  ?>
    <p><label for="testititle">Title:
  		<input id="<?php echo $this->get_field_id('team_title'); ?>" name="<?php echo $this->get_field_name('team_title'); ?>" type="text" class="widefat" value="<?php echo $team_title;?>" /></label></p>  
	<p><label for="team_num">Number to display:
  		<input id="<?php echo $this->get_field_id('team_num'); ?>" name="<?php echo $this->get_field_name('team_num'); ?>" type="text" class="widefat" value="<?php echo $team_num;?>" /></label></p>
    <?php    
  } 
  
  function update($new_instance, $old_instance) {
    return $new_instance;
  }
  
  function widget( $args, $instance ) {
    global $post;
    
    extract($args);
    
    $team_title = apply_filters('team_title',$instance['team_title']);
    $team_num = apply_filters('team_num',$instance['team_num']);    
        
    if ($team_num == "") $team_num = 1;
    if ($team_title == "") $team_title = "Our Team";
    
    echo $before_widget;
    $title = $before_title.$team_title.$after_title;
    echo imediapixel_related_team_function($title,$team_num);
    echo $after_widget;
  } 
}

add_action('widgets_init', create_function('', 'return register_widget("Team_Widget");'));


/*-----------------------------------------------------------------------------------*/
/*	Post to Box
/*-----------------------------------------------------------------------------------*/
class PostBox_Widget extends WP_Widget {
  function PostBox_Widget() {
    $widgets_opt = array('description'=>'Display Posts as small box in sidebar');
    parent::WP_Widget(false,$name= "ECOBIZ - Post to Box",$widgets_opt);
  }
  
  function form($instance) {
    global $post;
    
    $postid = esc_attr($instance['postid']);
    $opt_thumbnail = esc_attr($instance['opt_thumbnail']);
    $postexcerpt = esc_attr($instance['postexcerpt']);
    
		$ECOBIZposts = get_posts('numberposts=-1')
		?>  
	<p><label>Please select post display
			<select  name="<?php echo $this->get_field_name('postid'); ?>"  id="<?php echo $this->get_field_id('postid'); ?>" >
				<?php foreach ($ECOBIZposts as $post) { ?>
			<option value="<?php echo $post->ID;?>" <?php if ( $postid  ==  $post->ID) { echo ' selected="selected" '; }?>><?php echo  the_title(); ?></option>
			<?php } ?>
			</select>
	</label></p>
  <p>
		<input class="checkbox" type="checkbox" <?php if ($opt_thumbnail == "on") echo "checked";?> id="<?php echo $this->get_field_id('opt_thumbnail'); ?>" name="<?php echo $this->get_field_name('opt_thumbnail'); ?>" />
		<label for="<?php echo $this->get_field_id('opt_thumbnail'); ?>"><small>display thumbnail?</small></label><br />
    </p>
    <p><label for="postexcerpt">Number of words for excerpt :
  		<input id="<?php echo $this->get_field_id('postexcerpt'); ?>" name="<?php echo $this->get_field_name('postexcerpt'); ?>" type="text" class="widefat" value="<?php echo $postexcerpt;?>" /></label></p>  
    <?php    
  } 
  
  function update($new_instance, $old_instance) {
    return $new_instance;
  }
  
  function widget( $args, $instance ) {
    global $post;
    
    extract($args);
    
    $postid = apply_filters('postid',$instance['postid']);
    $opt_thumbnail = apply_filters('opt_thumbnail',$instance['opt_thumbnail']);
    $postexcerpt = apply_filters('postexcerpt',$instance['postexcerpt']);
    if ($postexcerpt =="") $postexcerpt = 20;
    
    echo $before_widget;
    $postlist = new WP_Query('p='.$postid);
    
    while ($postlist->have_posts()) : $postlist->the_post();
    
    $thumb   = get_post_thumbnail_id();
    $img_url = wp_get_attachment_url( $thumb,'full' ); //get full URL to image (use "large" or "medium" if the images too big)
    $image   = aq_resize( $img_url, 182, 78, true,true,true); //resize & crop the image
    
    $title = $before_title.get_the_title().$after_title;
    
    echo $title;
    ?>
      <?php if ($opt_thumbnail == "on") { ?>
        <div class="boximg2">
        <?php if (function_exists('has_post_thumbnail') && has_post_thumbnail()) {?>
        <img src="<?php echo $image;?>" class="boximg-pad aligncenter" alt="" />
        <?php } ?>
        </div>
        <?php 
        }  
      ?>
    <p><?php echo the_excerpt();?><a href="<?php the_permalink();?>"  class="button white medium"> <?php echo __('Read more &raquo;','ecobiz');?></a></p>
    <?php   endwhile;
    echo $after_widget;
  } 
}

add_action('widgets_init', create_function('', 'return register_widget("PostBox_Widget");'));


/*-----------------------------------------------------------------------------------*/
/*	Contact Info Widget 
/*-----------------------------------------------------------------------------------*/
class OfficeAdress_Widget extends WP_Widget {
  function OfficeAdress_Widget() {
    $widgets_opt = array('description'=>'display your contact information');
    parent::WP_Widget(false,$name= "ECOBIZ - Contact Info",$widgets_opt);
  }
  
  function form($instance) {
    global $post;
    
    $contact_title = esc_attr($instance['contact_title']);
  ?>
  <p><label for="contact_title"><?php echo __('Title','ecobiz');?>:
  		<input id="<?php echo $this->get_field_id('contact_title'); ?>" name="<?php echo $this->get_field_name('contact_title'); ?>" type="text" class="widefat" value="<?php echo $contact_title;?>"/></label></p>
    <?php    
  } 
  
  function update($new_instance, $old_instance) {
		
		return $new_instance;
  }
  
  function widget( $args, $instance ) {
    global $post, $ecobiz;
    
    extract($args);
    
    $contact_title = apply_filters('contact_title',$instance['contact_title']);
    
    echo $before_widget;
    
    echo $before_title.$contact_title.$after_title;
    
    ?>
    <ul class="addresslist">
      <?php
         $info_address = $ecobiz['office-address'];
        $info_phone = $ecobiz['phone-number'];
        $info_fax = $ecobiz['faximile-number'];
        $info_email = $ecobiz['email-address'];
      ?>        
        <?php if ($info_address) { ?><li><?php echo $info_address;?></li><?php } ?>
      <?php if ($info_phone) { ?><li><strong><?php echo __('Phone','ecobiz');?></strong> : <?php echo $info_phone;?></li><?php } ?>
      <?php if ($info_fax) { ?>
        <li><strong><?php echo __('Fax','ecobiz');?></strong> : <?php echo $info_fax;?></li><?php 
        } ?>
      <?php if ($info_email) { ?><li><strong><?php echo __('Email','ecobiz');?></strong> : <a href="mailto:<?php echo $info_email ? $info_email : "#";?>"><?php echo $info_email;?></a></li><?php } ?>
      </ul>    

   <?php
    echo $after_widget;
  } 
}

add_action('widgets_init', create_function('', 'return register_widget("OfficeAdress_Widget");'));


/*-----------------------------------------------------------------------------------*/
/*	Search box  
/*-----------------------------------------------------------------------------------*/
class searchbox_Widget extends WP_Widget {
  function searchbox_Widget () {
    $widgets_opt = array('description'=>'ECOBIZ search box widget');
    parent::WP_Widget(false,$name= "ECOBIZ - Search Box",$widgets_opt);
  }
  
  function form($instance) {
    global $post;
    
    $searchboxtitle = esc_attr($instance['searchboxtitle']);         
  ?>
    <p><label for="bannertitle">Title:
  		<input id="<?php echo $this->get_field_id('searchboxtitle'); ?>" name="<?php echo $this->get_field_name('searchboxtitle'); ?>" type="text" class="widefat" value="<?php echo $searchboxtitle;?>" /></label></p>       		
    <?php    
  } 
  
  function update($new_instance, $old_instance) {
    return $new_instance;
  }
  
  function widget( $args, $instance ) {
    global $post;
    
    extract($args);
    
    echo $before_widget;
    
    $searchboxtitle = apply_filters('searchboxtitle',$instance['searchboxtitle']);
    
    if ($searchboxtitle == "") $searchboxtitle = __("Search",'ecobiz');
    
    echo $title = $before_title.$searchboxtitle.$after_title;
    
    get_template_part( 'searchbox','ECOBIZ search box' );
    
    echo $after_widget; 
  } 
}

add_action('widgets_init', create_function('', 'return register_widget("searchbox_Widget");'));

/*-----------------------------------------------------------------------------------*/
/*	Twitter Widget
/*-----------------------------------------------------------------------------------*/
class Twitter_Widget extends WP_Widget {
  function Twitter_Widget() {
    $widgets_opt = array('description'=>'display your latest twitter feed');
    parent::WP_Widget(false,$name= "ECOBIZ - Twitter Update",$widgets_opt);
  }
  
  function form($instance) {
    global $post;
    
    $twittertitle = esc_attr($instance['twittertitle']);
    $twitternum = esc_attr($instance['twitternum']);

  ?>
    <p><label for="twittertitle">Title:
  		<input id="<?php echo $this->get_field_id('twittertitle'); ?>" name="<?php echo $this->get_field_name('twittertitle'); ?>" type="text" class="widefat" value="<?php echo $twittertitle;?>" /></label></p>
    <p><label for="twitternum">Number to dispay:
  		<input id="<?php echo $this->get_field_id('twitternum'); ?>" name="<?php echo $this->get_field_name('twitternum'); ?>" type="text" class="widefat" value="<?php echo $twitternum;?>" /></label></p>                            
	  <?php    
  } 
  
  function update($new_instance, $old_instance) {
    return $new_instance;
  }
  
  function widget( $args, $instance ) {
    global $post;
    
    extract($args);
    
    echo $before_widget;
    
    $twittertitle = apply_filters('twittertitle',$instance['twittertitle']);
    $twitternum = apply_filters('twitternum',$instance['twitternum']);
       
    if ($twittertitle =="") $twittertitle = __("Twitter Update!",'ecobiz');
    
    imediapixel_twitter_feed($twittertitle,$twitternum);
    
    echo $after_widget;
  } 
}

add_action('widgets_init', create_function('', 'return register_widget("Twitter_Widget");'));

/*-----------------------------------------------------------------------------------*/
/*	Flickr Widget 
/*-----------------------------------------------------------------------------------*/
class Flickr_Widget extends WP_Widget {
  function Flickr_Widget() {
    $widgets_opt = array('description'=>'display your latest twitter feed');
    parent::WP_Widget(false,$name= "ECOBIZ - Flickr Gallery",$widgets_opt);
  }
  
  function form($instance) {
    global $post;
    
    $flickrtitle = esc_attr($instance['flickrtitle']);
    $flickrnum = esc_attr($instance['flickrnum']);

  ?>
    <p><label for="flickrtitle">Title:
  		<input id="<?php echo $this->get_field_id('flickrtitle'); ?>" name="<?php echo $this->get_field_name('flickrtitle'); ?>" type="text" class="widefat" value="<?php echo $flickrtitle;?>" /></label></p>
    <p><label for="flickrnum">Number to dispay:
  		<input id="<?php echo $this->get_field_id('flickrnum'); ?>" name="<?php echo $this->get_field_name('flickrnum'); ?>" type="text" class="widefat" value="<?php echo $flickrnum;?>" /></label></p>                            
	  <?php    
  } 
  
  function update($new_instance, $old_instance) {
    return $new_instance;
  }
  
  function widget( $args, $instance ) {
    global $post, $ecobiz;
    
    extract($args);
    
    echo $before_widget;
    
    $flickrtitle = apply_filters('flickrtitle',$instance['flickrtitle']);
    $flickrnum = apply_filters('flickrnum',$instance['flickrnum']);
    
    if ($flickrtitle =="") $flickrtitle = __("Flickr Gallery",'ecobiz');
    if ($flickrnum == "") $flickrnum = 6;
    
    $title = $before_title.$flickrtitle.$after_title;
    
    $flickr_id = $ecobiz['flickr-id'];
    
    imediapixel_flickr_gallery($title,$flickr_id,$flickrnum);
    
    echo $after_widget;
  } 
}

add_action('widgets_init', create_function('', 'return register_widget("Flickr_Widget");'));

/*-----------------------------------------------------------------------------------*/
/*	Brochure Widget  
/*-----------------------------------------------------------------------------------*/
class Brochure_Widget extends WP_Widget {
  function Brochure_Widget() {
    $widgets_opt = array('description'=>'Display your brochure and download link');
    parent::WP_Widget(false,$name= "ECOBIZ - Brochure",$widgets_opt);
  }
  
  function form($instance) {
    global $post;
    
    $brochure_title = esc_attr($instance['brochure_title']);
    $brochure_desc = esc_attr($instance['brochure_desc']);
    $brochure_download_url = esc_attr($instance['brochure_download_url']);

  ?>
    <p><label for="brochure_title">Title:
  		<input id="<?php echo $this->get_field_id('brochure_title'); ?>" name="<?php echo $this->get_field_name('brochure_title'); ?>" type="text" class="widefat" value="<?php echo $brochure_title;?>" /></label></p>
    <p><label for="brochure_download_url">Brochure Download Url:
  		<input id="<?php echo $this->get_field_id('brochure_download_url'); ?>" name="<?php echo $this->get_field_name('brochure_download_url'); ?>" class="widefat" value="<?php echo $brochure_download_url;?>"/></label></p>
    <p><label for="brochure_desc"><?php echo __('Description','ecobiz');?>:</label>
		<textarea id="<?php echo $this->get_field_id('brochure_desc'); ?>" name="<?php echo $this->get_field_name('brochure_desc'); ?>" class="widefat" rows="6" cols="20" ><?php echo $brochure_desc;?></textarea></p>  
	  <?php    
  } 
  
  function update($new_instance, $old_instance) {
    return $new_instance;
  }
  
  function widget( $args, $instance ) {
    global $post;
    
    extract($args);
    
    $brochure_title = apply_filters('brochure_title',$instance['brochure_title']);
    $brochure_desc = apply_filters('brochure_desc',$instance['brochure_desc']);
    $brochure_download_url = apply_filters('brochure_download_url',$instance['brochure_download_url']);    
    
    echo $before_widget;
    echo $before_title.$brochure_title.$after_title;
    ?>
    <a href="<?php echo $brochure_download_url;?>"><img src="<?php echo get_template_directory_uri();?>/images/pdf.gif" alt="" class="alignleft" align=""/></a>
    <p><?php echo $brochure_desc;?></p>
    <p><a href="<?php echo $brochure_download_url;?>" class="button small white"><?php echo __('Download Now!','ecobiz');?> &raquo;</a></p>
    <div class="clear"></div>    
  <?php 
   echo $after_widget;
  } 
}

add_action('widgets_init', create_function('', 'return register_widget("Brochure_Widget");'));

/*-----------------------------------------------------------------------------------*/
/*	Child Pages Widget  
/*-----------------------------------------------------------------------------------*/
class ChildPages_Widget extends WP_Widget {
  function ChildPages_Widget() {
    $widgets_opt = array('description'=>'Display child pages of current parent page at the sidebar');
    parent::WP_Widget(false,$name= "ECOBIZ - Child Pages",$widgets_opt);
  }
  
  function form($instance) {
    global $post;
    
    $childpages_title = esc_attr($instance['childpages_title']);

  ?>
    <p><label for="childpages_title">Title:
  		<input id="<?php echo $this->get_field_id('childpages_title'); ?>" name="<?php echo $this->get_field_name('childpages_title'); ?>" type="text" class="widefat" value="<?php echo $childpages_title;?>" /></label></p>  
	  <?php    
  } 
  
  function update($new_instance, $old_instance) {
    return $new_instance;
  }
  
  function widget( $args, $instance ) {
    global $post;
    
    extract($args);
    
    $childpages_title = apply_filters('childpages_title',$instance['childpages_title']);
    
    $output = wp_list_pages('echo=0&depth=1&title_li=<h2>Top Level Pages </h2>' );
    if (is_page( )) {
        $page = $post->ID;
        if ($post->post_parent) {
            $page = $post->post_parent;
        }
        
        $children=wp_list_pages( 'echo=0&child_of=' . $page . '&title_li=' );
        
        if ($children) {
            $output = wp_list_pages ('echo=0&child_of=' . $page . '&title_li=');
        }
    }   
      
    $parent_title = get_the_title($page);
    
    if ($childpages_title) {
        $title = $childpages_title;
    } else {
        $title = $parent_title;
    }
   
    echo $before_widget;
    echo $before_title.$title.$after_title;
    
    echo '<ul class="sidelist">'.$output.'</ul>';

   echo $after_widget;
  } 
}

add_action('widgets_init', create_function('', 'return register_widget("ChildPages_Widget");'));
/*-----------------------------------------------------------------------------------*/
/*	Feature Box Widget  
/*-----------------------------------------------------------------------------------*/
class SiteFeature_Col_Widget extends WP_Widget {
  function SiteFeature_Col_Widget() {
    $widgets_opt = array('description'=>'Display your site features right below the slideshow section');
    parent::WP_Widget(false,$name= "ECOBIZ - Site Feature Box",$widgets_opt);
  }
  
  function form($instance) {
    global $post;
    
    $icon_feature = esc_attr($instance['icon_feature']);
    $icon_color = esc_attr($instance['icon_color']);
    $icon_size = esc_attr($instance['icon_size']);
    $icon_align = esc_attr($instance['icon_align']);
    $title_feature = esc_attr($instance['title_feature']);
    $desc_feature = esc_attr($instance['desc_feature']);
    $link_feature = esc_attr($instance['link_feature']);
    
    $icon_sizes = array('standard', 'icon-2x', 'icon-3x', 'icon-4x');
    $icon_aligns = array('left', 'right');
    
        $icons_list = imediapixel_get_elusive_font_icons();
        
        wp_enqueue_style('elusive-webfont', get_template_directory_uri().'/css/elusive-webfont.css');
        wp_enqueue_script( 'wp-color-picker');
        wp_enqueue_style('wp-color-picker');
        ?>
        
        <script type="text/javascript">
        // Add Color Picker to all inputs that have 'color-field' class
         jQuery(document).ready(function($) {
            $('.widget-icon-color-picker').wpColorPicker();
        });
        </script>
        
    <p><label for="title_feature"><?php echo __('Title :','ecobiz');?>
    <input id="<?php echo $this->get_field_id('title_feature'); ?>" name="<?php echo $this->get_field_name('title_feature'); ?>" type="text" class="widefat" value="<?php echo $title_feature;?>" /></label></p>
    <p><label for="icon_feature"><?php echo __('Please select the icon','ecobiz');?>
	<select  name="<?php echo $this->get_field_name('icon_feature'); ?>"  id="<?php echo $this->get_field_id('icon_feature'); ?>" class="widget-lib-elusive" >
	<?php foreach  ($icons_list as $icon_name => $icon_code) {?>
	   <option value="<?php echo $icon_name;?>" <?php selected($icon_feature, $icon_name, TRUE);?>><?php echo $icon_code.'&nbsp;&nbsp;'.$icon_name;?></option>
	<?php } ?>
	</select>
    </label></p>
    <p><label for="icon_color"><?php echo __('Icon Color','ecobiz');?>
    <input type="text"id="<?php echo $this->get_field_id('icon_color'); ?>" name="<?php echo $this->get_field_name('icon_color'); ?>"  value="<?php echo $icon_color;?>" class="widget-icon-color-picker"/>
    </label></p>
    <p><label for="icon_size"><?php echo __('Icon Size','ecobiz');?>
    <select  name="<?php echo $this->get_field_name('icon_size'); ?>"  id="<?php echo $this->get_field_id('icon_size'); ?>">
	<?php foreach  ($icon_sizes as $size) {?>
	   <option value="<?php echo $size;?>" <?php selected($icon_size, $size, TRUE);?>><?php echo $size;?></option>
	<?php } ?>
	</select>
    </label></p>
    <p><label for="icon_align"><?php echo __('Icon Align','ecobiz');?>
    <select  name="<?php echo $this->get_field_name('icon_align'); ?>"  id="<?php echo $this->get_field_id('icon_align'); ?>">
	<?php foreach  ($icon_aligns as $align) {?>
	   <option value="<?php echo $align;?>" <?php selected($icon_align, $align, TRUE);?>><?php echo $align;?></option>
	<?php } ?>
	</select>
    </label></p>
    <p><label for="desc_feature"><?php echo __('Description','ecobiz');?>:</label>
	<textarea id="<?php echo $this->get_field_id('desc_feature'); ?>" name="<?php echo $this->get_field_name('desc_feature'); ?>" class="widefat" ><?php echo $desc_feature;?></textarea></p>
    <p><label for="link_feature"><?php echo __('Link','ecobiz');?>:</label>
	<input id="<?php echo $this->get_field_id('link_feature'); ?>" name="<?php echo $this->get_field_name('link_feature'); ?>" type="text" class="widefat" value="<?php echo $link_feature;?>" />                            
	  <?php    
  } 
  
  function update($new_instance, $old_instance) {
    return $new_instance;
  }
  
  function widget( $args, $instance ) {
    global $post;
    
    extract($args);
    
    $icon_feature   = apply_filters('icon_feature',$instance['icon_feature']);
    $icon_color   = apply_filters('icon_color',$instance['icon_color']);
    $icon_size   = apply_filters('icon_size',$instance['icon_size']);
    $icon_align   = apply_filters('icon_align',$instance['icon_align']);
    $title_feature  = apply_filters('title_feature',$instance['title_feature']);
    $desc_feature   = apply_filters('desc_feature',$instance['desc_feature']);
    $link_feature   = apply_filters('link_feature',$instance['link_feature']);
    
    if ($icon_align == "right") {
        $align = "alignright";
    } else if ($icon_align == "left") {
        $align = "alignleft";
    } 
    
    $style = 'style="color:'.$icon_color.'";';
    
    echo $before_widget;
    ?>
        
    <i class="<?php echo $icon_feature.' '.$icon_size.' '.$align;?>" <?php echo $style;?>></i>
    <h4><a href="<?php echo $link_feature;?>"><?php echo stripslashes($title_feature);?></a></h4>
    <p><?php echo stripslashes($desc_feature);?></p>
    
    <?php
   echo $after_widget;
  } 
}

add_action('widgets_init', create_function('', 'return register_widget("SiteFeature_Col_Widget");'));
?>