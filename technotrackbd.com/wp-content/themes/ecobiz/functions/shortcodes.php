<?php

function imediapixel_run_shortcode( $content ) {
    global $shortcode_tags;
 
    // Backup current registered shortcodes and clear them all out
    $orig_shortcode_tags = $shortcode_tags;
    remove_all_shortcodes();
 
    add_shortcode('checklist', 'imediapixel_checklist');
    add_shortcode('itemlist', 'imediapixel_itemlist');
    add_shortcode('bulletlist', 'imediapixel_bulletlist');
    add_shortcode('arrowlist', 'imediapixel_arrowlist');
    add_shortcode('starlist', 'imediapixel_starlist');
    add_shortcode('warning', 'imediapixel_warningbox');
    add_shortcode('info', 'imediapixel_infobox');
    add_shortcode('success', 'imediapixel_successbox');
    add_shortcode('error', 'imediapixel_errorbox');
    add_shortcode('pullquote_right', 'imediapixel_pullquote_right');
    add_shortcode('pullquote_left', 'imediapixel_pullquote_left');
    add_shortcode('quotebox', 'imediapixel_quotebox');
    add_shortcode('dropcap', 'imediapixel_drop_cap');
    add_shortcode('spacer', 'imediapixel_spacer');
    add_shortcode('divider', 'imediapixel_divider');
    add_shortcode('highlight_yellow', 'imediapixel_highlight_yellow');
    add_shortcode('highlight_dark', 'imediapixel_highlight_dark');
    add_shortcode('highlight_green', 'imediapixel_highlight_green');
    add_shortcode('highlight_red', 'imediapixel_highlight_red');
    add_shortcode('col_12', 'imediapixel_col_12');
    add_shortcode('col_12_last', 'imediapixel_col_12_last');
    add_shortcode('col_13', 'imediapixel_col_13');
    add_shortcode('col_13_last', 'imediapixel_col_13_last');
    add_shortcode('col_14', 'imediapixel_col_14');
    add_shortcode('col_14_last', 'imediapixel_col_14_last');
    add_shortcode('col_23', 'imediapixel_col_23');
    add_shortcode('col_34', 'imediapixel_col_34');
    add_shortcode('col_12_inner', 'imediapixel_col_12_inner');
    add_shortcode('col_12_inner_last', 'imediapixel_col_12_inner_last');
    add_shortcode('col_13_inner', 'imediapixel_col_13_inner');
    add_shortcode('col_13_inner_last', 'imediapixel_col_13_inner_last');
    add_shortcode('col_14_inner', 'imediapixel_col_14_inner');
    add_shortcode('col_24_inner', 'imediapixel_col_24_inner');
    add_shortcode('col_14_inner_last', 'imediapixel_col_14_inner_last');
    add_shortcode('col_23_inner', 'imediapixel_col_23_inner');
    add_shortcode('col_34_inner', 'imediapixel_col_34_inner');
    add_shortcode('button', 'imediapixel_button');
    add_shortcode("video", "imediapixel_video");
    add_shortcode('pagelist','imediapixel_pagelist_shortcode');
    add_shortcode('bloglist','imediapixel_bloglist_shortcode');
    add_shortcode('teamlist','imediapixel_team_list_shortcode');
    add_shortcode('testimonial_list','imediapixel_testimonial_list_shortcode');
    add_shortcode( 'pricing', 'imediapixel_pricing_table_shortcode' );
    add_shortcode( 'item', 'imediapixel_pricing_shortcode' );
    add_shortcode('table', 'imediapixel_table');
    add_shortcode('gmap','imediapixel_shortcode_googlemap');
    add_shortcode('toggle', 'theme_shortcode_toggle');
    add_shortcode('image', 'imediapixel_imagealignment');
    add_shortcode('tabs', 'theme_shortcode_tabs');
    add_shortcode('mini_tabs', 'theme_shortcode_tabs');

    // Do the shortcode (only the one above is registered)
    $content = do_shortcode( $content );
    
    // Put the original shortcodes back
    $shortcode_tags = $orig_shortcode_tags;
    
    return $content;
}
add_filter( 'the_content', 'imediapixel_run_shortcode', 7 );

/* ======================================
   List Styles 
   ======================================*/
function imediapixel_checklist( $atts, $content = null ) {
	$content = str_replace('<ul>', '<ul class="checklist">', do_shortcode($content));
	return $content;	
}

function imediapixel_itemlist( $atts, $content = null ) {
	$content = str_replace('<ul>', '<ul class="itemlist">', do_shortcode($content));
	return $content;
	
}

function imediapixel_bulletlist( $atts, $content = null ) {
	$content = str_replace('<ul>', '<ul class="bulletlist">', do_shortcode($content));
	return $content;
	
}

function imediapixel_arrowlist( $atts, $content = null ) {
	$content = str_replace('<ul>', '<ul class="arrowlist">', do_shortcode($content));
	return $content;
	
}

function imediapixel_starlist( $atts, $content = null ) {
	$content = str_replace('<ul>', '<ul class="starlist">', do_shortcode($content));
	return $content;
	
}

/* ======================================
   Messages Box
   ======================================*/
function imediapixel_warningbox( $atts, $content = null ) {
   return '<div class="warning">' . do_shortcode($content) . '</div>';
}

function imediapixel_infobox( $atts, $content = null ) {
   return '<div class="info">' . do_shortcode($content) . '</div>';
}

function imediapixel_successbox( $atts, $content = null ) {
   return '<div class="success">' . do_shortcode($content) . '</div>';
}

function imediapixel_errorbox( $atts, $content = null ) {
   return '<div class="error">' . do_shortcode($content) . '</div>';
}

/* ======================================
   Pullquote
   ======================================*/

function imediapixel_pullquote_right( $atts, $content = null ) {
   return '<span class="pullquote_right">' . do_shortcode($content) . '</span>';
}

function imediapixel_pullquote_left( $atts, $content = null ) {
   return '<span class="pullquote_left">' . do_shortcode($content) . '</span>';
}

function imediapixel_quotebox( $atts, $content = null ) {
  return '<div class="content_quotebox"><h3>'.do_shortcode($content).'</h3></div>';
}


/* ======================================
   Dropcap
   ======================================*/
function imediapixel_drop_cap( $atts, $content = null ) {
   return '<span class="dropcap">' . do_shortcode($content) . '</span>';
}

/* ======================================
   Spacer
   ======================================*/
function imediapixel_spacer( $atts, $content = null ) {
   return '<div class="spacer"></div>';
}

/* ======================================
   Divider
   ======================================*/
function imediapixel_divider( $atts, $content = null ) {
   return '<div class="divider"></div>';
}

/* ======================================
   Highlight
   ======================================*/
function imediapixel_highlight_yellow( $atts, $content = null ) {
   return '<span class="highlight-yellow">' . do_shortcode($content) . '</span>';
}

function imediapixel_highlight_dark( $atts, $content = null ) {
   return '<span class="highlight-dark">' . do_shortcode($content) . '</span>';
}

function imediapixel_highlight_green( $atts, $content = null ) {
   return '<span class="highlight-green">' . do_shortcode($content) . '</span>';
}

function imediapixel_highlight_red( $atts, $content = null ) {
   return '<span class="highlight-red">' . do_shortcode($content) . '</span>';
}

/* ======================================
   Columns
   ======================================*/
function imediapixel_col_12( $atts, $content = null ) {
   return '<div class="col_12">' . do_shortcode($content) . '</div>';
}

function imediapixel_col_12_last( $atts, $content = null ) {
   return '<div class="col_12_last">' . do_shortcode($content) . '</div>';
}

function imediapixel_col_13( $atts, $content = null ) {
   return '<div class="col_13">' . do_shortcode($content) . '</div>';
}

function imediapixel_col_13_last( $atts, $content = null ) {
   return '<div class="col_13_last">' . do_shortcode($content) . '</div>';
}

function imediapixel_col_14( $atts, $content = null ) {
   return '<div class="col_14">' . do_shortcode($content) . '</div>';
}

function imediapixel_col_14_last( $atts, $content = null ) {
   return '<div class="col_14_last">' . do_shortcode($content) . '</div>';
}

function imediapixel_col_23( $atts, $content = null ) {
   return '<div class="col_23">' . do_shortcode($content) . '</div>';
}

function imediapixel_col_34($atts, $content = null ) {
   return '<div class="col_34">' . do_shortcode($content) . '</div>';
}

/* ======================================
   Buttons 
   ======================================*/
function imediapixel_button( $atts, $content = null ) {
	extract(shortcode_atts(array(
		'link'	=> '#',
		'color'	=> '',
		'size'	=> '',
	), $atts));
	
	$out = "<a class=\"button $color $size\" href=\"" .$link. "\">" .do_shortcode($content). "</a>";
  
	return $out;
}


/*-----------------------------------------------------------------------------------
  Video
-----------------------------------------------------------------------------------*/
function imediapixel_video( $attr, $content = null ) {
	extract(shortcode_atts(array(
            'url' => '',
    		'width' => '620',
    		'height' => '328'
    	), $attr));
	
	$output  = '<div class="video-container">';
	$output .= wp_oembed_get($url,array('width' =>$width,'height' =>$height));
	$output .= '</div>';
	
	return $output;
} 

/* ======================================
   Child pages list base on parent page
   ======================================*/
function imediapixel_pagelist_shortcode($atts,$content=null) {
  global $post;
  
  extract(shortcode_atts(array(
    "parent_page" => '',
    "num" => '',
    "orderby" => '',
    "column" => '',
    "readmore_text" => ''
  ),$atts));
  
  if ($column == "") $column = "2";
  if ($orderby == "") $orderby = "date";
   
  return imediapixel_pagelist($parent_page,$num,$orderby,$column,$readmore_text);
}

/* ======================================
   Team List
   ======================================*/
if (!function_exists('imediapixel_team_list_shortcode')) { 
    function imediapixel_team_list_shortcode($atts,$content=null) {
	global $post;

    extract(shortcode_atts(array(
    	"column" => '',
    	"showpost" => ''
	),$atts));

    return imediapixel_team_function($column,$showpost);
    }
}

/* ======================================
   Testimonial List
   ======================================*/
if (!function_exists('imediapixel_testimonial_list_shortcode')) { 
    function imediapixel_testimonial_list_shortcode($atts,$content=null) {
	global $post;

    extract(shortcode_atts(array(
    	"column" => '',
    	"showpost" => ''
	),$atts));

    return imediapixel_testimonial_list($column,$showpost);
    }
}
/* ======================================
   Blog list base on category
   ======================================*/
function imediapixel_bloglist_shortcode($atts,$content=null) {
  global $post;
  
  extract(shortcode_atts(array(
    "cat" => '',
    "num" => '' 
  ),$atts));
  
  return imediapixel_bloglist($cat, $num);
}

/*-----------------------------------------------------------------------------------*/
/*	Pricing Tables
/*-----------------------------------------------------------------------------------*/

/*main*/
function imediapixel_pricing_table_shortcode( $atts, $content = null  ) {
  
  extract( shortcode_atts( array(
		'column' => '4'
	), $atts ) );
	
	
	//set variables
	if($column == '3') {
		$column_size = 'third-col';
	}
	if($column =='4') {
		$column_size = 'fourth-col';
	}
	if($column =='5') {
		$column_size = 'fifth-col';
	}
    
	$out = '<div class="pricing-wrapper '.$column_size.'">'; 
	$out .= do_shortcode($content);
	$out .= '</div><div class="clear"></div>';

	return $out;
}

/*section*/
function imediapixel_pricing_shortcode( $atts, $content = null  ) {
	
	extract( shortcode_atts( array(
		'featured' => '',
    'color' => '',
		'title' => '',
    'subtitle' => '',
		'price' => '',
		'per' => '',
		'button_url' => '',
		'button_text' => 'Order'
	), $atts ) );
	
  $pr_color = '';
  $btn_color = '';
  switch ($color) {
  	case "brown" :
  		$pr_color ="brown-pr";
      $btn_color = "orange";
  	break;
  	case "orange" :
  		$pr_color ="orange-pr";
      $btn_color = "orange";
  	break;
  	case "green" :
  		$pr_color ="green-pr";
      $btn_color = "green";
  	break;
  	case "blue" :
  		$pr_color ="blue-pr";
      $btn_color = "blue";
  	break;
  	case "red" :
  		$pr_color ="red-pr";
      $btn_color = "red";
  	break;
    default :
      $pr_color ="";
      $btn_color = "gray";
  }
  
  $pricing_content = '';
  
  if ($featured =="yes") {
    $pricing_content .= '<div class="pricing-column'.' '.$pr_color.' feature-package">';
  } else {
    $pricing_content .= '<div class="pricing-column'.' '.$pr_color.'">';
  }
      
  $pricing_content .= '<h5 class="pricing-title">'.$title.'</h5>';
  $pricing_content .= '<div class="pricing-price">';
  $pricing_content .= '<h1>'. $price;
  if ($per !="") {
    $pricing_content .= '<span>/'.$per.'</span>'; 
  }
  $pricing_content .= '</h1>';
  $pricing_content .= '</div>';                 
  $pricing_content .= '<div class="pricing-feature">'.do_shortcode($content).'</div>';
  $pricing_content .= '<div class="pricing-description">'.$subtitle.'</div>';                
  $pricing_content .= '<div class="pricing-button">';
  if ($featured =="yes") {
    $pricing_content .= '<p><a href="'. $button_url .'" class="button small '.$btn_color.'">'. $button_text .'</a></p>';
  } else {
    $pricing_content .= '<p><a href="'. $button_url .'" class="button small '.$btn_color.'">'. $button_text .'</a></p>';
  }
  $pricing_content .= '</div>';                  
  $pricing_content .= '</div>';
  
	return $pricing_content;
}

/* Tables */

function imediapixel_table( $atts, $content = null ) {
  extract(shortcode_atts(array(
        'color'      => ''
    ), $atts));
    
	$content = "<div class=\"table-$color\">".str_replace('<table>', '<table class="table">', do_shortcode($content))."</div>";
	return $content;
	
}

/* ======================================
   Google Map
   ======================================*/
if (!function_exists('imediapixel_shortcode_googlemap')) {    
function imediapixel_shortcode_googlemap($atts, $content = null, $code) {
	extract(shortcode_atts(array(
		"width" => false,
		"height" => '400',
		"address" => '',
		"latitude" => 0,
		"longitude" => 0,
		"zoom" => 14,
		"html" => '',
		"popup" => 'false',
		"controls" => 'false',
		'pancontrol' => 'true',
		'zoomcontrol' => 'true',
		'maptypecontrol' => 'true',
		'scalecontrol' => 'true',
		'streetviewcontrol' => 'true',
		'overviewmapcontrol' => 'true',
		"scrollwheel" => 'true',
		'doubleclickzoom' =>'true',
		"maptype" => 'ROADMAP',
		"marker" => 'true',
		"class" => '',
		'align' => false,
	), $atts));
	
	if($width){
		if(is_numeric($width)){
			$width = $width.'px';
		}
		$width = 'width:'.$width.';';
	}else{
		$width = '';
		$align = false;
	}
	if($height){
		if(is_numeric($height)){
			$height = $height.'px';
		}
		$height = 'height:'.$height.';';
	}else{
		$height = '';
	}
	
	//enqueue script
	wp_enqueue_script( 'jquery.gmap');
	
	/* fix */
	$search  = array('G_NORMAL_MAP', 'G_SATELLITE_MAP', 'G_HYBRID_MAP', 'G_DEFAULT_MAP_TYPES', 'G_PHYSICAL_MAP');
	$replace = array('ROADMAP', 'SATELLITE', 'HYBRID', 'HYBRID', 'TERRAIN');
	$maptype = str_replace($search, $replace, $maptype);
	/* end fix */
	
	if($controls == 'true'){
	$controls = '{
	panControl: '.$pancontrol.',
	zoomControl: '.$zoomcontrol.',
	mapTypeControl: '.$maptypecontrol.',
	scaleControl: '.$scalecontrol.',
	streetViewControl: '.$streetviewcontrol.',
	overviewMapControl: '.$overviewmapcontrol.'}';
	}
	
	$align = $align?' align'.$align:'';
	$id = rand(100,1000);
	if($marker != 'false'){
	
		$out ='
		<div class="'.$class.'"><div id="google_map_'.$id.'" class="google_map'.$align.'" style="'.$width.''.$height.'"></div></div>
		<div style="line-height:0"><script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
		<script type="text/javascript">
		jQuery(document).ready(function($) {
			var tabs = jQuery("#google_map_'.$id.'").parents(".tabs_container,.mini_tabs_container,.accordion");
			jQuery("#google_map_'.$id.'").bind("initGmap",function(){
				jQuery(this).gMap({
					zoom: '.$zoom.',
					markers:[{
						address: "'.$address.'",
						latitude: '.$latitude.',
						longitude: '.$longitude.',
						html: "'.$html.'",
						popup: '.$popup.'
					}],
					controls: '.$controls.',
					maptype: "'.$maptype.'",
					doubleclickzoom:'.$doubleclickzoom.',
					scrollwheel:'.$scrollwheel.'
				});
				jQuery(this).data("gMapInited",true);
			}).data("gMapInited",false);
			if(tabs.size()!=0){
				tabs.find("ul.tabs,ul.mini_tabs,.accordion").data("tabs").onClick(function(index) {
					this.getCurrentPane().find(".google_map").each(function(){
						if(jQuery(this).data("gMapInited")==false){
							jQuery(this).trigger("initGmap");
						}
				});
			});
		}else{
			jQuery("#google_map_'.$id.'").trigger("initGmap");
		}
	});
	</script>
	</div>';

}else{
	
	$out ='
	<div class="'.$class.'"><div id="google_map_'.$id.'" class="google_map'.$align.'" style="'.$width.''.$height.'"></div></div>
	<div><script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
	<script type="text/javascript">
	jQuery(document).ready(function($) {
		var tabs = jQuery("#google_map_'.$id.'").parents(".tabs_container,.mini_tabs_container,.accordion");
		jQuery("#google_map_'.$id.'").bind("initGmap",function(){
			jQuery("#google_map_'.$id.'").gMap({
				zoom: '.$zoom.',
				latitude: '.$latitude.',
				longitude: '.$longitude.',
				address: "'.$address.'",
				controls: '.$controls.',
				maptype: "'.$maptype.'",
				doubleclickzoom:'.$doubleclickzoom.',
				scrollwheel:'.$scrollwheel.'
			});
			jQuery(this).data("gMapInited",true);
		}).data("gMapInited",false);
		if(tabs.size()!=0){
			tabs.find("ul.tabs,ul.mini_tabs,.accordion").data("tabs").onClick(function(index) {
				this.getCurrentPane().find(".google_map").each(function(){
					if(jQuery(this).data("gMapInited")==false){
						jQuery(this).trigger("initGmap");
					}
				});
			});
		}else{
			jQuery("#google_map_'.$id.'").trigger("initGmap");
		}
	});
	</script>
	</div>';
}
return $out;
}
add_shortcode('gmap','imediapixel_shortcode_googlemap');
}

function theme_shortcode_toggle($atts, $content = null, $code) {
	extract(shortcode_atts(array(
		'title' => false
	), $atts));
	return '<div class="toggle"><h5 class="toggle_title">' . $title . '</h5><div class="toggle_content"><p>' . do_shortcode(trim($content)) . '</p></div></div>';
}

/* Images */
function imediapixel_imagealignment( $atts, $content = null ) {
    extract(shortcode_atts(array(
        'source'      => '#',
        'align' => '',
        'style' =>''
    ), $atts));
  
  switch ($align) {
    case "left" :
      $class="alignleft";
    break;
    case "right" :
      $class="alignright";
    break;
    case "center" :
      $class="aligncenter";
    break;
  }
  $out = '';
  
  if ($style == "frame1") {
    $out .= '<div class="boximg box-left">';
    $out .= '<img class="boximg-pad" src="'.$source.'" style="width:84px;height:84px;" alt="">';
    $out .= '</div>';
  } else if ($style == "frame2") {    
    $out .= '<div class="portfolio-blockimg3"><div class="portfolio-imgbox3">';
    $out .= '<img class="boximg-pad" src="'.$source.'" style="width:196px;height:86px;" alt="">';
    $out .= '</div></div>';
  } else if ($style == "frame3") {
    $out .= '<div class="portfolio-blockimg2"><div class="portfolio-imgbox2">';
    $out .= '<img class="boximg-pad2" src="'.$source.'" style="width:270px;height:122px;" alt="">';
    $out .= '</div></div>';
  } else {
    $out .= '<img class="'.$class.'" src="'.$source.'" alt="">'; 
  }
    
  return $out;
}

/* Tabs and Accordiaon */
function theme_shortcode_tabs($atts, $content = null, $code) {
	extract(shortcode_atts(array(
		'style' => false
	), $atts));
	
	if (!preg_match_all("/(.?)\[(tab)\b(.*?)(?:(\/))?\](?:(.+?)\[\/tab\])?(.?)/s", $content, $matches)) {
		return do_shortcode($content);
	} else {
		for($i = 0; $i < count($matches[0]); $i++) {
			$matches[3][$i] = shortcode_parse_atts($matches[3][$i]);
		}
		$output = '<div class="tabs-wrapper"><ul class="'.$code.'">';
		
		for($i = 0; $i < count($matches[0]); $i++) {
			$output .= '<li><a href="#">' . $matches[3][$i]['title'] . '</a></li>';
		}
		$output .= '</ul>';
		$output .= '<div class="panes">';
		for($i = 0; $i < count($matches[0]); $i++) {
			$output .= '<div class="pane">' . do_shortcode(trim($matches[5][$i])) . '</div>';
		}
		$output .= '</div></div>';
		
		return '<div class="'.$code.'_container">' . $output . '</div>';
	}
}

/* ======================================
   Icon
   ======================================*/
if (!function_exists('imediapixel_icons')) { 
function imediapixel_icons( $atts, $content = null ) {
  extract(shortcode_atts(array(
		'name'	=> '',
		'color'	=> '',
		'size'	=> 'left',
		'align' => ''
		
	), $atts));
	
    switch ($align) {
        case "left" :
          $class="alignleft";
        break;
        case "right" :
          $class="alignright";
        break;
        case "center" :
          $class="aligncenter";
        break;
    }
  
    return '<i class="'.$name.' '.$size.' '.$class.' icon-spin" style="color:'.$color.'"></i>';
}
add_shortcode('icon', 'imediapixel_icons');
}
?>