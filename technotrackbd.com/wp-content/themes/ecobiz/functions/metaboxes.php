<?php
/**
 * Include and setup custom metaboxes and fields. (make sure you copy this file to outside the CMB directory)
 *
 * @category YourThemeOrPlugin
 * @package  Metaboxes
 * @license  http://www.opensource.org/licenses/gpl-license.php GPL v2.0 (or later)
 * @link     https://github.com/webdevstudios/Custom-Metaboxes-and-Fields-for-WordPress
 */

/**
 * Get the bootstrap! If using the plugin from wordpress.org, REMOVE THIS!
 */

if ( file_exists( dirname( __FILE__ ) . '/cmb2/init.php' ) ) {
	require_once dirname( __FILE__ ) . '/cmb2/init.php';
} elseif ( file_exists( dirname( __FILE__ ) . '/CMB2/init.php' ) ) {
	require_once dirname( __FILE__ ) . '/CMB2/init.php';
}

/**
 * Conditionally displays a field when used as a callback in the 'show_on_cb' field parameter
 *
 * @param  CMB2_Field object $field Field object
 *
 * @return bool                     True if metabox should show
 */
function cmb2_hide_if_no_cats( $field ) {
	// Don't show this field if not in the cats category
	if ( ! has_tag( 'cats', $field->object_id ) ) {
		return false;
	}
	return true;
}

add_filter( 'cmb2_meta_boxes', 'cmb2_sample_metaboxes' );
/**
 * Define the metabox and field configurations.
 *
 * @param  array $meta_boxes
 * @return array
 */
function cmb2_sample_metaboxes( array $meta_boxes ) {
	// Start with an underscore to hide fields from custom fields list
	$prefix = '_imediapixel_';

	/**
	 * Sample metabox to demonstrate each field type included
	 */
	$meta_boxes['imx_page_slider_metabox'] = array(
		'id'            => 'imx_page_slider_metabox',
		'title'         => __( 'Slideshow Options', 'ecobiz-admin' ),
		'object_types'  => array( 'page', ), // Post type
		'context'       => 'normal',
		'priority'      => 'high',
		'show_names'    => true, // Show field names on the left
	    'cmb_styles'    => true, // false to disable the CMB stylesheet
		// 'closed'     => true, // true to keep the metabox closed by default
		'fields'        => array(
			array(
				'name'    => __( 'Slider Type', 'ecobiz-admin' ),
				'desc'    => __( 'Select the slider type', 'ecobiz-admin' ),
				'id'      => $prefix . 'page_slider_type',
				'type'    => 'select',
				'options' => array(
					'none' => __( 'None', 'ecobiz-admin' ),
					'nivo-slider'   => __( 'Nivo Slider', 'ecobiz-admin' ),
					'kwicks-slider'     => __( 'Kwicks Slider', 'ecobiz-admin' ),
                    'static-slider'     => __( 'Static', 'ecobiz-admin' ),
				),
			),
			array(
				'name'     => __( 'Slider Category', 'ecobiz-admin' ),
				'desc'     => __( 'Select your default slider category', 'ecobiz-admin' ),
				'id'       => $prefix . 'page_slider_cat',
				'type'     => 'taxonomy_select',
				'taxonomy' => 'slideshow_category', // Taxonomy Slug
			),
		),
	);
    
    $meta_boxes['imx_page_metabox'] = array(
		'id'            => 'imx_page_metabox',
		'title'         => __( 'Page Options', 'ecobiz-admin' ),
		'object_types'  => array( 'page', ), // Post type
		'context'       => 'normal',
		'priority'      => 'high',
		'show_names'    => true, // Show field names on the left
	    'cmb_styles'    => true, // false to disable the CMB stylesheet
		// 'closed'     => true, // true to keep the metabox closed by default
		'fields'        => array(
            array(
				'name' => __( 'Page Heading Image', 'ecobiz-admin' ),
				'desc' => __( 'Upload an image or enter a URL that will be used as page heading image, recommende size is 960x182px', 'ecobiz-admin' ),
				'id'   => $prefix . 'page_heading_image',
				'type' => 'file',
			),
			array(
				'name' => __( 'Page Description', 'ecobiz-admin' ),
				'desc' => __( 'Additional page description for the page heading section', 'ecobiz-admin' ),
				'id'   => $prefix . 'page_desc',
				'type' => 'textarea_small',
			),
            array(
				'name' => __( 'Page Description Position', 'ecobiz-admin' ),
				'desc' => __( 'Additional page description for the page heading section', 'ecobiz-admin' ),
				'id'   => $prefix . 'page_desc_position',
				'type' => 'select',
				'options' => array(
                    'left'  => __('Left','ecobiz-admin'),
                    'right' => __('Right','ecobiz-admin')
                )
			),
		),
	);
    
    $meta_boxes['imx_page_sidebar_metabox'] = array(
		'id'            => 'imx_page_sidebar_metabox',
		'title'         => __( 'Page Sidebar Options', 'ecobiz-admin' ),
		'object_types'  => array( 'page', ), // Post type
		'context'       => 'normal',
		'priority'      => 'high',
		'show_names'    => true, // Show field names on the left
	    'cmb_styles'    => true, // false to disable the CMB stylesheet
		// 'closed'     => true, // true to keep the metabox closed by default
		'fields'        => array(
			array(
				'name'     => __( 'Sidebar Widget', 'ecobiz-admin' ),
				'desc'     => __( 'Select default sidebar widgets place for your sidebar', 'ecobiz-admin' ),
				'id'       => $prefix . 'page_sidebar',
				'type'     => 'select',
                'options'   => cmb2_get_sidebar_widgets(),
			),
            array(
				'name'     => __( 'Sidebar Position', 'ecobiz-admin' ),
				'desc'     => __( 'Select default sidebar position, left or right', 'ecobiz-admin' ),
				'id'       => $prefix . 'page_sidebar_position',
				'type'     => 'select',
                'options'   => array(
                    'left'  => 'Left',
                    'right'  => 'Right',
                ),
                'default'   => 'right'
			),
		),
	);
    
    $meta_boxes['imx_slider_metabox'] = array(
		'id'            => 'imx_slider_metabox',
		'title'         => __( 'Slidehow Options', 'ecobiz-admin' ),
		'object_types'  => array( 'slideshow', ), // Post type
		'context'       => 'normal',
		'priority'      => 'high',
		'show_names'    => true, // Show field names on the left
	    'cmb_styles'    => true, // false to disable the CMB stylesheet
		// 'closed'     => true, // true to keep the metabox closed by default
		'fields'        => array(
            array(
				'name' => __( 'Slideshow Content', 'ecobiz-admin' ),
				'desc' => __( 'Add your short slideshow content here', 'ecobiz-admin' ),
				'id'   => $prefix . 'slideshow_content',
				'type' => 'textarea_small',
				// 'repeatable' => true,
			),
			array(
				'name' => __( 'Slideshow URL', 'ecobiz-admin' ),
				'desc' => __( 'Additional custom slideshow URL', 'ecobiz-admin' ),
				'id'   => $prefix . 'slideshow_url',
				'type' => 'text',
				// 'repeatable' => true,
			),
		),
	);

	$meta_boxes['imx_portfolio_metabox'] = array(
		'id'            => 'imx_portfolio_metabox',
		'title'         => __( 'Portfolio Options', 'ecobiz-admin' ),
		'object_types'  => array( 'portfolio', ), // Post type
		'context'       => 'normal',
		'priority'      => 'high',
		'show_names'    => true, // Show field names on the left
	    'cmb_styles'    => true, // false to disable the CMB stylesheet
		// 'closed'     => true, // true to keep the metabox closed by default
		'fields'        => array(
            array(
				'name' => __( 'Preview Link', 'ecobiz-admin' ),
				'desc' => __( 'Used for popup lightbox preview<br/>Images : <br />http://yoursite.com/uploaded_image.jpg<br/> Video : <br />
                              http://www.youtube.com/watch?v=tESK9RcyexU<br />
                              http://vimeo.com/12816548<br />', 'ecobiz-admin' ),
				'id'   => $prefix . 'portfolio_link',
				'type' => 'text',
				// 'repeatable' => true,
			),
            array(
				'name'         => __( 'Gallery', 'cmb2' ),
				'desc'         => __( 'Upload multiple images for your portfolio item gallery.', 'ecobiz-admin' ),
				'id'           => $prefix . 'portfolio_gallery',
				'type'         => 'file_list',
				'preview_size' => array( 100, 100 ), // Default: array( 50, 50 )
			),
			
		),
	);
    
    //TEAM METABOX
	$meta_boxes[] = array(
		'id'              => 'imx_team_option',
		'title'           => __('Team Options','ecobiz-admin'),
		'object_types'    => array( 'team' ), // Post type
		'context'         => 'normal',
		'priority'        => 'high',
		'show_names'      => true, // Show field names on the left
        'cmb_styles'        => true, // false to disable the CMB stylesheet
		'fields' => array(
			array(
				'name' => __('Occupation','ecobiz-admin'),
				'desc' => __('Please enter the job position at the company','ecobiz-admin'),
				'id'   => $prefix . 'staff_occupation',
				'type' => 'text',
			),
			array(
				'name' => __('Facebook','ecobiz-admin'),
				'desc' => __('Please enter facebook URL. Example : http://www.facebook.com/username.','ecobiz-admin'),
				'id'   => $prefix . 'staff_fb',
				'type' => 'text',
			),
			array(
				'name' => __('Twitter','ecobiz-admin'),
				'desc' => __('Please enter twitter URL. Example : http://www.twitter.com/username.','ecobiz-admin'),
				'id'   => $prefix . 'staff_twitter',
				'type' => 'text',
			),
			array(
				'name' => __('Google+','ecobiz-admin'),
				'desc' => __('Please enter google+ URL. Example : https://plus.google.com/u/0/108763868013266824234/posts.','ecobiz-admin'),
				'id'   => $prefix . 'staff_google',
				'type' => 'text',
			),
			array(
				'name' => __('Linkedin','ecobiz-admin'),
				'desc' => __('Please enter Linkedin URL. Example : http://www.linkedin.com/in/username.','ecobiz-admin'),
				'id'   => $prefix . 'staff_linkedin',
				'type' => 'text',
			),
            array(
				'name' => __('E-mail','ecobiz-admin'),
				'desc' => __('Team personal e-mail address. Example : info@domain.com.','ecobiz-admin'),
				'id'   => $prefix . 'staff_email',
				'type' => 'text',
			),
		)
	);
    
    //TESTIMONIAL METABOX
	$meta_boxes[] = array(
		'id'              => 'imx_testimonial_option',
		'title'           => __('Testimonial Options','ecobiz-admin'),
		'object_types'    => array( 'testimonial' ), // Post type
		'context'         => 'normal',
		'priority'        => 'high',
		'show_names'      => true, // Show field names on the left
        'cmb_styles'        => true, // false to disable the CMB stylesheet
		'fields' => array(
			array(
				'name' => __('Additional Info','ecobiz-admin'),
				'desc' => __('Please enter additional info for testimonial item, eg. company name or business title','ecobiz-admin'),
				'id'   => $prefix . 'testimonial_info',
				'type' => 'text',
			),
		)
	);
    
	return $meta_boxes;
}