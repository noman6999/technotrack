<?php
/*
Template Name: Full Width
*/
?>  

<?php get_header();?>
    
      <?php
        global $post, $ecobiz;
        
        $meta_prefix = "_imediapixel_";
        
        $page_slider_type = get_post_meta(get_the_ID(),$meta_prefix."page_slider_type",true);
        $page_slider_cat = get_the_terms(get_the_ID(),'slideshow_category');
        if (is_array($page_slider_cat) && !empty($page_slider_cat) && $page_slider_cat !="none") {
            foreach ($page_slider_cat as $slider_cat) {
                $slider_cat_name = $slider_cat->name;
            }     
        }
        $page_heading_image = get_post_meta(get_the_ID(),$meta_prefix."page_heading_image",true);
        $page_desc = get_post_meta(get_the_ID(),$meta_prefix."page_desc",true);
        $page_sidebar_position = get_post_meta(get_the_ID(),$meta_prefix."page_sidebar_position",true);
        $page_desc_position      = get_post_meta(get_the_ID(),$meta_prefix."page_desc_position",true);
        
        $slideshow_order    = $ecobiz['slideshow-order'];
        $breadcrumb         = $ecobiz['breadcrumb'];
        
      ?>      
            
      <?php if ($page_slider_type =="" || !isset($page_slider_type) || $page_slider_type == "none" ) { ?>
        <!-- Page Heading --> 
        <div id="page-heading">
          <img src="<?php echo $page_heading_image ? $page_heading_image : get_template_directory_uri().'/images/page-heading.jpg';?>" alt="" />
          <div class="heading-text<?php if ($page_desc_position =="right") echo '-right';?>">
            <h3><?php the_title();?></h3>
            <p><?php echo stripslashes($page_desc);?></p>
          </div>
        </div>
        <!-- Page Heading End -->
      <?php } else { ?>
        <?php
        if ($page_slider_type !="none"  || $page_slider_type !="static-slider") {
          if ($page_slider_type == "nivo-slider") {
            imediapixel_get_nivoslider($slider_cat_name,$slideshow_order);
          } else if ($page_slider_type == "kwicks-slider") {
            imediapixel_get_kwicksslider($slider_cat_name,$slideshow_order);
          } else if ($page_slider_type == "static-slider") {
            echo imediapixel_get_staticslider();
          }
        }
        ?>
      <?php } ?>  
      <div class="clear"></div>
      <?php if (is_home() || is_front_page()) { ?>
        <div class="featuresbox">
          <?php 
          $count = count($sidebars_widgets['feature-boxes']);
          if ($count == 2) {
              echo '<ul class="features-2col">'; 
          } else if ($count == 3) {
              echo '<ul class="features-3col">';
          } else if ($count == 4) {
              echo '<ul>';
          } 
          ?>
          <?php
            if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('Site Features Box')) {
              echo __("Please place some \"Site Feature Box\" widget from Apppearances => Widgets menu",'ecobiz');
            } 
          ?>
          </ul>
        </div>
      <?php } ?>
      
      <div class="center">
      <?php if (!is_home() || !is_front_page()) { ?>      
        <?php if ($breadcrumb == 1) { ?>
          <div class="breadcrumb">
            <?php if ( function_exists( 'breadcrumbs_plus' ) ) breadcrumbs_plus(); ?>
          </div>
        <?php } ?>
      <?php } ?> 
      
        <!-- Main Content Wrapper -->
        <div class="maincontent-full">
          <?php if (have_posts()) : ?>
          <?php while (have_posts()) : the_post();?>
          <?php the_content();?>
          <?php endwhile;?>
          <?php endif;?>
          
          <div class="clear"></div>
        </div>
        <!-- Main Content Wrapper End -->
    
  <?php get_footer();?>