<?php 
global $ecobiz;
$info_phone = $ecobiz['phone-number'];
$info_email = $ecobiz['email-address'];
$header_contact = $ecobiz['header-contact'];
$favico = $ecobiz['favicon'];
?>
<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 9]>    <html class="no-js lt-ie10" <?php language_attributes(); ?>> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" <?php language_attributes(); ?>> <!--<![endif]-->

<head>
<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>"  />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />

<title>
<?php
  global $page, $paged;
	wp_title('|', true, 'right'); bloginfo('name');
	if ( $paged >= 2 || $page >= 2 ) echo ' | ' . sprintf( __( 'Page %s', 'ecobiz' ), max( $paged, $page ) );
?>
</title>

<meta name="generator" content="WordPress <?php bloginfo('version'); ?>" />
<meta name="robots" content="follow, all" />
<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" media="screen" />
<link rel="alternate" type="application/rss+xml" title="<?php bloginfo('name'); ?> RSS Feed" href="<?php bloginfo('rss2_url'); ?>" />
<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
<link rel="shortcut icon" href="<?php echo ($favico['url']) ? ($favico['url']) : get_template_directory_uri().'/images/favicon.ico';?>"/>
<?php if ( is_singular() ) wp_enqueue_script( 'comment-reply' ); ?>

<?php wp_head(); ?>

</head>

<body <?php body_class(array('bgpattern','bgimage')); ?>>
  <div id="wrapper">
    <div class="header-contact-box">
    <?php if (function_exists('language_selector_flags')) { ?>
        <div id="flags_language_selector"><?php language_selector_flags(); ?></div >
    <?php } ?>
    <?php if ($header_contact ==1) { ?>
    <ul class="header-contact">
        <?php if ($info_phone) { ?>
          <li><i class="icon-phone"></i> <?php echo $info_phone;?></li>
        <?php } ?>
        <?php if ($info_email) { ?>
          <li><i class="icon-envelope"></i> <a href="mailto:<?php echo $info_email;?>"><?php echo $info_email;?></a></li>
        <?php } ?>
    </ul>
    <?php } ?>
    </div>
    <div class="clear"></div>
    <div id="topwrapper"></div>
    <div id="mainwrapper">
      <!-- Header Start -->
      <div id="header">
        <div class="center">
          <!-- Logo Start -->
          <div id="logo">
          <?php $logo = $ecobiz['header-logo'];?>
          <a href="<?php echo home_url();?>"><img src="<?php echo ($logo['url']) ? $logo['url'] : get_template_directory_uri().'/images/logo.png';?>" alt="Logo"/></a>
          </div>
          <!-- Logo End -->
          
          <div id="headerright">
            <!-- Menu Navigation Start -->
            <div id="mobilemenu"></div> 
            <div id="mainmenu">
                <?php 
                if (function_exists('wp_nav_menu')) { 
                  wp_nav_menu( array( 
                    'menu_id' => 'menu', 
                    'menu_class' => '', 
                    'theme_location' => 'topnav', 
                    'fallback_cb'=>'imediapixel_topmenu_pages',
                    'depth' =>4 
                    ) );
                } 
                ?>
            </div>
            <!-- Menu Navigation End -->
          </div>
        </div>
      </div>
      <!-- Header End  -->