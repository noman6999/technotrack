<?php
/*
Template Name: Portfolio 3 Columns
*/
?>
<?php get_header();?>
    
       <?php
        global $post, $ecobiz;
        
        $meta_prefix = "_imediapixel_";
        
        $page_slider_type = get_post_meta(get_the_ID(),$meta_prefix."page_slider_type",true);
        $page_slider_cat = get_the_terms(get_the_ID(),'slideshow_category');
        if (is_array($page_slider_cat) && !empty($page_slider_cat) && $page_slider_cat !="none") {
            foreach ($page_slider_cat as $slider_cat) {
                $slider_cat_name = $slider_cat->name;
            }     
        }
        $page_heading_image = get_post_meta(get_the_ID(),$meta_prefix."page_heading_image",true);
        $page_desc = get_post_meta(get_the_ID(),$meta_prefix."page_desc",true);
        $page_sidebar_position = get_post_meta(get_the_ID(),$meta_prefix."page_sidebar_position",true);
        $page_desc_position      = get_post_meta(get_the_ID(),$meta_prefix."page_desc_position",true);
        
        $slideshow_order    = $ecobiz['ecobiz-slideshow-order'];
        $breadcrumb         = $ecobiz['breadcrumb'];
        
      ?>      
            
      <?php if ($page_slider_type =="" || !isset($page_slider_type) || $page_slider_type == "none" ) { ?>
        <!-- Page Heading --> 
        <div id="page-heading">
          <img src="<?php echo $page_heading_image ? $page_heading_image : get_template_directory_uri().'/images/page-heading.jpg';?>" alt="" />
          <div class="heading-text<?php if ($page_desc_position =="right") echo '-right';?>">
            <h3><?php the_title();?></h3>
            <p><?php echo stripslashes($page_desc);?></p>
          </div>
        </div>
        <!-- Page Heading End -->
      <?php } else { ?>
        <?php
        if ($page_slider_type !="none"  || $page_slider_type !="static-slider") {
          if ($page_slider_type == "nivo-slider") {
            imediapixel_get_nivoslider($slider_cat_name,$slideshow_order);
          } else if ($page_slider_type == "kwicks-slider") {
            imediapixel_get_kwicksslider($slider_cat_name,$slideshow_order);
          } else if ($page_slider_type == "static-slider") {
            echo imediapixel_get_staticslider();
          }
        }
        ?>
      <?php } ?>  
      <div class="clear"></div>
      <?php if (is_home() || is_front_page()) { ?>
        <div class="featuresbox">
          <?php 
          $count = count($sidebars_widgets['feature-boxes']);
          if ($count == 2) {
              echo '<ul class="features-2col">'; 
          } else if ($count == 3) {
              echo '<ul class="features-3col">';
          } else if ($count == 4) {
              echo '<ul>';
          } 
          ?>
          <?php
            if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('Site Features Box')) {
              echo __("Please place some \"Site Feature Box\" widget from Apppearances => Widgets menu",'ecobiz');
            } 
          ?>
        </div>
      <?php } ?>
      
      <div class="center">
      <?php if (!is_home() || !is_front_page()) { ?>      
        <?php if ($breadcrumb == 1) { ?>
          <div class="breadcrumb">
            <?php if ( function_exists( 'breadcrumbs_plus' ) ) breadcrumbs_plus(); ?>
          </div>
        <?php } ?>
      <?php } ?>  
      
        <!-- Main Content Wrapper -->
        <div class="maincontent-full">
          
          <ul class="portfolio-3col">
          <?php 
          $page = (get_query_var('paged')) ? get_query_var('paged') : 1;
          
          $myquery = array(
            'post_type' => 'portfolio', 
            'showposts' => $portfolio_perpage,
            'paged'=> $page,
            "orderby" => $portfolio_order,
            'order'=> 'ASC'
          );
          
          query_posts($myquery);
          
          $counter = 0;
          while ( have_posts() ) : the_post();
            $counter++;
            $meta_prefix = "_imediapixel_";
            
            $pf_link = get_post_meta(get_the_ID(), $meta_prefix.'portfolio_link', true );
            $pf_url = get_post_meta(get_the_ID(), $meta_prefix.'portfolio_url', true );
            $pf_gallery = get_post_meta(get_the_ID(), $meta_prefix.'portfolio_gallery', true );
            
            $thumb   = get_post_thumbnail_id();
            $img_url = wp_get_attachment_url( $thumb,'full' ); //get full URL to image (use "large" or "medium" if the images too big)
            $image   = aq_resize( $img_url, 270, 122, true ); //resize & crop the image
            ?>
            <li <?php if ($counter %3 == 0) echo 'class="last"';?>>
              <div class="portfolio-blockimg2">
                <div class="portfolio-imgbox2">
                    <div class="zoom">
                          <?php if (is_array($pf_gallery) && !empty($pf_gallery) && ($pf_gallery !="")) { 
                            foreach ($pf_gallery as $pf_gal) {
                                echo '<a href="'.$pf_gal.'" class="fancybox" rel="gallery'.get_the_ID().'" title="'.get_the_title().'">';
                            }
                          } else if (function_exists('has_post_thumbnail') && has_post_thumbnail()) {?>
                              <a href="<?php echo ($pf_link) ? $pf_link : $img_url;?>" class="fancybox" title="<?php the_title();?>">
                          <?php } ?>
                            <img src="<?php echo $image;?>" class="boximg-pad fade" alt="<?php the_title();?>" />
                            </a>
                        </div>
                </div>
                <h4><a href="<?php the_permalink();?>"><?php the_title();?></a></h4>
                <?php the_excerpt();?>
                <p><a href="<?php the_permalink();?>" class="button white small"><?php echo __('View Detail','ecobiz');?> &raquo;</a></p>     
              </div>
            </li>            
          <?php endwhile;?>
          </ul>
          <div class="clear"></div>
          <?php 
            global $wp_query; 
            $total_pages = $wp_query->max_num_pages; 
            if ( $total_pages > 1 ) {
            if (function_exists("wpapi_pagination")) {
                wpapi_pagination($total_pages); 
              }
            }
          ?>    
        </div>
        <!-- Main Content Wrapper End -->
    
  <?php get_footer();?>