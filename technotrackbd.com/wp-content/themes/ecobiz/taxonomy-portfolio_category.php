<?php get_header();?>
    
       <?php
        global $post, $ecobiz;
        
        $meta_prefix = "_imediapixel_";
        
        $page_slider_type = get_post_meta(get_the_ID(),$meta_prefix."page_slider_type",true);
        $page_slider_cat = get_the_terms(get_the_ID(),'slideshow_category');
        if (is_array($page_slider_cat) && !empty($page_slider_cat) && $page_slider_cat !="none") {
            foreach ($page_slider_cat as $slider_cat) {
                $slider_cat_name = $slider_cat->name;
            }     
        }
        $page_heading_image = get_post_meta(get_the_ID(),$meta_prefix."page_heading_image",true);
        $bgtext_heading_position = get_post_meta(get_the_ID(),"_imediapixel_bgtext_heading_position",true);
        $page_desc = get_post_meta(get_the_ID(),$meta_prefix."page_desc",true);
        $page_sidebar_position = get_post_meta(get_the_ID(),$meta_prefix."page_sidebar_position",true);
        
        $portfolio_page = $ecobiz['portfolio-page'];
        $slideshow_order    = $ecobiz['ecobiz-slideshow-order'];
        $breadcrumb         = $ecobiz['breadcrumb'];
        
      ?>      
        <!-- Page Heading --> 
        <div id="page-heading">
          <img src="<?php echo $page_heading_image ? $page_heading_image : get_template_directory_uri().'/images/page-heading.jpg';?>" alt="" />
          <div class="heading-text<?php if ($bgtext_heading_position =="right") echo '-right';?>">
            <h3><?php the_title();?></h3>
            <p><?php echo stripslashes($page_desc);?></p>
          </div>
        </div>
        <!-- Page Heading End -->
        
      <div class="clear"></div>
      
      <div class="center">
        <?php if ($breadcrumb == 1) { ?>
          <div class="breadcrumb">
            <?php if ( function_exists( 'breadcrumbs_plus' ) ) breadcrumbs_plus(); ?>
          </div>
        <?php } ?>
      
        <!-- Main Content Wrapper -->
        <div class="maincontent-full">
          
          <ul id="filter">
 			<li><a class="<?php if (!get_query_var('portfolio_category')) echo 'current'; ?>" href="<?php echo get_page_link($portfolio_page);?>"><?php echo __('All','ecobiz');?></a></li>
          	<?php  
            $categories = get_categories('taxonomy=portfolio_category&orderby=ID&title_li=&hide_empty=0');
            foreach ($categories as $category) { 
            $termlink = get_term_link($category->slug,$category->taxonomy);
            ?>
              <li><a  class="<?php if (get_query_var($category->taxonomy) == $category->slug) echo 'current'; ?>" href="<?php echo $termlink;?>"><?php echo $category->name;?></a></li>
              <?php
            }
            ?>
          </ul>
          
          <div class="clear"></div>
          <ul class="portfolio-4col">
          <?php 
          
          $counter = 0;
          while ( have_posts() ) : the_post();
            $counter++;
            $meta_prefix = "_imediapixel_";
            
            $pf_link = get_post_meta(get_the_ID(), $meta_prefix.'portfolio_link', true );
            $pf_url = get_post_meta(get_the_ID(), $meta_prefix.'portfolio_url', true );
            $pf_gallery = get_post_meta(get_the_ID(), $meta_prefix.'portfolio_gallery', true );
            
            $thumb   = get_post_thumbnail_id();
            $img_url = wp_get_attachment_url( $thumb,'full' ); //get full URL to image (use "large" or "medium" if the images too big)
            $image   = aq_resize( $img_url, 196, 86, true ); //resize & crop the image
            ?>
            <li <?php if ($counter %4 == 0) echo 'class="last"';?>>
              <div class="portfolio-blockimg3">
                <div class="portfolio-imgbox3">
                    <div class="zoom">
                      <?php if (is_array($pf_gallery) && !empty($pf_gallery) && ($pf_gallery !="")) { 
                        foreach ($pf_gallery as $pf_gal) {
                            echo '<a href="'.$pf_gal.'" class="fancybox" rel="gallery'.get_the_ID().'" title="'.get_the_title().'">';
                        }
                      } else if (function_exists('has_post_thumbnail') && has_post_thumbnail()) {?>
                          <a href="<?php echo ($pf_link) ? $pf_link : $img_url;?>" class="fancybox" title="<?php the_title();?>">
                      <?php } ?>
                        <img src="<?php echo $image;?>" class="boximg-pad fade" alt="<?php the_title();?>" />
                        </a>
                    </div>
                </div>
                <h4><a href="<?php the_permalink();?>"><?php the_title();?></a></h4>
                <?php the_excerpt();?>
                <p><a href="<?php the_permalink();?>" class="button white small"><?php echo __('View Detail','ecobiz');?> &raquo;</a></p>
              </div>
            </li>            
          <?php endwhile;?>
          </ul>
          <div class="clear"></div>
          <?php 
            global $wp_query; 
            $total_pages = $wp_query->max_num_pages; 
            if ( $total_pages > 1 ) {
            if (function_exists("wpapi_pagination")) {
                wpapi_pagination($total_pages); 
              }
            }
          ?>   
        </div>
        <!-- Main Content Wrapper End -->
    
  <?php get_footer();?>