<?php
    /**
     * ReduxFramework Sample Config File
     * For full documentation, please visit: http://docs.reduxframework.com/
     */

    if ( ! class_exists( 'Redux_Framework_sample_config' ) ) {

        class Redux_Framework_sample_config {

            public $args = array();
            public $sections = array();
            public $theme;
            public $ReduxFramework;

            public function __construct() {

                if ( ! class_exists( 'ReduxFramework' ) ) {
                    return;
                }

                // This is needed. Bah WordPress bugs.  ;)
                if ( true == Redux_Helpers::isTheme( __FILE__ ) ) {
                    $this->initSettings();
                } else {
                    add_action( 'plugins_loaded', array( $this, 'initSettings' ), 10 );
                }

            }

            public function initSettings() {

                // Just for demo purposes. Not needed per say.
                $this->theme = wp_get_theme();

                // Set the default arguments
                $this->setArguments();

                // Set a few help tabs so you can see how it's done
                $this->setHelpTabs();

                // Create the sections and fields
                $this->setSections();

                if ( ! isset( $this->args['opt_name'] ) ) { // No errors please
                    return;
                }

                // If Redux is running as a plugin, this will remove the demo notice and links
                //add_action( 'redux/loaded', array( $this, 'remove_demo' ) );

                // Function to test the compiler hook and demo CSS output.
                // Above 10 is a priority, but 2 in necessary to include the dynamically generated CSS to be sent to the function.
                //add_filter('redux/options/'.$this->args['opt_name'].'/compiler', array( $this, 'compiler_action' ), 10, 3);

                // Change the arguments after they've been declared, but before the panel is created
                //add_filter('redux/options/'.$this->args['opt_name'].'/args', array( $this, 'change_arguments' ) );

                // Change the default value of a field after it's been set, but before it's been useds
                //add_filter('redux/options/'.$this->args['opt_name'].'/defaults', array( $this,'change_defaults' ) );

                // Dynamically add a section. Can be also used to modify sections/fields
                //add_filter('redux/options/' . $this->args['opt_name'] . '/sections', array($this, 'dynamic_section'));

                $this->ReduxFramework = new ReduxFramework( $this->sections, $this->args );
            }

            /**
             * This is a test function that will let you see when the compiler hook occurs.
             * It only runs if a field    set with compiler=>true is changed.
             * */
            function compiler_action( $options, $css, $changed_values ) {
                echo '<h1>The compiler hook has run!</h1>';
                echo "<pre>";
                print_r( $changed_values ); // Values that have changed since the last save
                echo "</pre>";
                //print_r($options); //Option values
                //print_r($css); // Compiler selector CSS values  compiler => array( CSS SELECTORS )

                /*
              // Demo of how to use the dynamic CSS and write your own static CSS file
              $filename = dirname(__FILE__) . '/style' . '.css';
              global $wp_filesystem;
              if( empty( $wp_filesystem ) ) {
                require_once( ABSPATH .'/wp-admin/includes/file.php' );
              WP_Filesystem();
              }

              if( $wp_filesystem ) {
                $wp_filesystem->put_contents(
                    $filename,
                    $css,
                    FS_CHMOD_FILE // predefined mode settings for WP files
                );
              }
             */
            }

            /**
             * Custom function for filtering the sections array. Good for child themes to override or add to the sections.
             * Simply include this function in the child themes functions.php file.
             * NOTE: the defined constants for URLs, and directories will NOT be available at this point in a child theme,
             * so you must use get_template_directory_uri() if you want to use any of the built in icons
             * */
            function dynamic_section( $sections ) {
                //$sections = array();
                $sections[] = array(
                    'title'  => __( 'Section via hook', 'ecobiz-admin' ),
                    'desc'   => __( '<p class="description">This is a section created by adding a filter to the sections array. Can be used by child themes to add/remove sections from the options.</p>', 'ecobiz-admin' ),
                    'icon'   => 'el-icon-paper-clip',
                    // Leave this as a blank section, no options just some intro text set above.
                    'fields' => array()
                );

                return $sections;
            }

            /**
             * Filter hook for filtering the args. Good for child themes to override or add to the args array. Can also be used in other functions.
             * */
            function change_arguments( $args ) {
                //$args['dev_mode'] = true;

                return $args;
            }

            /**
             * Filter hook for filtering the default value of any given field. Very useful in development mode.
             * */
            function change_defaults( $defaults ) {
                $defaults['str_replace'] = 'Testing filter hook!';

                return $defaults;
            }

            // Remove the demo link and the notice of integrated demo from the redux-framework plugin
            function remove_demo() {

                // Used to hide the demo mode link from the plugin page. Only used when Redux is a plugin.
                if ( class_exists( 'ReduxFrameworkPlugin' ) ) {
                    remove_filter( 'plugin_row_meta', array(
                        ReduxFrameworkPlugin::instance(),
                        'plugin_metalinks'
                    ), null, 2 );

                    // Used to hide the activation notice informing users of the demo panel. Only used when Redux is a plugin.
                    remove_action( 'admin_notices', array( ReduxFrameworkPlugin::instance(), 'admin_notices' ) );
                }
            }

            public function setSections() {

                /**
                 * Used within different fields. Simply examples. Search for ACTUAL DECLARATION for field examples
                 * */
                // Background Patterns Reader
                $sample_patterns_path = ReduxFramework::$_dir . '../sample/patterns/';
                $sample_patterns_url  = ReduxFramework::$_url . '../sample/patterns/';
                $sample_patterns      = array();

                if ( is_dir( $sample_patterns_path ) ) :

                    if ( $sample_patterns_dir = opendir( $sample_patterns_path ) ) :
                        $sample_patterns = array();

                        while ( ( $sample_patterns_file = readdir( $sample_patterns_dir ) ) !== false ) {

                            if ( stristr( $sample_patterns_file, '.png' ) !== false || stristr( $sample_patterns_file, '.jpg' ) !== false ) {
                                $name              = explode( '.', $sample_patterns_file );
                                $name              = str_replace( '.' . end( $name ), '', $sample_patterns_file );
                                $sample_patterns[] = array(
                                    'alt' => $name,
                                    'img' => $sample_patterns_url . $sample_patterns_file
                                );
                            }
                        }
                    endif;
                endif;

                ob_start();

                $ct          = wp_get_theme();
                $this->theme = $ct;
                $item_name   = $this->theme->get( 'Name' );
                $tags        = $this->theme->Tags;
                $screenshot  = $this->theme->get_screenshot();
                $class       = $screenshot ? 'has-screenshot' : '';

                $customize_title = sprintf( __( 'Customize &#8220;%s&#8221;', 'ecobiz-admin' ), $this->theme->display( 'Name' ) );

                ?>
                <div id="current-theme" class="<?php echo esc_attr( $class ); ?>">
                    <?php if ( $screenshot ) : ?>
                        <?php if ( current_user_can( 'edit_theme_options' ) ) : ?>
                            <a href="<?php echo wp_customize_url(); ?>" class="load-customize hide-if-no-customize"
                               title="<?php echo esc_attr( $customize_title ); ?>">
                                <img src="<?php echo esc_url( $screenshot ); ?>"
                                     alt="<?php esc_attr_e( 'Current theme preview', 'ecobiz-admin' ); ?>"/>
                            </a>
                        <?php endif; ?>
                        <img class="hide-if-customize" src="<?php echo esc_url( $screenshot ); ?>"
                             alt="<?php esc_attr_e( 'Current theme preview', 'ecobiz-admin' ); ?>"/>
                    <?php endif; ?>

                    <h4><?php echo $this->theme->display( 'Name' ); ?></h4>

                    <div>
                        <ul class="theme-info">
                            <li><?php printf( __( 'By %s', 'ecobiz-admin' ), $this->theme->display( 'Author' ) ); ?></li>
                            <li><?php printf( __( 'Version %s', 'ecobiz-admin' ), $this->theme->display( 'Version' ) ); ?></li>
                            <li><?php echo '<strong>' . __( 'Tags', 'ecobiz-admin' ) . ':</strong> '; ?><?php printf( $this->theme->display( 'Tags' ) ); ?></li>
                        </ul>
                        <p class="theme-description"><?php echo $this->theme->display( 'Description' ); ?></p>
                        <?php
                            if ( $this->theme->parent() ) {
                                printf( ' <p class="howto">' . __( 'This <a href="%1$s">child theme</a> requires its parent theme, %2$s.', 'ecobiz-admin' ) . '</p>', __( 'http://codex.wordpress.org/Child_Themes', 'ecobiz-admin' ), $this->theme->parent()->display( 'Name' ) );
                            }
                        ?>

                    </div>
                </div>

                <?php
                $item_info = ob_get_contents();

                ob_end_clean();

                $sampleHTML = '';
                if ( file_exists( dirname( __FILE__ ) . '/info-html.html' ) ) {
                    Redux_Functions::initWpFilesystem();

                    global $wp_filesystem;

                    $sampleHTML = $wp_filesystem->get_contents( dirname( __FILE__ ) . '/info-html.html' );
                }

                $this->sections[] = array(
                    'icon'   => 'el-icon-cogs',
                    'title'  => __( 'General', 'ecobiz-admin' ),
                    'fields' => array(
                        array(
                            'id'       => 'header-logo',
                            'type'     => 'media',
                            'url'      => true,
                            'title'    => __( 'Header Logo', 'ecobiz-admin' ),
                            'subtitle' => __( 'Upload your logo for header section here', 'ecobiz-admin' ),
                            'compiler' => 'true',
                            'default'  => array( 'url' => get_template_directory_uri().'/images/logo.png'),
                        ),
                        array(
                            'id'       => 'favicon',
                            'type'     => 'media',
                            'url'      => true,
                            'title'    => __( 'Favicon', 'ecobiz-admin' ),
                            'subtitle' => __( 'Upload your favicon for your site here', 'ecobiz-admin' ),
                            'compiler' => 'true',
                            'default'  => array( 'url' => get_template_directory_uri().'/images/favicon.ico' ),
                        ),
                        array(
                            'id'        =>'analytic-code',
                            'type'      => 'textarea',
                            'title'     => __('Analytics Code', 'ecobiz-admin'),
                            'subtitle'  => __('Paste your Google Analytics (or other) tracking code here.', 'ecobiz-admin'),
                        ),                        
                        array(
                            'id'        => 'page-not-found-text',
                            'type'      => 'textarea',
                            'title'     => __('404 Text', 'ecobiz-admin'),
                            'subtitle'  => __('Enter your 404 (Page Not Found) Text here, HTML tags are allowed.', 'ecobiz-admin'),
                            'default'   => 'Page Not Found!',
                            'validate'  => 'html'
                        ),
                    )
                );
                
                $this->sections[] = array(
                    'icon'   => 'el-icon-tasks',
                    'title'  => __( 'Header', 'ecobiz-admin' ),
                    'fields' => array(
                        array(
                            'id'       => 'header-contact',
                            'type'     => 'switch',
                            'title'    => __( 'Header Contact', 'ecobiz-admin' ),
                            'subtitle' => __( 'Enable or disable contact info in header section', 'ecobiz-admin' ),
                            'default'  => true,
                        ),
                        array(
                            'id'        =>'breadcrumb',
                            'type'      => 'switch',
                            'title'     => __('Breacrumb Menu', 'ecobiz-admin'),
                            'subtitle'  => __('Enable or disable breadcrumb navigation.', 'ecobiz-admin'),
                            'default'  => true,
                        ),
                    )
                );
                
                
                $this->sections[] = array(
                    'title'  => __( 'Slider', 'ecobiz-admin' ),
                    'desc'   => __( 'General Slider Settings','ecobiz-admin' ),
                    'icon'   => 'el-icon-screen-alt',
                    'fields' => array(
                        array( 
                            'id' => 'slideshow-order',
                            'type' => 'select',
                            'title' => __('Slideshow Items Order','ecobiz'),
        					'subtitle' => __('Select your order parameter for slideshow items.','ecobiz'),
        					'options' => array(
                                'author'        => 'author',
                                'date'          => 'date',
                                'title'         => 'title',
                                'modified'      => 'modified',
                                'menu_order'    => 'menu_order',
                                'parent'        => 'parent',
                                'ID'            => 'ID',
                                'rand'          => 'rand'
                            ),
                            'default' => 'date'
                        )
                    ),
                );
                
                $this->sections[] = array(
                    'icon'       => 'el-icon-th',
                    'title'      => __( 'Nivo Slider', 'ecobiz-admin' ),
                    'subsection' => true,
                    'fields'     => array(
                        $fields = array(
                            'id'   => 'info_normal',
                            'type' => 'info',
                            'style'=> 'info',
                            'desc' => __('Nivo Slider Settings','ecobiz-admin')
                        ),
                        array(
                            'id'       => 'nivo-transition',
                            'type'     => 'select',
                            'title'    => __( 'Slider Transition', 'ecobiz-admin' ),
                            'subtitle'     => __( 'Please select your default Nivo slider transition', 'ecobiz-admin' ),
                            'options' => array(
                                        'sliceDown'             => 'sliceDown',
                                        'sliceDownLeft'         => 'sliceDownLeft',
                                        'sliceUp'               => 'sliceUp',
                                        'sliceUpLeft'           => 'sliceUpLeft',
                                        'sliceUpDown'           => 'sliceUpDown',
                                        'sliceUpDownLeft'       => 'sliceUpDownLeft',
                                        'fold'                  => 'fold',
                                        'fade'                  => 'fade',
                                        'random'                => 'random',
                                        'slideInRight'          => 'slideInRight',
                                        'slideInLeft'           => 'slideInLeft',
                                        'boxRandom'             => 'boxRandom',
                                        'boxRain'               => 'boxRain',
                                        'boxRainReverse'        => 'boxRainReverse',
                                        'boxRainGrow'           => 'boxRainGrow',
                                        'boxRainGrowReverse'    => 'boxRainGrowReverse'
                                    ),
                            'default' => 'random'
                        ),
                        array(
                            'id'      => 'nivo-slice',
                            'type'    => 'slider',
                            'title'   => __( 'Slider Slice Number', 'ecobiz-admin' ),
                            'subtitle'    => __( 'Please select number of slices for slideshow', 'ecobiz-admin' ),
                            'default' => 15,
                            'min'     => 5,
                            'step'    => 5,
                            'max'     => 50,
                            'display_value' => 'text'
                        ),
                        array(
                            'id'      => 'nivo-speed',
                            'type'    => 'slider',
                            'title'   => __( 'Slider Transition Speed', 'ecobiz-admin' ),
                            'subtitle'    => __( 'Please select speed time for transation (in milliseconds)', 'ecobiz-admin' ),
                            'default' => 500,
                            'min'     => 100,
                            'step'    => 100,
                            'max'     => 2000,
                            'display_value' => 'text'
                        ),
                        array(
                            'id'      => 'nivo-pause',
                            'type'    => 'slider',
                            'title'   => __( 'Slider Pause Speed', 'ecobiz-admin' ),
                            'subtitle'    => __( 'Please select pause time for transation (in milliseconds)', 'ecobiz-admin' ),
                            'default' => 3000,
                            'min'     => 1000,
                            'step'    => 1000,
                            'max'     => 7000,
                            'display_value' => 'text'
                        ),
                        array(
                            'id'       => 'nivo-nav',
                            'type'     => 'switch',
                            'title'    => __( 'Display Direction Navigation?', 'ecobiz-admin' ),
                            'subtitle' => __( 'Show or hide "Prev" and "Next" slider button navigation', 'ecobiz-admin' ),
                            'default'  => true,
                        ),
                        array(
                            'id'       => 'nivo-pause-hover',
                            'type'     => 'switch',
                            'title'    => __( 'Pause on Hover?', 'ecobiz-admin' ),
                            'subtitle' => __( 'Pause slider animation on hovering', 'ecobiz-admin' ),
                            'default'  => true,
                        ),
                        array(
                            'id'       => 'nivo-caption',
                            'type'     => 'switch',
                            'title'    => __( 'Slider Caption', 'ecobiz-admin' ),
                            'subtitle' => __( 'Show or hide slider caption', 'ecobiz-admin' ),
                            'default'  => false,
                        ),
                        /*array(
                            'id'       => 'nivo-caption-position',
                            'type'     => 'select',
                            'required' => array( 'nivo-caption', '=', '1' ),
                            'title'    => __( 'Caption Position', 'ecobiz-admin' ),
                            'subtitle' => __( 'Please select your slider caption position, left or right', 'ecobiz-admin' ),
                            //Must provide key => value pairs for select options
                            'options'  => array(
                                'left' => 'Left',
                                'right' => 'Right'
                            ),
                            'default'  => 'left'
                        ),*/
                        array(
                            'id'       => 'nivo-permalink',
                            'type'     => 'switch',
                            'title'    => __( 'Slider Permalink', 'ecobiz-admin' ),
                            'subtitle' => __( 'Enable or disable slider permalink', 'ecobiz-admin' ),
                            'default'  => true,
                        ),
                    )
                );
                
                $this->sections[] = array(
                    'icon'       => 'el-icon-barcode',
                    'title'      => __( 'Kwicks Slider', 'ecobiz-admin' ),
                    'subsection' => true,
                    'fields'     => array(
                        $fields = array(
                            'id'   => 'info_normal',
                            'type' => 'info',
                            'style'=> 'info',
                            'desc' => __('Kwicks Slider Settings','ecobiz-admin')
                        ),
                         array(
                            'id'      => 'kwicks-speed',
                            'type'    => 'slider',
                            'title'   => __( 'Slider Speed', 'ecobiz-admin' ),
                            'subtitle'    => __( 'Please select speed time for transation (in milliseconds)', 'ecobiz-admin' ),
                            'default' => 300,
                            'min'     => 300,
                            'step'    => 100,
                            'max'     => 2000,
                            'display_value' => 'text'
                        ),
                        array(
                            'id'       => 'kwicks-caption',
                            'type'     => 'switch',
                            'title'    => __( 'Slider Caption', 'ecobiz-admin' ),
                            'subtitle' => __( 'Show or hide slider caption', 'ecobiz-admin' ),
                            'default'  => true,
                        ),
                    )
                );
                
                $this->sections[] = array(
                    'icon'       => 'el-icon-photo',
                    'title'      => __( 'Static Slider', 'ecobiz-admin' ),
                    'subsection' => true,
                    'fields'     => array(
                        $fields = array(
                            'id'   => 'info_normal',
                            'type' => 'info',
                            'style'=> 'info',
                            'desc' => __('Static Slider Settings','ecobiz-admin')
                        ),
                         array(
                            'id'      => 'slider-static-title',
                            'type'    => 'text',
                            'title'   => __( 'Title', 'ecobiz-admin' ),
                            'subtitle'    => __( 'Enter your title here', 'ecobiz-admin' ),
                        ),
                        array(
                            'id'      => 'slider-static-content',
                            'type'    => 'textarea',
                            'title'   => __( 'Content', 'ecobiz-admin' ),
                            'subtitle'    => __( 'Enter your title here', 'ecobiz-admin' ),
                        ),
                        array(
                            'id'      => 'slider-static-media',
                            'type'    => 'text',
                            'title'   => __( 'Image/Video', 'ecobiz-admin' ),
                            'subtitle'    => __( 'URL for uploaded image or video link', 'ecobiz-admin' ),
                        ),
                        array(
                            'id'      => 'slider-static-url',
                            'type'    => 'text',
                            'title'   => __( 'Custom URL', 'ecobiz-admin' ),
                            'subtitle'    => __( 'Enter your custom URL here', 'ecobiz-admin' ),
                        ),
                        array(
                            'id'      => 'slider-static-button-text',
                            'type'    => 'text',
                            'title'   => __( 'Button URL text', 'ecobiz-admin' ),
                            'subtitle'    => __( 'Enter your custom text for button URL here', 'ecobiz-admin' ),
                        ),
                        array(
                            'id'       => 'slider-static-bgcolor',
                            'type'     => 'color',
                            'title'    => __('Text Block Background Color', 'ecobiz-admin'),
                            'subtitle' => __('Pick a background color (default: #6EBA0B).', 'ecobiz-admin'),
                            'default'  => '#6EBA0B',
                            'validate' => 'color',
                            'transparent' => false
                        ),
                    )
                );
                
                $this->sections[] = array(
                    'icon'       => 'el-icon-website',
                    'title'      => __( 'Styling Options', 'ecobiz-admin' ),
                    'subsection' => false,
                    'fields'     => array(
                                array(
        							'id'        => 'style-skin',
        							'type'      => 'image_select',
        							'title'     => __('Predefined Skin', 'ecobiz-admin'),
        							'subtitle'  => __('Please select of one of predefined skins as your default skin.', 'ecobiz-admin'),
        							'options'   => array(
        								'green.png' => array('alt' => '',  'img' => get_template_directory_uri() . '/admin/sample/skins/green.png'),
        								'blue.png' => array('alt' => '',  'img' => get_template_directory_uri() . '/admin/sample/skins/blue.png'),
        								'red.png' => array('alt' => '',  'img' => get_template_directory_uri(). '/admin/sample/skins/red.png'),
        								'orange.png' => array('alt' => '',  'img' => get_template_directory_uri() . '/admin/sample/skins/orange.png'),
        								'dark.png' => array('alt' => '',  'img' => get_template_directory_uri() . '/admin/sample/skins/dark.png'),
                                        'brown.png' => array('alt' => '',  'img' => get_template_directory_uri() . '/admin/sample/skins/brown.png')	
        							),
        							'width' => 40,
        							'height' => 40,
        							'default'   => 'green.png'
        						),
                                array(
        							'id'        => 'style-pattern',
        							'type'      => 'image_select',
        							'title'     => __('Transparent Background Pattern', 'ecobiz-admin'),
        							'subtitle'  => __('Please select of one of patterns as your default background pattern..', 'ecobiz-admin'),
                                    'tiles'     => true,
        							'options'   => array(
                                        '3px-tile.png' =>   array('alt' => '3px-tile',  'img' => get_template_directory_uri() . '/images/pattern/3px-tile.png'),
                                        'arches.png' =>   array('alt' => 'arches',  'img' => get_template_directory_uri() . '/images/pattern/arches.png'),
                                        'argyle.png' =>   array('alt' => 'argyle',  'img' => get_template_directory_uri() . '/images/pattern/argyle.png'),
                                        'asfalt-dark.png' =>   array('alt' => 'asfalt-dark',  'img' => get_template_directory_uri() . '/images/pattern/asfalt-dark.png'),
                                        'asfalt-light.png' =>   array('alt' => 'asfalt-light',  'img' => get_template_directory_uri() . '/images/pattern/asfalt-light.png'),
                                        'axiom-pattern.png' =>   array('alt' => 'axiom-pattern',  'img' => get_template_directory_uri() . '/images/pattern/axiom-pattern.png'),
                                        'black-linen.png' =>   array('alt' => 'black-linen',  'img' => get_template_directory_uri() . '/images/pattern/black-linen.png'),
                                        'black-thread-light.png' =>   array('alt' => 'black-thread-light',  'img' => get_template_directory_uri() . '/images/pattern/black-thread-light.png'),
                                        'blu-stripes.png' =>   array('alt' => 'blu-stripes',  'img' => get_template_directory_uri() . '/images/pattern/blu-stripes.png'),
                                        'bright-squares.png' =>   array('alt' => 'bright-squares',  'img' => get_template_directory_uri() . '/images/pattern/bright-squares.png'),
                                        'broken-noise.png' =>   array('alt' => 'broken-noise',  'img' => get_template_directory_uri() . '/images/pattern/broken-noise.png'),
                                        'brushed-alum-dark.png' =>   array('alt' => 'brushed-alum-dark',  'img' => get_template_directory_uri() . '/images/pattern/brushed-alum-dark.png'),
                                        'brushed-alum.png' =>   array('alt' => 'brushed-alum',  'img' => get_template_directory_uri() . '/images/pattern/brushed-alum.png'),
                                        'bubbles-1.png' =>   array('alt' => 'bubbles-1',  'img' => get_template_directory_uri() . '/images/pattern/bubbles-1.png'),
                                        'bubbles-2.png' =>   array('alt' => 'bubbles-2',  'img' => get_template_directory_uri() . '/images/pattern/bubbles-2.png'),
                                        'cartographer.png' =>   array('alt' => 'cartographer',  'img' => get_template_directory_uri() . '/images/pattern/cartographer.png'),
                                        'checkered-light-emboss.png' =>   array('alt' => 'checkered-light-emboss',  'img' => get_template_directory_uri() . '/images/pattern/checkered-light-emboss.png'),
                                        'checkered-pattern.png' =>   array('alt' => 'checkered-pattern',  'img' => get_template_directory_uri() . '/images/pattern/checkered-pattern.png'),
                                        'classy-fabric.png' =>   array('alt' => 'classy-fabric',  'img' => get_template_directory_uri() . '/images/pattern/classy-fabric.png'),
                                        'climpek.png' =>   array('alt' => 'climpek',  'img' => get_template_directory_uri() . '/images/pattern/climpek.png'),
                                        'cream-pixels.png' =>   array('alt' => 'cream-pixels',  'img' => get_template_directory_uri() . '/images/pattern/cream-pixels.png'),
                                        'crisp-paper-ruffles.png' =>   array('alt' => 'crisp-paper-ruffles',  'img' => get_template_directory_uri() . '/images/pattern/crisp-paper-ruffles.png'),
                                        'crissxcross.png' =>   array('alt' => 'crissxcross',  'img' => get_template_directory_uri() . '/images/pattern/crissxcross.png'),
                                        'crumpled-paper.png' =>   array('alt' => 'crumpled-paper',  'img' => get_template_directory_uri() . '/images/pattern/crumpled-paper.png'),
                                        'dark-denim-3.png' =>   array('alt' => 'dark-denim-3',  'img' => get_template_directory_uri() . '/images/pattern/dark-denim-3.png'),
                                        'dark-exa.png' =>   array('alt' => 'dark-exa',  'img' => get_template_directory_uri() . '/images/pattern/dark-exa.png'),
                                        'dark-mosaic.png' =>   array('alt' => 'dark-mosaic',  'img' => get_template_directory_uri() . '/images/pattern/dark-mosaic.png'),
                                        'diagmonds-light.png' =>   array('alt' => 'diagmonds-light',  'img' => get_template_directory_uri() . '/images/pattern/diagmonds-light.png'),
                                        'diagmonds.png' =>   array('alt' => 'diagmonds',  'img' => get_template_directory_uri() . '/images/pattern/diagmonds.png'),
                                        'diagonal-line1.png' =>   array('alt' => 'diagonal-line1',  'img' => get_template_directory_uri() . '/images/pattern/diagonal-line1.png'),
                                        'diagonal-line3.png' =>   array('alt' => 'diagonal-line3',  'img' => get_template_directory_uri() . '/images/pattern/diagonal-line3.png'),
                                        'diagonal-striped-brick.png' =>   array('alt' => 'diagonal-striped-brick',  'img' => get_template_directory_uri() . '/images/pattern/diagonal-striped-brick.png'),
                                        'diagonales-decalees.png' =>   array('alt' => 'diagonales-decalees',  'img' => get_template_directory_uri() . '/images/pattern/diagonales-decalees.png'),
                                        'diamond-upholstery.png' =>   array('alt' => 'diamond-upholstery',  'img' => get_template_directory_uri() . '/images/pattern/diamond-upholstery.png'),
                                        'escheresque.png' =>   array('alt' => 'escheresque',  'img' => get_template_directory_uri() . '/images/pattern/escheresque.png'),
                                        'fabric-of-squares.png' =>   array('alt' => 'fabric-of-squares',  'img' => get_template_directory_uri() . '/images/pattern/fabric-of-squares.png'),
                                        'fabric-plaid.png' =>   array('alt' => 'fabric-plaid',  'img' => get_template_directory_uri() . '/images/pattern/fabric-plaid.png'),
                                        'flower-black.png' =>   array('alt' => 'flower-black',  'img' => get_template_directory_uri() . '/images/pattern/flower-black.png'),
                                        'flower-pattern.png' =>   array('alt' => 'flower-pattern',  'img' => get_template_directory_uri() . '/images/pattern/flower-pattern.png'),
                                        'flower-swirl1.png' =>   array('alt' => 'flower-swirl1',  'img' => get_template_directory_uri() . '/images/pattern/flower-swirl1.png'),
                                        'flower-swirl10.png' =>   array('alt' => 'flower-swirl10',  'img' => get_template_directory_uri() . '/images/pattern/flower-swirl10.png'),
                                        'flower-swirl2.png' =>   array('alt' => 'flower-swirl2',  'img' => get_template_directory_uri() . '/images/pattern/flower-swirl2.png'),
                                        'flower-swirl3.png' =>   array('alt' => 'flower-swirl3',  'img' => get_template_directory_uri() . '/images/pattern/flower-swirl3.png'),
                                        'flower-swirl4.png' =>   array('alt' => 'flower-swirl4',  'img' => get_template_directory_uri() . '/images/pattern/flower-swirl4.png'),
                                        'flower-swirl5.png' =>   array('alt' => 'flower-swirl5',  'img' => get_template_directory_uri() . '/images/pattern/flower-swirl5.png'),
                                        'flower-swirl7.png' =>   array('alt' => 'flower-swirl7',  'img' => get_template_directory_uri() . '/images/pattern/flower-swirl7.png'),
                                        'flower-swirl8.png' =>   array('alt' => 'flower-swirl8',  'img' => get_template_directory_uri() . '/images/pattern/flower-swirl8.png'),
                                        'flower-swirl9.png' =>   array('alt' => 'flower-swirl9',  'img' => get_template_directory_uri() . '/images/pattern/flower-swirl9.png'),
                                        'flower-white.png' =>   array('alt' => 'flower-white',  'img' => get_template_directory_uri() . '/images/pattern/flower-white.png'),
                                        'flowers.png' =>   array('alt' => 'flowers',  'img' => get_template_directory_uri() . '/images/pattern/flowers.png'),
                                        'football-no-lines.png' =>   array('alt' => 'football-no-lines',  'img' => get_template_directory_uri() . '/images/pattern/football-no-lines.png'),
                                        'fresh-snow.png' =>   array('alt' => 'fresh-snow',  'img' => get_template_directory_uri() . '/images/pattern/fresh-snow.png'),
                                        'gplay.png' =>   array('alt' => 'gplay',  'img' => get_template_directory_uri() . '/images/pattern/gplay.png'),
                                        'gradient-squares.png' =>   array('alt' => 'gradient-squares',  'img' => get_template_directory_uri() . '/images/pattern/gradient-squares.png'),
                                        'graphy-dark.png' =>   array('alt' => 'graphy-dark',  'img' => get_template_directory_uri() . '/images/pattern/graphy-dark.png'),
                                        'graphy.png' =>   array('alt' => 'graphy',  'img' => get_template_directory_uri() . '/images/pattern/graphy.png'),
                                        'gravel.png' =>   array('alt' => 'gravel',  'img' => get_template_directory_uri() . '/images/pattern/gravel.png'),
                                        'gray-floral.png' =>   array('alt' => 'gray-floral',  'img' => get_template_directory_uri() . '/images/pattern/gray-floral.png'),
                                        'grid-me.png' =>   array('alt' => 'grid-me',  'img' => get_template_directory_uri() . '/images/pattern/grid-me.png'),
                                        'grid1.png' =>   array('alt' => 'grid1',  'img' => get_template_directory_uri() . '/images/pattern/grid1.png'),
                                        'grid2.png' =>   array('alt' => 'grid2',  'img' => get_template_directory_uri() . '/images/pattern/grid2.png'),
                                        'grid3.png' =>   array('alt' => 'grid3',  'img' => get_template_directory_uri() . '/images/pattern/grid3.png'),
                                        'grid4.png' =>   array('alt' => 'grid4',  'img' => get_template_directory_uri() . '/images/pattern/grid4.png'),
                                        'grilled-noise.png' =>   array('alt' => 'grilled-noise',  'img' => get_template_directory_uri() . '/images/pattern/grilled-noise.png'),
                                        'groovepaper.png' =>   array('alt' => 'groovepaper',  'img' => get_template_directory_uri() . '/images/pattern/groovepaper.png'),
                                        'grunge.png' =>   array('alt' => 'grunge',  'img' => get_template_directory_uri() . '/images/pattern/grunge.png'),
                                        'hexellence.png' =>   array('alt' => 'hexellence',  'img' => get_template_directory_uri() . '/images/pattern/hexellence.png'),
                                        'horizontal-line2.png' =>   array('alt' => 'horizontal-line2',  'img' => get_template_directory_uri() . '/images/pattern/horizontal-line2.png'),
                                        'inflicted.png' =>   array('alt' => 'inflicted',  'img' => get_template_directory_uri() . '/images/pattern/inflicted.png'),
                                        'inspiration-geometry.png' =>   array('alt' => 'inspiration-geometry',  'img' => get_template_directory_uri() . '/images/pattern/inspiration-geometry.png'),
                                        'light-sketch.png' =>   array('alt' => 'light-sketch',  'img' => get_template_directory_uri() . '/images/pattern/light-sketch.png'),
                                        'light-wool.png' =>   array('alt' => 'light-wool',  'img' => get_template_directory_uri() . '/images/pattern/light-wool.png'),
                                        'lined-paper-2.png' =>   array('alt' => 'lined-paper-2',  'img' => get_template_directory_uri() . '/images/pattern/lined-paper-2.png'),
                                        'lined-paper.png' =>   array('alt' => 'lined-paper',  'img' => get_template_directory_uri() . '/images/pattern/lined-paper.png'),
                                        'maze-white.png' =>   array('alt' => 'maze-white',  'img' => get_template_directory_uri() . '/images/pattern/maze-white.png'),
                                        'mozaic1.png' =>   array('alt' => 'mozaic1',  'img' => get_template_directory_uri() . '/images/pattern/mozaic1.png'),
                                        'mozaic2.png' =>   array('alt' => 'mozaic2',  'img' => get_template_directory_uri() . '/images/pattern/mozaic2.png'),
                                        'my-little-plaid.png' =>   array('alt' => 'my-little-plaid',  'img' => get_template_directory_uri() . '/images/pattern/my-little-plaid.png'),
                                        'nami.png' =>   array('alt' => 'nami',  'img' => get_template_directory_uri() . '/images/pattern/nami.png'),
                                        'natural-paper.png' =>   array('alt' => 'natural-paper',  'img' => get_template_directory_uri() . '/images/pattern/natural-paper.png'),
                                        'noise-pattern-with-subtle-cross-lines.png' =>   array('alt' => 'noise-pattern-with-subtle-cross-lines',  'img' => get_template_directory_uri() . '/images/pattern/noise-pattern-with-subtle-cross-lines.png'),
                                        'noisy-grid.png' =>   array('alt' => 'noisy-grid',  'img' => get_template_directory_uri() . '/images/pattern/noisy-grid.png'),
                                        'padded-light.png' =>   array('alt' => 'padded-light',  'img' => get_template_directory_uri() . '/images/pattern/padded-light.png'),
                                        'pixelite.png' =>   array('alt' => 'pixelite',  'img' => get_template_directory_uri() . '/images/pattern/pixelite.png'),
                                        'ps-neutral.png' =>   array('alt' => 'ps-neutral',  'img' => get_template_directory_uri() . '/images/pattern/ps-neutral.png'),
                                        'purty-wood.png' =>   array('alt' => 'purty-wood',  'img' => get_template_directory_uri() . '/images/pattern/purty-wood.png'),
                                        'pw-pattern.png' =>   array('alt' => 'pw-pattern',  'img' => get_template_directory_uri() . '/images/pattern/pw-pattern.png'),
                                        'random-grey-variations.png' =>   array('alt' => 'random-grey-variations',  'img' => get_template_directory_uri() . '/images/pattern/random-grey-variations.png'),
                                        'shattered-dark.png' =>   array('alt' => 'shattered-dark',  'img' => get_template_directory_uri() . '/images/pattern/shattered-dark.png'),
                                        'shattered.png' =>   array('alt' => 'shattered',  'img' => get_template_directory_uri() . '/images/pattern/shattered.png'),
                                        'shley-tree-1.png' =>   array('alt' => 'shley-tree-1',  'img' => get_template_directory_uri() . '/images/pattern/shley-tree-1.png'),
                                        'shley-tree-2.png' =>   array('alt' => 'shley-tree-2',  'img' => get_template_directory_uri() . '/images/pattern/shley-tree-2.png'),
                                        'skulls.png' =>   array('alt' => 'skulls',  'img' => get_template_directory_uri() . '/images/pattern/skulls.png'),
                                        'striped.png' =>   array('alt' => 'striped',  'img' => get_template_directory_uri() . '/images/pattern/striped.png'),
                                        'subtle-grey.png' =>   array('alt' => 'subtle-grey',  'img' => get_template_directory_uri() . '/images/pattern/subtle-grey.png'),
                                        'subtle-grunge.png' =>   array('alt' => 'subtle-grunge',  'img' => get_template_directory_uri() . '/images/pattern/subtle-grunge.png'),
                                        'subtle-stripes.png' =>   array('alt' => 'subtle-stripes',  'img' => get_template_directory_uri() . '/images/pattern/subtle-stripes.png'),
                                        'subtle-white-feathers.png' =>   array('alt' => 'subtle-white-feathers',  'img' => get_template_directory_uri() . '/images/pattern/subtle-white-feathers.png'),
                                        'subtle-zebra-3d.png' =>   array('alt' => 'subtle-zebra-3d',  'img' => get_template_directory_uri() . '/images/pattern/subtle-zebra-3d.png'),
                                        'swirl.png' =>   array('alt' => 'swirl',  'img' => get_template_directory_uri() . '/images/pattern/swirl.png'),
                                        'tex2res1.png' =>   array('alt' => 'tex2res1',  'img' => get_template_directory_uri() . '/images/pattern/tex2res1.png'),
                                        'tileable-wood-colored.png' =>   array('alt' => 'tileable-wood-colored',  'img' => get_template_directory_uri() . '/images/pattern/tileable-wood-colored.png'),
                                        'tileable-wood.png' =>   array('alt' => 'tileable-wood',  'img' => get_template_directory_uri() . '/images/pattern/tileable-wood.png'),
                                        'tree-bark.png' =>   array('alt' => 'tree-bark',  'img' => get_template_directory_uri() . '/images/pattern/tree-bark.png'),
                                        'vertical-line2.png' =>   array('alt' => 'vertical-line2',  'img' => get_template_directory_uri() . '/images/pattern/vertical-line2.png'),
                                        'washi.png' =>   array('alt' => 'washi',  'img' => get_template_directory_uri() . '/images/pattern/washi.png'),
                                        'wavecut.png' =>   array('alt' => 'wavecut',  'img' => get_template_directory_uri() . '/images/pattern/wavecut.png'),
                                        'white-diamond-dark.png' =>   array('alt' => 'white-diamond-dark',  'img' => get_template_directory_uri() . '/images/pattern/white-diamond-dark.png'),
                                        'white-diamond.png' =>   array('alt' => 'white-diamond',  'img' => get_template_directory_uri() . '/images/pattern/white-diamond.png'),
                                        'white-wall-3-2.png' =>   array('alt' => 'white-wall-3-2',  'img' => get_template_directory_uri() . '/images/pattern/white-wall-3-2.png'),
                                        'wild-flowers.png' =>   array('alt' => 'wild-flowers',  'img' => get_template_directory_uri() . '/images/pattern/wild-flowers.png'),
                                        'wood-pattern.png' =>   array('alt' => 'wood-pattern',  'img' => get_template_directory_uri() . '/images/pattern/wood-pattern.png'),
                                        'wood.png' =>   array('alt' => 'wood',  'img' => get_template_directory_uri() . '/images/pattern/wood.png'),
                                    ),
        							'width' => 40,
        							'height' => 40,
        							'default'   => get_template_directory_uri() . '/images/pattern/grid1.png'
        						),
                                array(
                                    'id'       => 'style-bgpattern-switcher',
                                    'type'     => 'switch',
                                    'title'    => __( 'Disable Background Pattern?', 'ecobiz-admin' ),
                                    'subtitle' => __( 'Switch to "on" to disable background pattern', 'ecobiz-admin' ),
                                    'default'  => false,
                                ),
                                array(
                                    'id'       => 'style-bgcolor',
                                    'type'     => 'color',
                                    'title'    => __('Body Background Color', 'redux-framework-demo'),
                                    'subtitle' => __('Pick a background color for the theme (default: #5D8500).', 'ecobiz-admin'),
                                    'default'  => '#5D8500',
                                    'validate' => 'color',
                                    'transparent'   => false
                                ),
                                array(
                                    'id'       => 'style-bgimage-switcher',
                                    'type'     => 'switch',
                                    'title'    => __( 'Background Image', 'ecobiz-admin' ),
                                    'subtitle' => __( 'Enable Background image?', 'ecobiz-admin' ),
                                    'default'  => false,
                                ),
                                array(         
                                    'id'       => 'style-background',
                                    'type'     => 'background',
                                    'title'    => __('Body Background Image', 'ecobiz-admin'),
                                    'subtitle' => __('Body background with image', 'ecobiz-admin'),
                                    'background-color' => false,
                                    'transparent' => false,
                                    'required' => array( 'style-bgimage-switcher', '=', '1' ),
                                ),
                                array(
                                    'id'          => 'style-body-font',
                                    'type'        => 'typography', 
                                    'title'       => __('Body Font', 'ecobiz-admin'),
                                    'google'      => false, 
                                    'font-backup' => false,
                                    'units'       =>'px',
                                    'subtitle'    => __('Set the default font for body paragraph section', 'redux-framework-demo'),
                                    'text-align'    => false,
                                    'font-weight'   => false,
                                    'font-style'    => false,
                                    'always_display'    => true,
                                    'default'     => array(
                                        'color'       => '#666666', 
                                        'font-style'  => '400', 
                                        'font-family' => 'Arial', 
                                        'google'      => false,
                                        'font-size'   => '12px', 
                                        'line-height' => '21px'
                                    ),
                                ),
                                array(
                                    'id'          => 'style-heading-font',
                                    'type'        => 'typography', 
                                    'title'       => __('Heading Font', 'ecobiz-admin'),
                                    'google'      => true, 
                                    'font-backup' => true,
                                    'units'       =>'px',
                                    'subtitle'    => __('Set the default font for heading section', 'redux-framework-demo'),
                                    'text-align'    => false,
                                    'color'         => false,
                                    'font-size'     => false,
                                    'line-height'   => false,
                                    'always_display'    => true,
                                    'default'     => array(
                                        'color'       => '#333', 
                                        'font-family' => 'Open Sans', 
                                        'google'      => true,
                                    ),
                                    'preview'      => array(
                                        'font-size' => '33px'
                                    )
                                ),
                                array(
                                    'id'          => 'style-menu-font',
                                    'type'        => 'typography', 
                                    'title'       => __('Menu Font', 'ecobiz-admin'),
                                    'google'      => true, 
                                    'font-backup' => true,
                                    'units'       =>'px',
                                    'subtitle'    => __('Set the default font for header menu section', 'redux-framework-demo'),
                                    'text-align'    => false,
                                    'color'         => false,
                                    'font-size'     => false,
                                    'line-height'   => false,
                                    'always_display'    => true,
                                    'default'     => array(
                                        'color'       => '#333',
                                        'font-family' => 'Open Sans', 
                                        'google'      => true,
                                    ),
                                    'preview'      => array(
                                        'font-size' => '33px'
                                    )
                                ),
                                array(
                                    'id'      => 'custom-css-code',
                                    'type'    => 'textarea',
                                    'title'   => __( 'Custom CSS', 'ecobiz-admin' ),
                                    'subtitle'    => __( 'Add your custom css code here.', 'ecobiz-admin' ),
                                ),
                    )
                );
                                                                                
                $this->sections[] = array(
                    'title'  => __( 'Portfolio', 'ecobiz-admin' ),
                    'desc'   => __( 'General options for portfolio page', 'ecobiz-admin' ),
                    'icon'   => 'el-icon-picture',
                    'fields' => array(
                            array(
                                'id'        => 'portfolio-page',
                                'type'      => 'select',
                                'title'     => __( 'Portfolio Page', 'ecobiz-admin' ),
                                'subtitle'  => __( 'Select your default portfolio page', 'ecobiz-admin' ),
                                'data'      => 'pages',
                                'args'      => array('orderby=date')       
                            ),
                             array(
                                'id'        => 'portfolio-order',
                                'type'      => 'select',
                                'title'     => __( 'Portfolio Category', 'ecobiz-admin' ),
                                'subtitle'  => __( 'Select the categories that will be included in blog page', 'ecobiz-admin' ),
                                'options' => array(
                                        'author'        => 'author',
                                        'date'          => 'date',
                                        'title'         => 'title',
                                        'modified'      => 'modified',
                                        'menu_order'    => 'menu_order',
                                        'parent'        => 'parent',
                                        'ID'            => 'ID',
                                        'rand'          => 'rand'
                                    ),
                                'default' => 'date'       
                            ),    
                            array(
                                'id'      => 'portfolio-number-perpage',
                                'type'    => 'slider',
                                'title'   => __( 'Item per page ', 'ecobiz-admin' ),
                                'subtitle'    => __( 'Number items to display per blog page', 'ecobiz-admin' ),
                                'default' => 4,
                                'min'     => 1,
                                'step'    => 1,
                                'max'     => 16,
                                'display_value' => 'label'
                            ),  
                    ),
                );
                
                $this->sections[] = array(
                    'title'  => __( 'Blog', 'ecobiz-admin' ),
                    'desc'   => __( 'General options for blog page.', 'ecobiz-admin' ),
                    'icon'   => 'el-icon-calendar',
                    'fields' => array(
                            array(
                                'id'        => 'blog-category',
                                'type'      => 'checkbox',
                                'title'     => __( 'Blog Category', 'ecobiz-admin' ),
                                'subtitle'  => __( 'Select the categories that will be included in blog page', 'ecobiz-admin' ),
                                'data'      => 'category',
                                'args'      => array('orderby=date')       
                            ), 
                            array(
                                'id'        => 'blog-order',
                                'type'      => 'select',
                                'title'     => __( 'Blog Order', 'ecobiz-admin' ),
                                'subtitle'  => __( 'Select default order parameter for blog items', 'ecobiz-admin' ),
                                'options' => array(
                                        'author'        => 'author',
                                        'date'          => 'date',
                                        'title'         => 'title',
                                        'modified'      => 'modified',
                                        'menu_order'    => 'menu_order',
                                        'parent'        => 'parent',
                                        'ID'            => 'ID',
                                        'rand'            => 'rand'
                                    ),
                                'default' => 'date'       
                            ),    
                            array(
                                'id'      => 'blog-number-perpage',
                                'type'    => 'slider',
                                'title'   => __( 'Item per page ', 'ecobiz-admin' ),
                                'subtitle'    => __( 'Number items to display per blog page', 'ecobiz-admin' ),
                                'default' => 1,
                                'min'     => 1,
                                'step'    => 1,
                                'max'     => 10,
                                'display_value' => 'text'
                            ),  
                            array(
                                'id'       => 'blog-author',
                                'type'     => 'switch',
                                'title'    => __( 'Author Box', 'ecobiz-admin' ),
                                'subtitle' => __( 'Enable or disable author box info', 'ecobiz-admin' ),
                                'default'  => true,
                            ),
                            array(
                                'id'       => 'blog-comment',
                                'type'     => 'switch',
                                'title'    => __( 'Comment', 'ecobiz-admin' ),
                                'subtitle' => __( 'Enable or disable comment box', 'ecobiz-admin' ),
                                'default'  => true,
                            ), 
                            array(
                                'id'       => 'blog-metapost',
                                'type'     => 'switch',
                                'title'    => __( 'Meta Post', 'ecobiz-admin' ),
                                'subtitle' => __( 'Enable or disable meta post info', 'ecobiz-admin' ),
                                'default'  => true,
                            ),   
                            array(
                                'id'       => 'blog-comment',
                                'type'     => 'switch',
                                'title'    => __( 'Blog Comment', 'ecobiz-admin' ),
                                'subtitle' => __( 'Enable or disable meta post info', 'ecobiz-admin' ),
                                'default'  => true,
                            ),                              
                    ),
                );                                                
                
                 // ACTUAL DECLARATION OF SECTIONS
                $this->sections[] = array(
                    'title'  => __( 'Sidebar', 'ecobiz-admin' ),
                    'desc'   => '',
                    'icon'   => 'el-icon-file-new-alt',
                    // 'submenu' => false, // Setting submenu to false on a given section will hide it from the WordPress sidebar menu!
                    'fields' => array(
                           array(
                            'id'        =>'sidebar-widget',
                            'type'=> 'multi_text',
							'title' => __('Generate a new sidebar', 'ecobiz-admin'),
							'subtitle' => __('Type the name of the new sidebar widget area', 'ecobiz-admin'),
                            ),
                    )
                );
                                                                
                $this->sections[] = array(
                    'title'  => __( 'Contact', 'ecobiz-admin' ),
                    'icon'   => 'el-icon-comment-alt',
                    'fields' => array(
                        array(
                            'id'       => 'office-address',
                            'type'     => 'textarea',
                            'title'    => __( 'Office Address', 'ecobiz-admin' ),
                            'subtitle' => __( 'Enter your office address here.', 'ecobiz-admin' ),
                        ),
                        array(
                            'id'       => 'latitude-address',
                            'type'     => 'text',
                            'title'    => __( 'Latitude', 'ecobiz-admin' ),
                            'subtitle' => __( 'Enter your latitude point here, for quick search, please visit <a href="http://itouchmap.com/latlong.html" target="_blank">http://itouchmap.com/latlong.html</a>', 'ecobiz-admin' ),
                        ),
                        array(
                            'id'       => 'longitude-address',
                            'type'     => 'text',
                            'title'    => __( 'Longitude', 'ecobiz-admin' ),
                            'subtitle' => __( 'Enter your longitude here, for quick search, please visit <a href="http://itouchmap.com/latlong.html" target="_blank">http://itouchmap.com/latlong.html</a>', 'ecobiz-admin' ),
                        ),
                        array(
                            'id'       => 'phone-number',
                            'type'     => 'text',
                            'title'    => __( 'Phone', 'ecobiz-admin' ),
                            'subtitle' => __( 'Enter your phone number here.', 'ecobiz-admin' ),
                        ),
                        array(
                            'id'       => 'faximile-number',
                            'type'     => 'text',
                            'title'    => __( 'Faximile', 'ecobiz-admin' ),
                            'subtitle' => __( 'Enter your faximile number here.', 'ecobiz-admin' ),
                        ),
                        array(
                            'id'       => 'email-address',
                            'type'     => 'text',
                            'title'    => __( 'E-mail', 'ecobiz-admin' ),
                            'subtitle' => __( 'Enter your e-mail address here.', 'ecobiz-admin' ),
                            'validate' => 'email',
                            'msg'      => 'Invalid e-mail address!'
                        ),
                        array(
                            'id'       => 'email-success-msg',
                            'type'     => 'textarea',
                            'title'    => __( 'Success Message', 'ecobiz-admin' ),
                            'subtitle' => __( 'Enter your success message when contact form successfully sent email.', 'ecobiz-admin' ),
                            'default'   => __('Email Successfully Sent!','ecobiz-admin')
                        ),
                    ),
                );
                
                $this->sections[] = array(
                    'icon'       => 'el-icon-twitter',
                    'title'      => __( 'Twitter', 'ecobiz-admin' ),
                    'subsection' => true,
                    'fields'     => array(
                        $fields = array(
                            'id'   => 'info_normal',
                            'type' => 'info',
                            'style'=> 'info',
                            'desc' => __('<strong>Twitter Settings</strong><br/>To get token, token secret, consumer key and consumer key secret.</br>
                                          1. Go to <a href="https://apps.twitter.com/" target="_blank">https://apps.twitter.com/</a>. Login use twitter account</br>
                                          2. Create an Application on the twitter dev site, click the "Create Application" button. Fill the form then click the "Create your Twitter application" button.</br>
                                          3. Click "Create my access token" button, you will get token and token secret code.', 'ecobiz-admin')
                        ),
                        array(
                            'id'       => 'twitter-id',
                            'type'     => 'text',
                            'title'    => __( 'Twitter ID', 'ecobiz-admin' ),
                            'subtitle'     => __( 'Please input your twitter username. default : imediapixel', 'ecobiz-admin' )
                        ),
                        array(
                            'id'       => 'twitter-token',
                            'type'     => 'text',
                            'title'    => __( 'Twitter Token', 'ecobiz-admin' ),
                            'subtitle'     => __( 'Please input your twitter username. default : imediapixel', 'ecobiz-admin' )
                        ),
                        array(
                            'id'       => 'twitter-token-secret',
                            'type'     => 'text',
                            'title'    => __( 'Twitter Token Secret', 'ecobiz-admin' ),
                            'subtitle'     => __( 'Please input your twitter username. default : imediapixel', 'ecobiz-admin' )
                        ),
                        array(
                            'id'       => 'twitter-comsumer-key',
                            'type'     => 'text',
                            'title'    => __( 'Twitter Comsumer Key', 'ecobiz-admin' ),
                            'subtitle'     => __( 'Please input your twitter username. default : imediapixel', 'ecobiz-admin' )
                        ),
                        array(
                            'id'       => 'twitter-consumer-secret',
                            'type'     => 'text',
                            'title'    => __( 'Twitter Comsumer Secret', 'ecobiz-admin' ),
                            'subtitle'     => __( 'Please input your twitter username. default : imediapixel', 'ecobiz-admin' )
                        ),
                    )
                );
                
                $this->sections[] = array(
                    'icon'       => 'el-icon-group',
                    'title'      => __( 'Social Profile', 'ecobiz-admin' ),
                    'subsection' => true,
                    'fields'     => array(
                        array(
                            'id'       => 'linkedin-id',
                            'type'     => 'text',
                            'title'    => __( 'Linkedin', 'ecobiz-admin' ),
                            'subtitle'     => __( 'Please add your linkedin profile URL here.', 'ecobiz-admin' )
                        ),
                        array(
                            'id'       => 'facebook-id',
                            'type'     => 'text',
                            'title'    => __( 'Facebook', 'ecobiz-admin' ),
                            'subtitle'     => __( 'Please add your Facebook profile URL here.', 'ecobiz-admin' )
                        ),
                        array(
                            'id'       => 'flickr-id',
                            'type'     => 'text',
                            'title'    => __( 'Flickr', 'ecobiz-admin' ),
                            'subtitle'     => __( 'Please add your Flickr ID here, use <a href="http://www.idgettr.com">IDGettr</a> to find it.', 'ecobiz-admin' )
                        ),
                        array(
                            'id'       => 'skype-id',
                            'type'     => 'text',
                            'title'    => __( 'Skype', 'ecobiz-admin' ),
                            'subtitle'     => __( 'Please add your Skype ID here, not an URL.', 'ecobiz-admin' )
                        ),
                        array(
                            'id'       => 'youtube-id',
                            'type'     => 'text',
                            'title'    => __( 'Youtube', 'ecobiz-admin' ),
                            'subtitle'     => __( 'Please add your Youtube profile URL here.', 'ecobiz-admin' )
                        ),
                    )
                );
                
                $this->sections[] = array(
                    'title'  => __( 'Footer', 'ecobiz-admin' ),
                    'icon'   => 'el-icon-road',
                    'fields' => array(
                            array(
                                'id'       => 'footer-logo',
                                'type'     => 'media',
                                'url'      => true,
                                'title'    => __( 'Footer Logo', 'ecobiz-admin' ),
                                'subtitle' => __( 'Upload your logo for footer section here', 'ecobiz-admin' ),
                                'compiler' => 'true',
                                'default'  => array( 'url' => get_template_directory_uri().'/images/footerlogo.png' ),
                            ),
                            array(
                                'id'       => 'footer-text',
                                'type'     => 'textarea',
                                'title'    => __( 'Footer Text', 'ecobiz-admin' ),
                                'subtitle' => __( 'Add your additional footer text here, eg. Site Copyright, etc', 'ecobiz-admin' ),
                            ),
                            array(
                                'id'       => 'footer-column',
                                'type'     => 'select',
                                'title'    => __( 'Footer Columns', 'ecobiz-admin' ),
                                'subtitle' => __( 'Select your default columns for footer section', 'ecobiz-admin' ),
                                'options'  => array(
                                    '4' => 'Four Columns',
                                    '3' => 'Three Columns',
                                    '2' => 'Two Columns',
                                    '1' => 'One Column',
                                ),
                                'default'  => '4',
                            ),
                            array(
                                'id'       => 'footer-disable',
                                'type'     => 'switch',
                                'title'    => __( 'Disable Footer Columns?', 'ecobiz-admin' ),
                                'subtitle' => __( 'Enable or disable fotoer columns section', 'ecobiz-admin' ),
                                'default'  => false,
                            ),
                    ),
                );
                
                
                $this->sections[] = array(
                    'title'  => __( 'Import / Export', 'ecobiz-admin' ),
                    'desc'   => __( 'Import and Export your Redux Framework settings from file, text or URL.', 'ecobiz-admin' ),
                    'icon'   => 'el-icon-refresh',
                    'fields' => array(
                        array(
                            'id'         => 'opt-import-export',
                            'type'       => 'import_export',
                            'title'      => 'Import Export',
                            'subtitle'   => 'Save and restore your Redux options',
                            'full_width' => false,
                        ),
                    ),
                );
                
                $this->sections[] = array(
                    'icon'   => 'el-icon-info-sign',
                    'title'  => __( 'Theme Information', 'redux-framework-demo' ),
                    'desc'   => __( '', 'ecobiz-admin' ),
                    'fields' => array(
                        array(
                            'id'      => 'opt-raw-info',
                            'type'    => 'raw',
                            'content' => $item_info,
                        )
                    ),
                );

            }
            

            public function setHelpTabs() {

                // Custom page help tabs, displayed using the help API. Tabs are shown in order of definition.
                $this->args['help_tabs'][] = array(
                    'id'      => 'redux-help-tab-1',
                    'title'   => __( 'Theme Information 1', 'ecobiz-admin' ),
                    'content' => __( '<p>This is the tab content, HTML is allowed.</p>', 'ecobiz-admin' )
                );

                $this->args['help_tabs'][] = array(
                    'id'      => 'redux-help-tab-2',
                    'title'   => __( 'Theme Information 2', 'ecobiz-admin' ),
                    'content' => __( '<p>This is the tab content, HTML is allowed.</p>', 'ecobiz-admin' )
                );

                // Set the help sidebar
                $this->args['help_sidebar'] = __( '<p>This is the sidebar content, HTML is allowed.</p>', 'ecobiz-admin' );
            }

            /**
             * All the possible arguments for Redux.
             * For full documentation on arguments, please refer to: https://github.com/ReduxFramework/ReduxFramework/wiki/Arguments
             * */
            public function setArguments() {

                $theme = wp_get_theme(); // For use with some settings. Not necessary.

                $this->args = array(
                    // TYPICAL -> Change these values as you need/desire
                    'opt_name'             => 'ecobiz',
                    // This is where your data is stored in the database and also becomes your global variable name.
                    'display_name'         => $theme->get( 'Name' ),
                    // Name that appears at the top of your panel
                    'display_version'      => $theme->get( 'Version' ),
                    // Version that appears at the top of your panel
                    'menu_type'            => 'menu',
                    //Specify if the admin menu should appear or not. Options: menu or submenu (Under appearance only)
                    'allow_sub_menu'       => true,
                    // Show the sections below the admin menu item or not
                    'menu_title'           => __( 'Ecobiz Options', 'ecobiz-admin' ),
                    'page_title'           => __( 'Ecobiz Options', 'ecobiz-admin' ),
                    // You will need to generate a Google API key to use this feature.
                    // Please visit: https://developers.google.com/fonts/docs/developer_api#Auth
                    'google_api_key'       => '',
                    // Set it you want google fonts to update weekly. A google_api_key value is required.
                    'google_update_weekly' => false,
                    // Must be defined to add google fonts to the typography module
                    'async_typography'     => true,
                    // Use a asynchronous font on the front end or font string
                    //'disable_google_fonts_link' => true,                    // Disable this in case you want to create your own google fonts loader
                    'admin_bar'            => true,
                    // Show the panel pages on the admin bar
                    'admin_bar_icon'     => 'dashicons-portfolio',
                    // Choose an icon for the admin bar menu
                    'admin_bar_priority' => 50,
                    // Choose an priority for the admin bar menu
                    'global_variable'      => '',
                    // Set a different name for your global variable other than the opt_name
                    'dev_mode'             => false,
                    // Show the time the page took to load, etc
                    'update_notice'        => false,
                    // If dev_mode is enabled, will notify developer of updated versions available in the GitHub Repo
                    'customizer'           => true,
                    // Enable basic customizer support
                    //'open_expanded'     => true,                    // Allow you to start the panel in an expanded way initially.
                    //'disable_save_warn' => true,                    // Disable the save warning when a user changes a field

                    // OPTIONAL -> Give you extra features
                    'page_priority'        => null,
                    // Order where the menu appears in the admin area. If there is any conflict, something will not show. Warning.
                    'page_parent'          => 'themes.php',
                    // For a full list of options, visit: http://codex.wordpress.org/Function_Reference/add_submenu_page#Parameters
                    'page_permissions'     => 'manage_options',
                    // Permissions needed to access the options panel.
                    'menu_icon'            => '',
                    // Specify a custom URL to an icon
                    'last_tab'             => '',
                    // Force your panel to always open to a specific tab (by id)
                    'page_icon'            => 'icon-themes',
                    // Icon displayed in the admin panel next to your menu_title
                    'page_slug'            => '_options',
                    // Page slug used to denote the panel
                    'save_defaults'        => true,
                    // On load save the defaults to DB before user clicks save or not
                    'default_show'         => false,
                    // If true, shows the default value next to each field that is not the default value.
                    'default_mark'         => '',
                    // What to print by the field's title if the value shown is default. Suggested: *
                    'show_import_export'   => true,
                    // Shows the Import/Export panel when not used as a field.

                    // CAREFUL -> These options are for advanced use only
                    'transient_time'       => 60 * MINUTE_IN_SECONDS,
                    'output'               => true,
                    // Global shut-off for dynamic CSS output by the framework. Will also disable google fonts output
                    'output_tag'           => true,
                    // Allows dynamic CSS to be generated for customizer and google fonts, but stops the dynamic CSS from going to the head
                    // 'footer_credit'     => '',                   // Disable the footer credit of Redux. Please leave if you can help it.

                    // FUTURE -> Not in use yet, but reserved or partially implemented. Use at your own risk.
                    'database'             => '',
                    // possible: options, theme_mods, theme_mods_expanded, transient. Not fully functional, warning!
                    'system_info'          => false,
                    // REMOVE

                    // HINTS
                    'hints'                => array(
                        'icon'          => 'icon-question-sign',
                        'icon_position' => 'right',
                        'icon_color'    => 'lightgray',
                        'icon_size'     => 'normal',
                        'tip_style'     => array(
                            'color'   => 'light',
                            'shadow'  => true,
                            'rounded' => false,
                            'style'   => '',
                        ),
                        'tip_position'  => array(
                            'my' => 'top left',
                            'at' => 'bottom right',
                        ),
                        'tip_effect'    => array(
                            'show' => array(
                                'effect'   => 'slide',
                                'duration' => '500',
                                'event'    => 'mouseover',
                            ),
                            'hide' => array(
                                'effect'   => 'slide',
                                'duration' => '500',
                                'event'    => 'click mouseleave',
                            ),
                        ),
                    )
                );

                // ADMIN BAR LINKS -> Setup custom links in the admin bar menu as external items.
                $this->args['admin_bar_links'][] = array(
                    'id'    => 'redux-docs',
                    'href'   => 'http://docs.reduxframework.com/',
                    'title' => __( 'Documentation', 'ecobiz-admin' ),
                );

                $this->args['admin_bar_links'][] = array(
                    //'id'    => 'redux-support',
                    'href'   => 'https://github.com/ReduxFramework/redux-framework/issues',
                    'title' => __( 'Support', 'ecobiz-admin' ),
                );

                $this->args['admin_bar_links'][] = array(
                    'id'    => 'redux-extensions',
                    'href'   => 'reduxframework.com/extensions',
                    'title' => __( 'Extensions', 'ecobiz-admin' ),
                );

                // Panel Intro text -> before the form
                if ( ! isset( $this->args['global_variable'] ) || $this->args['global_variable'] !== false ) {
                    if ( ! empty( $this->args['global_variable'] ) ) {
                        $v = $this->args['global_variable'];
                    } else {
                        $v = str_replace( '-', '_', $this->args['opt_name'] );
                    }
                    //$this->args['intro_text'] = sprintf( __( '<p>Did you know that Redux sets a global variable for you? To access any of your saved options from within your code you can use your global variable: <strong>$%1$s</strong></p>', 'ecobiz-admin' ), $v );
                } else {
                    //$this->args['intro_text'] = __( '<p>This text is displayed above the options panel. It isn\'t required, but more info is always better! The intro_text field accepts all HTML.</p>', 'ecobiz-admin' );
                }

                // Add content after the form.
                //$this->args['footer_text'] = __( '<p>This text is displayed below the options panel. It isn\'t required, but more info is always better! The footer_text field accepts all HTML.</p>', 'ecobiz-admin' );
            }

            public function validate_callback_function( $field, $value, $existing_value ) {
                $error = true;
                $value = 'just testing';

                /*
              do your validation

              if(something) {
                $value = $value;
              } elseif(something else) {
                $error = true;
                $value = $existing_value;
                
              }
             */

                $return['value'] = $value;
                $field['msg']    = 'your custom error message';
                if ( $error == true ) {
                    $return['error'] = $field;
                }

                return $return;
            }

            public function class_field_callback( $field, $value ) {
                print_r( $field );
                echo '<br/>CLASS CALLBACK';
                print_r( $value );
            }

        }

        global $reduxConfig;
        $reduxConfig = new Redux_Framework_sample_config();
    } else {
        echo "The class named Redux_Framework_sample_config has already been called. <strong>Developers, you need to prefix this class with your company name or you'll run into problems!</strong>";
    }

    /**
     * Custom function for the callback referenced above
     */
    if ( ! function_exists( 'redux_my_custom_field' ) ):
        function redux_my_custom_field( $field, $value ) {
            print_r( $field );
            echo '<br/>';
            print_r( $value );
        }
    endif;

    /**
     * Custom function for the callback validation referenced above
     * */
    if ( ! function_exists( 'redux_validate_callback_function' ) ):
        function redux_validate_callback_function( $field, $value, $existing_value ) {
            $error = true;
            $value = 'just testing';

            /*
          do your validation

          if(something) {
            $value = $value;
          } elseif(something else) {
            $error = true;
            $value = $existing_value;
            
          }
         */

            $return['value'] = $value;
            $field['msg']    = 'your custom error message';
            if ( $error == true ) {
                $return['error'] = $field;
            }

            return $return;
        }
    endif;
