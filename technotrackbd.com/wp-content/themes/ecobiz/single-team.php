<?php get_header();?>
      
      <?php
        global $post, $ecobiz;
        $meta_prefix = "_imediapixel_";
        
        $page_heading_image = get_post_meta(get_the_ID(),$meta_prefix."page_heading_image",true);
        $bgtext_heading_position = get_post_meta(get_the_ID(),"_imediapixel_bgtext_heading_position",true);
        $page_desc = get_post_meta(get_the_ID(),$meta_prefix."staff_occupation",true);
        
        $page_sidebar_position = get_post_meta(get_the_ID(),$meta_prefix."page_sidebar_position",true);
        
        $breadcrumb         = $ecobiz['breadcrumb'];
      ?>      

        <!-- Page Heading --> 
        <div id="page-heading">
          <img src="<?php echo $page_heading_image ? $page_heading_image : get_template_directory_uri().'/images/page-heading.jpg';?>" alt="" />
          <div class="heading-text<?php if ($bgtext_heading_position =="right") echo '-right';?>">
            <h3><?php the_title();?></h3>
            <p><?php echo stripslashes($page_desc);?></p>
          </div>
        </div>
        <!-- Page Heading End -->
        
      <div class="clear"></div>
      
      <div class="center">      
        <?php if ($breadcrumb == 1) { ?>
          <div class="breadcrumb">
            <?php if ( function_exists( 'breadcrumbs_plus' ) ) breadcrumbs_plus(); ?>
          </div>
        <?php } ?>              
          
        <!-- Main Content Wrapper -->
        <div class="maincontent">
          <?php if (have_posts()) : ?>
          <?php 
            while (have_posts()) : the_post();
            $meta_prefix             = "_imediapixel_";
             
            $thumb   = get_post_thumbnail_id();
            $img_url = wp_get_attachment_url( $thumb,'full' ); //get full URL to image (use "large" or "medium" if the images too big)
            $image   = aq_resize( $img_url, 220,'', true ); //resize & crop the image
            
            $staff_occupation   = get_post_meta(get_the_ID(),$meta_prefix."staff_occupation", true);
            $staff_fb           = get_post_meta(get_the_ID(),$meta_prefix.'staff_fb', true);
            $staff_twitter      = get_post_meta(get_the_ID(),$meta_prefix.'staff_twitter', true);
            $staff_google     = get_post_meta(get_the_ID(),$meta_prefix.'staff_google', true);
            $staff_linkedin     = get_post_meta(get_the_ID(),$meta_prefix.'staff_linkedin', true);
            $staff_email     = get_post_meta(get_the_ID(),$meta_prefix.'staff_email', true);
          ?>
          <div class="col_13">
            <div class="team-wrapper-page">
            <?php if (function_exists('has_post_thumbnail') && has_post_thumbnail()) {?>
              <img src="<?php echo $img_url;?>" alt="" class="alignleft" />                
            <?php } ?>
                <div class="team-title-page">
                    <h4><?php the_title();?></h4>
                    <p class="team-subtitle"><?php echo $staff_occupation;?></p>
                    <div class="team-social-page">
                    <?php if($staff_twitter){ ?>
                            <a href="<?php echo $staff_twitter;?>"><i class="icon-twitter"></i></a>
                        <?php }?>
                        <?php if($staff_fb){ ?>
                            <a href="<?php echo $staff_fb;?>"><i class="icon-facebook"></i></a>
                        <?php }?>
                        <?php if($staff_google){ ;?>
                            <a href="'.$staff_google.'"><i class="icon-googleplus"></i></a>
                        <?php }?>
                        <?php if($staff_linkedin){ ?>
                            <a href="<?php echo $staff_linkedin;?>"><i class="icon-linkedin"></i></a>
                        <?php }?>
                        <?php if($staff_email){ ?>
                            <a href="mailto:<?php echo $staff_email;?>"><i class="icon-envelope"></i></a>
                        <?php }?>
                    </div>
                </div>
            </div>
          </div>
          <div class="col_23 last">
                <?php the_content();?>
          </div>
          
          <?php endwhile;?>
          <?php endif;?>
        </div>
        <!-- Main Content Wrapper End -->
        
        <?php wp_reset_query();?>
        <?php get_sidebar();?>
    
  <?php get_footer();?>