<?php get_header();?>
      <?php
        global $post, $ecobiz;
        
        $meta_prefix = "_imediapixel_";
        
        $page_sidebar_position = get_post_meta(get_the_ID(),$meta_prefix."page_sidebar_position",true);
        $breadcrumb         = $ecobiz['breadcrumb'];
      ?>
        <!-- Page Heading --> 
        <div id="page-heading">
          <img src="<?php echo get_template_directory_uri().'/images/page-heading.jpg';?>" alt="" />
          <div class="heading-text<?php if ($bgtext_heading_position =="right") echo '-right';?>">
            <?php $post = $posts[0]; // Hack. Set $post so that the_date() works. ?>
         	  <?php /* If this is a category archive */ if (is_category()) { ?>
        		<h3><?php single_cat_title(); ?></h3>
         	  <?php /* If this is a tag archive */ } elseif( is_tag() ) { ?>
        		<h3><?php single_tag_title(); ?></h3>
         	  <?php /* If this is a daily archive */ } elseif (is_day()) { ?>
        		<h3><?php echo __('Archive for ','ecobiz');?><?php the_time('F jS, Y'); ?></h3>
         	  <?php /* If this is a monthly archive */ } elseif (is_month()) { ?>
        		<h3><?php echo __('Archive for ','ecobiz');?><?php the_time('F, Y'); ?></h3>
         	  <?php /* If this is a yearly archive */ } elseif (is_year()) { ?>
        		<h3><?php echo __('Archive for','ecobiz');?> <?php the_time('Y'); ?></h3>
        	  <?php /* If this is an author archive */ } elseif (is_author()) { ?>
        		<h3><?php echo __('Author Archive','ecobiz');?></h3>
         	  <?php /* If this is a paged archive */ } elseif (isset($_GET['paged']) && !empty($_GET['paged'])) { ?>
        		<h3><?php echo __('Archives','ecobiz');?></h3>
         	  <?php } ?>  
              <p><?php echo category_description(); ?></p>
          </div>
        </div>
        <!-- Page Heading End -->
      
      <div class="clear"></div>
      
      <div class="center">
        <?php if ($breadcrumb == 1) { ?>
          <div class="breadcrumb">
            <?php if ( function_exists( 'breadcrumbs_plus' ) ) breadcrumbs_plus(); ?>
          </div>
        <?php } ?>
        <!-- Main Content Wrapper -->
        <?php if ($page_sidebar_position !="right" ) { ?>
            <div class="maincontent right">
        <?php } else { ?>
            <div class="maincontent">
        <?php } ?>
        <!-- List Latest News Start //-->
        <ul id="listlatestnews">
        <?php
            global $post, $ecobiz;
            
            $paged = (get_query_var('paged')) ?get_query_var('paged') : ((get_query_var('page')) ? get_query_var('page') : 1);
          
            $slideshow_order = $ecobiz['ecobiz-slideshow-order'] ? $ecobiz['ecobiz-slideshow-order'] : "date";
            $blog_category = $ecobiz['blog-category'];
            
            $blog_order = $ecobiz['blog-order'];
            $blog_number_perpage = $ecobiz['blog-number-perpage'];
            $blog_author = $ecobiz['blog-author'];
            $blog_comment = $ecobiz['blog-comment'];
            $blog_metapost = $ecobiz['blog-metapost'];
            
          while ( have_posts() ) : the_post();
          $thumb   = get_post_thumbnail_id();
          $img_url = wp_get_attachment_url( $thumb,'full' ); //get full URL to image (use "large" or "medium" if the images too big)
          $image   = aq_resize( $img_url, 84, 84, true ); //resize & crop the image
          ?>
          <li>
            <div class="boximg-blog">
            <?php if (function_exists('has_post_thumbnail') && has_post_thumbnail()) {?>
              <div class="blogimage">
                <img src="<?php echo $image;?>" alt="" class="boximg-pad" />
              </div>
            <?php } ?>
            </div>
            <div <?php post_class('postbox'); ?>>
            <h3><a href="<?php the_permalink();?>"><?php the_title();?></a></h3>
            <p><?php the_excerpt();?></p>
           </div>
           <div class="clear"></div>
           <?php if ($blog_metapost ==1 ) { ?>
                <div class="metapost">
                  <span class="first"><?php echo __('Posted at ','ecobiz');?><?php the_time( get_option('date_format') ); ?> &raquo;</span>
                  <span><?php echo __('By ','ecobiz');?>: <?php the_author_posts_link();?>   &raquo;</span>                         
                  <span><?php echo __('Categories ','ecobiz');?>: <?php the_category(',');?>   &raquo;</span>
                  <span><?php comments_popup_link(__('0 Comment','ecobiz'),__('1 Comment','ecobiz'),__('% Comments','ecobiz'));?></span>
                </div>           
                <div class="clear"></div>
           <?php }?>
          </li>
          <?php endwhile;?> 
          </ul>
          <div class="clear"></div>
          <div class="pagination">
            <?php theme_blog_pagenavi('', '', $r, $paged);?>
          </div>          
        </div>
        <!-- Main Content Wrapper End -->
        
        <?php wp_reset_query();?>
        <?php get_sidebar();?>
    
  <?php get_footer();?>