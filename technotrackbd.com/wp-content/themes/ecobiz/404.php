<?php get_header();?>
      <?php
      global $ecobiz;
      $meta_prefix = "_imediapixel_";
      
      $page_not_found_text = $ecobiz['page-not-found-text'];
      $breadcrumb         = $ecobiz['breadcrumb'];
      $page_sidebar_position = get_post_meta(get_the_ID(),$meta_prefix."page_sidebar_position",true);
      ?>
      <!-- Page Heading --> 
      <div id="page-heading">
        <img src="<?php echo $heading_image ? $heading_image : get_template_directory_uri().'/images/page-heading.jpg';?>" alt="" />
        <div class="heading-text<?php if ($bgtext_heading_position =="right") echo '-right';?>">
          <h3><?php echo __('404 Page','ecobiz');?></h3>
        </div>
      </div>
      <!-- Page Heading End -->
      <div class="clear"></div>
      
      <div class="center">
        <?php if ($breadcrumb == 1) { ?>
          <div class="breadcrumb">
            <?php if ( function_exists( 'breadcrumbs_plus' ) ) breadcrumbs_plus(); ?>
          </div>
        <?php } ?>
        
        <!-- Main Content Wrapper -->
        <?php if ($page_sidebar_position !="right" ) { ?>
            <div class="maincontent right">
        <?php } else { ?>
            <div class="maincontent">
        <?php } ?>
        <?php
          $_404_text = $page_not_found_text ? $page_not_found_text : __("Apologies, but the page you requested could not be found",'ecobiz_');
        ?>
        <h1><?php echo stripslashes($_404_text);?></h1>
        <h4><?php echo __('Try different search?','ecobiz');?></h4>
        <div style="float: left;">
        <?php get_template_part('searchbox','Custom ECOBIZ search box');?>
        </div>      
        </div>
        <!-- Main Content Wrapper End -->
        
        <?php wp_reset_query();?>
        <?php get_sidebar();?>
    
  <?php get_footer();?>