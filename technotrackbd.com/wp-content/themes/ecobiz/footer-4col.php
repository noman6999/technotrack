    <?php
        global $ecobiz;
        
        $info_address = $ecobiz['office-address'];
        $info_phone = $ecobiz['phone-number'];
        $info_fax = $ecobiz['faximile-number'];
        $info_email = $ecobiz['email-address'];
        $footer_logo = $ecobiz['footer-logo'];
        
    ?>    
    <!-- Footer Box #1 -->
      <div class="footerbox">
        <?php if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('bottom1')) { ?>
        <h4><?php echo __('Categories','ecobiz');?></h4>
        <ul>
          <?php wp_list_categories('title_li=&hide_empty=0');?> 
        </ul>
        <?php } ?>
      </div>
      
      <!-- Footer Box #2 -->
      <div class="footerbox">
      <?php if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('bottom2')) { ?>
        <h4><?php echo __('Links','exobiz');?></h4>
        <ul>
          <?php wp_list_bookmarks('title_li=&categorize=0'); ?>
        </ul>
        <?php } ?>
      </div>
      
      <!-- Footer Box #3 -->
      <div class="footerbox">
      <?php if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('bottom3')) { ?>
        <h4><?php echo __('Our Address','ecobiz');?></h4>
        <ul class="addresslist">        
        <?php if ($info_address) { ?>
          <li>
            <?php echo $info_address ? stripslashes($info_address) :  
            "Jln. Damai menuju Syurga No. 14,<br />
            Jakarta 20035,<br />
            Indonesia";?>
          </li>
        <?php }?>
        <?php if ($info_phone) { ?>
          <li><strong><?php echo __('Phone ','ecobiz');?></strong>: <?php echo $info_phone ? $info_phone : "+62 525625";?></li>
        <?php } ?>
          <?php if ($info_fax !="") { ?>
          <li><strong><?php echo __('FAX ','ecobiz');?></strong>: <?php echo $info_fax ? $info_fax : "+62 525625";?></li>
        <?php } ?>
        <?php if ($info_email) { ?>
          <li><strong><?php echo __('Email ','ecobiz');?></strong>: <a href="mailto:<?php echo $info_email;?>"><?php echo $info_email ? $info_email : "info@mydomain.com";?></a></li>
        <?php } ?>
        </ul>
        <?php } ?>
      </div>
      
      <!-- Footer Box #4 -->
      <div class="footerbox box-last">
        <a href="<?php echo home_url();?>"><img src="<?php echo $footer_logo['url'];?>" alt="Footer Logo" class="alignleft"/></a>
        <div class="clear"></div>      
        <?php if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('bottom4')) { ?>
        <?php } ?>
        <ul class="social-links">
        <?php
            $twitter_id     = $ecobiz['twitter-id'];
            $linkedin_id    = $ecobiz['linkedin-id'];
            $facebook_id    = $ecobiz['facebook-id'];
            $flickr_id      = $ecobiz['flickr-id'];
            $skype_id       = $ecobiz['skype-id'];
            $youtube_id     = $ecobiz['youtube-id'];
        ?>
          <?php if ($twitter_id !="") { ?>
          <li>
            <a href="http://twitter.com/<?php echo $twitter_id;?>"><img src="<?php echo get_template_directory_uri();?>/images/twitter.png" alt="Twitter" /></a>
          </li>
          <?php } ?>
          <?php if ($facebook_id !="") { ?>
          <li>
            <a href="<?php echo $facebook_id;?>"><img src="<?php echo get_template_directory_uri();?>/images/facebook.png" alt="Facebook" /></a>
          </li>
          <?php } ?>
          <?php if ($linkedin_id !="") { ?>
          <li>
            <a href="<?php echo $linkedin_id;?>"><img src="<?php echo get_template_directory_uri();?>/images/linkedin.png" alt="Linkedin" /></a>
          </li>
          <?php } ?>
          <?php if ($skype_id !="") { ?>
          <li>
            <a href="skype:<?php echo $skype_id;?>?call"><img src="<?php echo get_template_directory_uri();?>/images/skype.png" alt="skype" /></a>
          </li>
          <?php } ?>
          <?php if ($flickr_id !="") { ?>
          <li>
            <a href="http://www.flickr.com/photos/<?php echo $flickr_id;?>"><img src="<?php echo get_template_directory_uri();?>/images/flickr.png" alt="Flickr" /></a>
          </li>
          <?php } ?>
          <?php if ($youtube_id !="") { ?>
          <li>
            <a href="http://www.youtube.com/user/<?php echo $youtube_id;?>"><img src="<?php echo get_template_directory_uri();?>/images/youtube.png" alt="youtube" /></a>
          </li>
          <?php } ?>
          <li><a href="<?php bloginfo('rss2_url');?>"><img src="<?php echo get_template_directory_uri();?>/images/rss.png" alt="RSS" /></a></li>
        </ul>
      </div>