<?php get_header();?>
    
      <?php
        global $post, $ecobiz;
        
        $blog_author = $ecobiz['blog-author'];
        $blog_comment = $ecobiz['blog-comment'];
        $blog_metapost = $ecobiz['blog-metapost'];
        $breadcrumb         = $ecobiz['breadcrumb'];
      ?>      
      <!-- Page Heading --> 
      <div id="page-heading">
        <img src="<?php echo $heading_image ? $heading_image : get_template_directory_uri().'/images/page-heading.jpg';?>" alt="" />
        <div class="heading-text<?php if ($bgtext_heading_position =="right") echo '-right';?>">
          <h3>
          <?php
          $post_categories = wp_get_post_categories( $post->ID);
          $cat_name = array();      
          foreach($post_categories as $c){
            $cats = get_category( $c );
            $cat_name[] = $cats->name;            
          }
          echo implode(', ',$cat_name);
          ?>          
          </h3>
        </div>
      </div>
      <!-- Page Heading End -->
      <div class="clear"></div>
      
      <div class="center">
            <?php if ($breadcrumb == 1) { ?>
              <div class="breadcrumb">
                <?php if ( function_exists( 'breadcrumbs_plus' ) ) breadcrumbs_plus(); ?>
              </div>
          <?php } ?>   
          
        <!-- Main Content Wrapper -->
        <div class="maincontent">
          <?php if (have_posts()) : ?>
          <?php while (have_posts()) : the_post();?>
          
          <h3><?php the_title();?></h3>
          
          <?php if ($blog_metapost ==1) { ?> 
            <div class="metapost">
              <span class="first"><?php echo __('Posted at ','ecobiz');?><?php the_time( get_option('date_format') ); ?> &raquo;</span>
              <span><?php echo __('By ','ecobiz');?>: <?php the_author_posts_link();?>   &raquo;</span>                         
              <span><?php echo __('Categories ','ecobiz');?>: <?php the_category(',');?>   &raquo;</span>
              <span><?php comments_popup_link(__('0 Comment','ecobiz'),__('1 Comment','ecobiz'),__('% Comments','ecobiz'));?></span>
          </div>           
          <div class="clear"></div>
          <?php }?>
          
          <div class="single-content"><?php the_content();?></div>
          
          <div class="navigation">
    				<div class="alignleft"><?php previous_post_link( '%link', '' . __( '&larr;', 'Previous post link', 'ecoboz' ) . ' %title' ); ?></div>
    				<div class="alignright"><?php next_post_link( '%link', '%title ' . __( '&rarr;', 'Next post link', 'ecobiz' ) ); ?></div>
				  </div><!-- #nav-below -->
          <?php endwhile;?>
          <?php endif;?>
          
          <div class="clear"></div>

          <!-- Author Box Start //-->
          <?php if ($blog_author == 1) { ?>
          <div id="authorbox">
            <div class="blockavatar">
              <?php if (function_exists('get_avatar')) { echo get_avatar(get_the_author_meta('user_email'), '48'); }?>
            </div>
             <div class="detail">
                <h4><?php echo __('About ','ebiz');?><a href="<?php the_author_meta('url') ?>"><?php the_author_meta('display_name'); ?></a></h4>
                <p><?php the_author_meta('description'); ?></p>
             </div>
             <div class="clear"></div>
          </div> 
          <?php } ?>
          <!-- Author Box End //-->
          
          <div class="clear"></div>
          
          <?php 
          if ($blog_comment == 1) {
            comments_template('', true);  
          }
          ?>
        </div>
        <!-- Main Content Wrapper End -->
        
        <?php wp_reset_query();?>
        <?php get_sidebar();?>
    
  <?php get_footer();?>